EESchema Schematic File Version 4
LIBS:DC_Load_NewBrain-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "DC Load NewBrain"
Date "18/04/2019"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_A J?
U 1 1 5CED444F
P 2825 2425
AR Path="/5CED444F" Ref="J?"  Part="1" 
AR Path="/5CEB517F/5CED444F" Ref="J5"  Part="1" 
F 0 "J5" H 2650 2850 50  0000 C CNN
F 1 "USB_A Female" H 2825 2775 39  0000 C CNN
F 2 "DC_Load_NewBrain:USB_A_Horizontal" H 2975 2375 50  0001 C CNN
F 3 " ~" H 2975 2375 50  0001 C CNN
	1    2825 2425
	1    0    0    -1  
$EndComp
$Comp
L dk_Ferrite-Beads-and-Chips:BLM21PG331SN1D FB?
U 1 1 5CED446D
P 3025 3175
AR Path="/5CED446D" Ref="FB?"  Part="1" 
AR Path="/5CEB517F/5CED446D" Ref="FB3"  Part="1" 
F 0 "FB3" H 2825 3050 39  0000 C CNN
F 1 "BLM21AG331SN1D" H 3025 2975 39  0000 C CNN
F 2 "DC_Load_NewBrain:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3225 3375 60  0001 L CNN
F 3 "https://www.murata.com/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 3225 3475 60  0001 L CNN
F 4 "490-5988-1-ND" H 3225 3575 60  0001 L CNN "Digi-Key_PN"
F 5 "BLM21PG331SN1D" H 3225 3675 60  0001 L CNN "MPN"
F 6 "Filters" H 3225 3775 60  0001 L CNN "Category"
F 7 "Ferrite Beads and Chips" H 3225 3875 60  0001 L CNN "Family"
F 8 "https://www.murata.com/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 3225 3975 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/murata-electronics-north-america/BLM21PG331SN1D/490-5988-1-ND/3845188" H 3225 4075 60  0001 L CNN "DK_Detail_Page"
F 10 "FERRITE BEAD 330 OHM 0805 1LN" H 3225 4175 60  0001 L CNN "Description"
F 11 "Murata Electronics North America" H 3225 4275 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3225 4375 60  0001 L CNN "Status"
	1    3025 3175
	1    0    0    -1  
$EndComp
$Comp
L dk_Ferrite-Beads-and-Chips:BLM21PG331SN1D FB?
U 1 1 5CED447D
P 3450 2225
AR Path="/5CED447D" Ref="FB?"  Part="1" 
AR Path="/5CEB517F/5CED447D" Ref="FB2"  Part="1" 
F 0 "FB2" H 3450 2466 39  0000 C CNN
F 1 "BLM21AG331SN1D" H 3375 2375 39  0000 C CNN
F 2 "DC_Load_NewBrain:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3650 2425 60  0001 L CNN
F 3 "https://www.murata.com/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 3650 2525 60  0001 L CNN
F 4 "490-5988-1-ND" H 3650 2625 60  0001 L CNN "Digi-Key_PN"
F 5 "BLM21PG331SN1D" H 3650 2725 60  0001 L CNN "MPN"
F 6 "Filters" H 3650 2825 60  0001 L CNN "Category"
F 7 "Ferrite Beads and Chips" H 3650 2925 60  0001 L CNN "Family"
F 8 "https://www.murata.com/en-us/products/productdata/8796738977822/ENFA0005.pdf" H 3650 3025 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/murata-electronics-north-america/BLM21PG331SN1D/490-5988-1-ND/3845188" H 3650 3125 60  0001 L CNN "DK_Detail_Page"
F 10 "FERRITE BEAD 330 OHM 0805 1LN" H 3650 3225 60  0001 L CNN "Description"
F 11 "Murata Electronics North America" H 3650 3325 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3650 3425 60  0001 L CNN "Status"
	1    3450 2225
	1    0    0    -1  
$EndComp
$Comp
L dk_TVS-Diodes:USBLC6-2SC6 D?
U 1 1 5CED448D
P 4300 2525
AR Path="/5CED448D" Ref="D?"  Part="1" 
AR Path="/5CEB517F/5CED448D" Ref="D7"  Part="1" 
F 0 "D7" H 4300 3212 60  0000 C CNN
F 1 "USBLC6-2SC6" H 4300 3106 60  0000 C CNN
F 2 "DC_Load_NewBrain:SOT23-6L" H 4500 2725 60  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/06/1d/48/9c/6c/20/4a/b2/CD00050750.pdf/files/CD00050750.pdf/jcr:content/translations/en.CD00050750.pdf" H 4500 2825 60  0001 L CNN
F 4 "497-5235-1-ND" H 4500 2925 60  0001 L CNN "Digi-Key_PN"
F 5 "USBLC6-2SC6" H 4500 3025 60  0001 L CNN "MPN"
F 6 "Circuit Protection" H 4500 3125 60  0001 L CNN "Category"
F 7 "TVS - Diodes" H 4500 3225 60  0001 L CNN "Family"
F 8 "http://www.st.com/content/ccc/resource/technical/document/datasheet/06/1d/48/9c/6c/20/4a/b2/CD00050750.pdf/files/CD00050750.pdf/jcr:content/translations/en.CD00050750.pdf" H 4500 3325 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/stmicroelectronics/USBLC6-2SC6/497-5235-1-ND/1121688" H 4500 3425 60  0001 L CNN "DK_Detail_Page"
F 10 "TVS DIODE 5.25V 17V SOT23-6" H 4500 3525 60  0001 L CNN "Description"
F 11 "STMicroelectronics" H 4500 3625 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4500 3725 60  0001 L CNN "Status"
	1    4300 2525
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CED4494
P 3525 3175
AR Path="/5CED4494" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CED4494" Ref="#PWR053"  Part="1" 
F 0 "#PWR053" H 3525 2925 50  0001 C CNN
F 1 "GND" H 3530 3002 50  0000 C CNN
F 2 "" H 3525 3175 50  0001 C CNN
F 3 "" H 3525 3175 50  0001 C CNN
	1    3525 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2525 3525 2525
Wire Wire Line
	2725 3175 2825 3175
Wire Wire Line
	2725 2825 2725 3175
Wire Wire Line
	3225 3175 3525 3175
Connection ~ 3525 3175
Wire Wire Line
	4900 1750 4900 2525
Wire Wire Line
	4900 2525 4800 2525
$Comp
L power:VBUS #PWR?
U 1 1 5CED44A4
P 5050 2525
AR Path="/5CED44A4" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CED44A4" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 5050 2375 50  0001 C CNN
F 1 "VBUS" H 5065 2698 50  0000 C CNN
F 2 "" H 5050 2525 50  0001 C CNN
F 3 "" H 5050 2525 50  0001 C CNN
	1    5050 2525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2525 4900 2525
Connection ~ 4900 2525
Wire Wire Line
	3125 2425 3800 2425
Wire Wire Line
	3800 2425 3800 2225
Wire Wire Line
	3125 2525 3225 2525
Wire Wire Line
	3225 2525 3225 2825
Wire Wire Line
	3225 2825 3800 2825
Wire Wire Line
	3525 2525 3525 3000
Wire Wire Line
	2825 2825 2825 3000
Wire Wire Line
	2825 3000 3525 3000
Connection ~ 3525 3000
Wire Wire Line
	3525 3000 3525 3175
Text Label 5175 2225 2    39   ~ 0
D+
Text Label 5175 2825 2    39   ~ 0
D-
$Comp
L Connector:SD_Card J?
U 1 1 5CEE9363
P 8375 2675
AR Path="/5CEE9363" Ref="J?"  Part="1" 
AR Path="/5CEB517F/5CEE9363" Ref="J6"  Part="1" 
F 0 "J6" H 8375 3340 50  0000 C CNN
F 1 "SOFNG SD-006M SD-CARD" H 8375 3249 50  0000 C CNN
F 2 "DC_Load_NewBrain:SOFNG_SD-006M_SD-CARD" H 8375 2675 50  0001 C CNN
F 3 "http://portal.fciconnect.com/Comergent//fci/drawing/10067847.pdf" H 8375 2675 50  0001 C CNN
	1    8375 2675
	1    0    0    -1  
$EndComp
Text Label 7125 2975 0    39   ~ 0
SD_MISO
Text Label 7125 2775 0    39   ~ 0
SD_SCK
Wire Wire Line
	7475 2775 7400 2775
Text Label 7125 2475 0    39   ~ 0
SD_MOSI
Wire Wire Line
	7475 2475 7450 2475
Text Label 7125 2375 0    39   ~ 0
SD_CS
$Comp
L Device:R R?
U 1 1 5CEE9370
P 6900 2275
AR Path="/5CEE9370" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE9370" Ref="R20"  Part="1" 
F 0 "R20" V 6825 2275 50  0000 C CNN
F 1 "10k" V 6900 2275 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6830 2275 50  0001 C CNN
F 3 "~" H 6900 2275 50  0001 C CNN
	1    6900 2275
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CEE9378
P 6725 2275
AR Path="/5CEE9378" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CEE9378" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 6725 2025 50  0001 C CNN
F 1 "GND" H 6725 2150 39  0000 C CNN
F 2 "" H 6725 2275 50  0001 C CNN
F 3 "" H 6725 2275 50  0001 C CNN
	1    6725 2275
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CEE937E
P 7025 3075
AR Path="/5CEE937E" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE937E" Ref="R21"  Part="1" 
F 0 "R21" V 6950 3075 50  0000 C CNN
F 1 "10k" V 7025 3075 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6955 3075 50  0001 C CNN
F 3 "~" H 7025 3075 50  0001 C CNN
	1    7025 3075
	0    1    1    0   
$EndComp
Wire Wire Line
	7475 3075 7175 3075
Wire Wire Line
	6875 3075 6825 3075
$Comp
L power:GND #PWR?
U 1 1 5CEE9387
P 6825 3075
AR Path="/5CEE9387" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CEE9387" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 6825 2825 50  0001 C CNN
F 1 "GND" H 6830 2902 50  0000 C CNN
F 2 "" H 6825 3075 50  0001 C CNN
F 3 "" H 6825 3075 50  0001 C CNN
	1    6825 3075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2275 6725 2275
Wire Wire Line
	7475 2875 6825 2875
Wire Wire Line
	6825 2875 6825 3075
Connection ~ 6825 3075
Wire Wire Line
	7475 2575 6825 2575
Wire Wire Line
	6825 2575 6825 2875
Connection ~ 6825 2875
$Comp
L power:GND #PWR?
U 1 1 5CEE9394
P 9350 2950
AR Path="/5CEE9394" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CEE9394" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 9350 2700 50  0001 C CNN
F 1 "GND" H 9355 2777 50  0000 C CNN
F 2 "" H 9350 2950 50  0001 C CNN
F 3 "" H 9350 2950 50  0001 C CNN
	1    9350 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9275 2775 9350 2775
Wire Wire Line
	9350 2775 9350 2875
Wire Wire Line
	9275 2875 9350 2875
Connection ~ 9350 2875
Wire Wire Line
	9350 2875 9350 2950
$Comp
L power:+3V3 #PWR?
U 1 1 5CEE939F
P 9400 1825
AR Path="/5CEE939F" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CEE939F" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 9400 1675 50  0001 C CNN
F 1 "+3V3" H 9400 1975 39  0000 C CNN
F 2 "" H 9400 1825 50  0001 C CNN
F 3 "" H 9400 1825 50  0001 C CNN
	1    9400 1825
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5CEE93A5
P 7450 1975
AR Path="/5CEE93A5" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE93A5" Ref="R17"  Part="1" 
F 0 "R17" V 7525 1975 50  0000 C CNN
F 1 "10k" V 7450 1975 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7380 1975 50  0001 C CNN
F 3 "~" H 7450 1975 50  0001 C CNN
	1    7450 1975
	-1   0    0    1   
$EndComp
Text Label 9750 2475 2    39   ~ 0
SD_DETECT
Text Label 9750 2575 2    39   ~ 0
SD_PROTECT
$Comp
L Device:R R?
U 1 1 5CEE93B0
P 9300 2075
AR Path="/5CEE93B0" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE93B0" Ref="R18"  Part="1" 
F 0 "R18" V 9375 2075 50  0000 C CNN
F 1 "10k" V 9300 2075 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9230 2075 50  0001 C CNN
F 3 "~" H 9300 2075 50  0001 C CNN
	1    9300 2075
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5CEE93B7
P 9575 2075
AR Path="/5CEE93B7" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE93B7" Ref="R19"  Part="1" 
F 0 "R19" V 9650 2075 50  0000 C CNN
F 1 "10k" V 9575 2075 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9505 2075 50  0001 C CNN
F 3 "~" H 9575 2075 50  0001 C CNN
	1    9575 2075
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 2375 9350 2575
Connection ~ 9350 2575
Wire Wire Line
	9350 2575 9275 2575
Wire Wire Line
	9300 1925 9300 1875
Wire Wire Line
	9300 1875 9400 1875
Wire Wire Line
	9400 1825 9400 1875
Connection ~ 9400 1875
$Comp
L Device:R R?
U 1 1 5CEE93CB
P 7175 1975
AR Path="/5CEE93CB" Ref="R?"  Part="1" 
AR Path="/5CEB517F/5CEE93CB" Ref="R16"  Part="1" 
F 0 "R16" V 7250 1975 50  0000 C CNN
F 1 "10k" V 7175 1975 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7105 1975 50  0001 C CNN
F 3 "~" H 7175 1975 50  0001 C CNN
	1    7175 1975
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 2125 7450 2475
Connection ~ 7450 2475
Wire Wire Line
	7175 2125 7400 2125
Wire Wire Line
	7400 2125 7400 2775
Connection ~ 7400 2775
Wire Wire Line
	7450 1775 7450 1825
$Comp
L Device:C C?
U 1 1 5CEE93E3
P 6500 2825
AR Path="/5CEE93E3" Ref="C?"  Part="1" 
AR Path="/5CEB517F/5CEE93E3" Ref="C20"  Part="1" 
F 0 "C20" H 6600 2875 39  0000 L CNN
F 1 "100nF" H 6575 2750 39  0000 L CNN
F 2 "DC_Load_NewBrain:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6538 2675 50  0001 C CNN
F 3 "~" H 6500 2825 50  0001 C CNN
	1    6500 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2975 6500 3075
Wire Wire Line
	6500 3075 6825 3075
Wire Wire Line
	6500 2675 7475 2675
$Comp
L power:+3V3 #PWR?
U 1 1 5CEE93ED
P 6500 2625
AR Path="/5CEE93ED" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CEE93ED" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 6500 2475 50  0001 C CNN
F 1 "+3V3" H 6500 2775 39  0000 C CNN
F 2 "" H 6500 2625 50  0001 C CNN
F 3 "" H 6500 2625 50  0001 C CNN
	1    6500 2625
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2625 6500 2675
Connection ~ 6500 2675
Wire Wire Line
	7125 2375 7475 2375
Wire Wire Line
	7125 2475 7450 2475
Wire Wire Line
	7125 2775 7400 2775
Wire Wire Line
	7125 2975 7475 2975
$Comp
L Device:C C?
U 1 1 5CEE93F9
P 6175 2825
AR Path="/5CEE93F9" Ref="C?"  Part="1" 
AR Path="/5CEB517F/5CEE93F9" Ref="C19"  Part="1" 
F 0 "C19" H 6275 2875 39  0000 L CNN
F 1 "10uF" H 6250 2750 39  0000 L CNN
F 2 "DC_Load_NewBrain:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6213 2675 50  0001 C CNN
F 3 "~" H 6175 2825 50  0001 C CNN
	1    6175 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	6175 2675 6500 2675
Wire Wire Line
	6175 2975 6175 3075
Wire Wire Line
	6175 3075 6500 3075
Connection ~ 6500 3075
$Comp
L Switch:SW_Push SW1
U 1 1 5D04B4BF
P 7125 4825
F 0 "SW1" H 7125 5110 50  0000 C CNN
F 1 "SW_Reset" H 7125 5019 39  0000 C CNN
F 2 "DC_Load_NewBrain:SW_Push_SPST_NO_6x6mm_H5mm" H 7125 5025 50  0001 C CNN
F 3 "~" H 7125 5025 50  0001 C CNN
	1    7125 4825
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5D04B4C6
P 7125 5200
F 0 "SW2" H 7125 5485 50  0000 C CNN
F 1 "SW_Boot" H 7125 5394 39  0000 C CNN
F 2 "DC_Load_NewBrain:SW_Push_SPST_NO_6x6mm_H5mm" H 7125 5400 50  0001 C CNN
F 3 "~" H 7125 5400 50  0001 C CNN
	1    7125 5200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR055
U 1 1 5D04B4CD
P 6675 4350
F 0 "#PWR055" H 6675 4200 50  0001 C CNN
F 1 "+3V3" H 6675 4500 39  0000 C CNN
F 2 "" H 6675 4350 50  0001 C CNN
F 3 "" H 6675 4350 50  0001 C CNN
	1    6675 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 5D04B4D3
P 6800 4575
F 0 "R23" V 6875 4575 50  0000 C CNN
F 1 "10k" V 6800 4575 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6730 4575 50  0001 C CNN
F 3 "~" H 6800 4575 50  0001 C CNN
	1    6800 4575
	-1   0    0    1   
$EndComp
$Comp
L Device:R R22
U 1 1 5D04B4DA
P 6550 4575
F 0 "R22" V 6625 4575 50  0000 C CNN
F 1 "10k" V 6550 4575 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6480 4575 50  0001 C CNN
F 3 "~" H 6550 4575 50  0001 C CNN
	1    6550 4575
	-1   0    0    1   
$EndComp
$Comp
L Device:C C21
U 1 1 5D04B4E1
P 6475 5400
F 0 "C21" H 6575 5425 39  0000 L CNN
F 1 "1uF" H 6575 5350 39  0000 L CNN
F 2 "DC_Load_NewBrain:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6513 5250 50  0001 C CNN
F 3 "~" H 6475 5400 50  0001 C CNN
	1    6475 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C22
U 1 1 5D04B4E8
P 6800 5400
F 0 "C22" H 6900 5425 39  0000 L CNN
F 1 "100nF" H 6900 5350 39  0000 L CNN
F 2 "DC_Load_NewBrain:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6838 5250 50  0001 C CNN
F 3 "~" H 6800 5400 50  0001 C CNN
	1    6800 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5D04B4EF
P 7450 5200
F 0 "#PWR060" H 7450 4950 50  0001 C CNN
F 1 "GND" H 7455 5027 50  0000 C CNN
F 2 "" H 7450 5200 50  0001 C CNN
F 3 "" H 7450 5200 50  0001 C CNN
	1    7450 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7325 4825 7450 4825
Wire Wire Line
	7450 4825 7450 5200
Wire Wire Line
	7325 5200 7450 5200
Connection ~ 7450 5200
Wire Wire Line
	6925 4825 6800 4825
Wire Wire Line
	6925 5200 6550 5200
Text Label 6200 4825 0    39   ~ 0
SW_RESET
Text Label 6200 5200 0    39   ~ 0
SW_BOOT
$Comp
L power:GND #PWR063
U 1 1 5D04B4FD
P 6650 5600
F 0 "#PWR063" H 6650 5350 50  0001 C CNN
F 1 "GND" H 6655 5427 50  0000 C CNN
F 2 "" H 6650 5600 50  0001 C CNN
F 3 "" H 6650 5600 50  0001 C CNN
	1    6650 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6475 5550 6475 5600
Wire Wire Line
	6475 5600 6650 5600
Wire Wire Line
	6800 5550 6800 5600
Wire Wire Line
	6800 5600 6650 5600
Connection ~ 6650 5600
Wire Wire Line
	6800 4725 6800 4825
Wire Wire Line
	6550 4725 6550 5200
Wire Wire Line
	6550 5250 6475 5250
Connection ~ 6550 5200
Wire Wire Line
	6550 5200 6200 5200
Wire Wire Line
	6550 5200 6550 5250
Connection ~ 6800 4825
Wire Wire Line
	6800 4825 6200 4825
Wire Wire Line
	6800 4825 6800 5250
Wire Wire Line
	6550 4425 6550 4350
Wire Wire Line
	6550 4350 6675 4350
Wire Wire Line
	6800 4425 6800 4350
Wire Wire Line
	6800 4350 6675 4350
Connection ~ 6675 4350
Wire Wire Line
	4800 2225 5175 2225
Wire Wire Line
	4800 2825 5175 2825
$Comp
L Connector_Generic:Conn_01x20 J?
U 1 1 5D30D02D
P 9075 5225
AR Path="/5D30D02D" Ref="J?"  Part="1" 
AR Path="/5CEB517F/5D30D02D" Ref="J7"  Part="1" 
F 0 "J7" H 9050 6225 50  0000 C CNN
F 1 "FPC/FFC" H 9175 6250 50  0001 C CNN
F 2 "DC_Load_NewBrain:TE_2-84952-0_1x20-1MP_P1.0mm_Horizontal" H 9075 5225 50  0001 C CNN
F 3 "~" H 9075 5225 50  0001 C CNN
	1    9075 5225
	1    0    0    -1  
$EndComp
Text Label 8425 4525 0    39   ~ 0
SD_PROTECT
Text Label 8425 4625 0    39   ~ 0
SD_DETECT
$Comp
L power:GND #PWR?
U 1 1 5D30D036
P 8425 4725
AR Path="/5D30D036" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D036" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 8425 4475 50  0001 C CNN
F 1 "GND" V 8425 4525 50  0000 C CNN
F 2 "" H 8425 4725 50  0001 C CNN
F 3 "" H 8425 4725 50  0001 C CNN
	1    8425 4725
	0    1    1    0   
$EndComp
Text Label 8425 4825 0    39   ~ 0
SD_MOSI
$Comp
L power:GND #PWR?
U 1 1 5D30D03D
P 8425 4925
AR Path="/5D30D03D" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D03D" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 8425 4675 50  0001 C CNN
F 1 "GND" V 8425 4725 50  0000 C CNN
F 2 "" H 8425 4925 50  0001 C CNN
F 3 "" H 8425 4925 50  0001 C CNN
	1    8425 4925
	0    1    1    0   
$EndComp
$Comp
L power:VBUS #PWR?
U 1 1 5D30D043
P 8425 4325
AR Path="/5D30D043" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D043" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 8425 4175 50  0001 C CNN
F 1 "VBUS" V 8425 4550 50  0000 C CNN
F 2 "" H 8425 4325 50  0001 C CNN
F 3 "" H 8425 4325 50  0001 C CNN
	1    8425 4325
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D30D049
P 8425 4425
AR Path="/5D30D049" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D049" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 8425 4175 50  0001 C CNN
F 1 "GND" V 8425 4225 50  0000 C CNN
F 2 "" H 8425 4425 50  0001 C CNN
F 3 "" H 8425 4425 50  0001 C CNN
	1    8425 4425
	0    1    1    0   
$EndComp
Text Label 8425 5025 0    39   ~ 0
SD_MISO
$Comp
L power:GND #PWR?
U 1 1 5D30D050
P 8425 5125
AR Path="/5D30D050" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D050" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 8425 4875 50  0001 C CNN
F 1 "GND" V 8425 4925 50  0000 C CNN
F 2 "" H 8425 5125 50  0001 C CNN
F 3 "" H 8425 5125 50  0001 C CNN
	1    8425 5125
	0    1    1    0   
$EndComp
Text Label 8425 5225 0    39   ~ 0
SD_SCK
$Comp
L power:GND #PWR?
U 1 1 5D30D057
P 8425 5325
AR Path="/5D30D057" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D057" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 8425 5075 50  0001 C CNN
F 1 "GND" V 8425 5125 50  0000 C CNN
F 2 "" H 8425 5325 50  0001 C CNN
F 3 "" H 8425 5325 50  0001 C CNN
	1    8425 5325
	0    1    1    0   
$EndComp
Text Label 8425 5425 0    39   ~ 0
SD_CS
$Comp
L power:GND #PWR?
U 1 1 5D30D05E
P 8425 5525
AR Path="/5D30D05E" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D05E" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 8425 5275 50  0001 C CNN
F 1 "GND" V 8425 5325 50  0000 C CNN
F 2 "" H 8425 5525 50  0001 C CNN
F 3 "" H 8425 5525 50  0001 C CNN
	1    8425 5525
	0    1    1    0   
$EndComp
Text Label 8425 5625 0    39   ~ 0
D-
$Comp
L power:GND #PWR?
U 1 1 5D30D065
P 8425 5725
AR Path="/5D30D065" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D065" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 8425 5475 50  0001 C CNN
F 1 "GND" V 8425 5525 50  0000 C CNN
F 2 "" H 8425 5725 50  0001 C CNN
F 3 "" H 8425 5725 50  0001 C CNN
	1    8425 5725
	0    1    1    0   
$EndComp
Text Label 8425 5825 0    39   ~ 0
D+
$Comp
L power:GND #PWR?
U 1 1 5D30D06E
P 8425 6125
AR Path="/5D30D06E" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D06E" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 8425 5875 50  0001 C CNN
F 1 "GND" V 8425 5925 50  0000 C CNN
F 2 "" H 8425 6125 50  0001 C CNN
F 3 "" H 8425 6125 50  0001 C CNN
	1    8425 6125
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5D30D074
P 8425 6225
AR Path="/5D30D074" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D30D074" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 8425 6075 50  0001 C CNN
F 1 "+3V3" V 8425 6425 50  0000 C CNN
F 2 "" H 8425 6225 50  0001 C CNN
F 3 "" H 8425 6225 50  0001 C CNN
	1    8425 6225
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8875 4325 8425 4325
Wire Wire Line
	8875 4425 8425 4425
Wire Wire Line
	8875 4525 8425 4525
Wire Wire Line
	8875 4625 8425 4625
Wire Wire Line
	8875 4725 8425 4725
Wire Wire Line
	8875 4825 8425 4825
Wire Wire Line
	8875 4925 8425 4925
Wire Wire Line
	8875 5025 8425 5025
Wire Wire Line
	8875 5125 8425 5125
Wire Wire Line
	8875 5225 8425 5225
Wire Wire Line
	8875 5325 8425 5325
Wire Wire Line
	8875 5425 8425 5425
Wire Wire Line
	8875 5525 8425 5525
Wire Wire Line
	8875 5625 8425 5625
Wire Wire Line
	8875 5725 8425 5725
Wire Wire Line
	8875 5825 8425 5825
Wire Wire Line
	8875 5925 8425 5925
Wire Wire Line
	8875 6025 8425 6025
Wire Wire Line
	8875 6125 8425 6125
Wire Wire Line
	8875 6225 8425 6225
Wire Notes Line
	7575 3925 7575 5875
Wire Notes Line
	5575 3925 5575 5875
Text Notes 5625 4050 0    59   ~ 0
BUTTONS PCB
Wire Notes Line
	8000 6375 8000 3925
Wire Notes Line
	8000 3925 9375 3925
Wire Notes Line
	9375 3925 9375 6375
Wire Notes Line
	8000 6375 9375 6375
Text Notes 8050 4050 0    59   ~ 0
FPC/FFC CONNECTOR
Wire Wire Line
	3650 2225 3700 2225
Wire Wire Line
	3700 2225 3700 1750
Wire Wire Line
	3700 1750 4900 1750
Wire Notes Line
	2500 1475 5300 1475
Wire Notes Line
	5300 1475 5300 3500
Wire Notes Line
	5300 3500 2500 3500
Wire Notes Line
	2500 1475 2500 3500
Wire Notes Line
	6025 1475 9850 1475
Wire Notes Line
	9850 1475 9850 3500
Wire Notes Line
	9850 3500 6025 3500
Wire Notes Line
	6025 3500 6025 1475
Wire Wire Line
	9275 2475 9300 2475
Wire Wire Line
	9350 2575 9750 2575
Text Notes 2550 1600 0    59   ~ 0
USB 2.0 FOR ROM BOOTLOADER
Text Notes 6100 1600 0    59   ~ 0
SDCARD
Wire Wire Line
	9300 2225 9300 2475
Connection ~ 9300 2475
Wire Wire Line
	9300 2475 9750 2475
Wire Wire Line
	7050 2275 7475 2275
Text Notes 7050 6975 0    50   ~ 0
Autor: JGReyes
Text Notes 7025 6750 0    118  ~ 0
WWW.CIRCUITEANDO.NET
Wire Wire Line
	9575 1925 9575 1875
Wire Wire Line
	9400 1875 9575 1875
Wire Wire Line
	9575 2225 9575 2375
Wire Wire Line
	9350 2375 9575 2375
Wire Wire Line
	7175 1775 7175 1825
Wire Wire Line
	7175 1775 7300 1775
$Comp
L power:+3V3 #PWR?
U 1 1 5D045A8B
P 7300 1725
AR Path="/5D045A8B" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D045A8B" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 7300 1575 50  0001 C CNN
F 1 "+3V3" H 7300 1875 39  0000 C CNN
F 2 "" H 7300 1725 50  0001 C CNN
F 3 "" H 7300 1725 50  0001 C CNN
	1    7300 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1725 7300 1775
Connection ~ 7300 1775
Wire Wire Line
	7300 1775 7450 1775
Wire Wire Line
	3125 2225 3225 2225
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5CC0F359
P 3225 2225
AR Path="/5D283494/5CC0F359" Ref="#FLG?"  Part="1" 
AR Path="/5CC0F359" Ref="#FLG?"  Part="1" 
AR Path="/5CEB517F/5CC0F359" Ref="#FLG0110"  Part="1" 
F 0 "#FLG0110" H 3225 2300 50  0001 C CNN
F 1 "PWR_FLAG" H 3225 2350 20  0000 C CNN
F 2 "" H 3225 2225 50  0001 C CNN
F 3 "~" H 3225 2225 50  0001 C CNN
	1    3225 2225
	-1   0    0    1   
$EndComp
Connection ~ 3225 2225
Wire Wire Line
	3225 2225 3250 2225
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 5CC722AC
P 5900 4925
F 0 "J3" H 5950 5050 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5950 5051 50  0001 C CNN
F 2 "DC_Load_NewBrain:PinHeader_2x02_P2.54mm_Vertical_SMD" H 5900 4925 50  0001 C CNN
F 3 "~" H 5900 4925 50  0001 C CNN
	1    5900 4925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4825 6200 4925
Wire Wire Line
	6200 5200 6200 5025
$Comp
L power:+3V3 #PWR046
U 1 1 5CC976E9
P 5700 4775
F 0 "#PWR046" H 5700 4625 50  0001 C CNN
F 1 "+3V3" H 5700 4925 39  0000 C CNN
F 2 "" H 5700 4775 50  0001 C CNN
F 3 "" H 5700 4775 50  0001 C CNN
	1    5700 4775
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR077
U 1 1 5CC977B1
P 5700 5175
F 0 "#PWR077" H 5700 4925 50  0001 C CNN
F 1 "GND" H 5705 5002 50  0000 C CNN
F 2 "" H 5700 5175 50  0001 C CNN
F 3 "" H 5700 5175 50  0001 C CNN
	1    5700 5175
	1    0    0    -1  
$EndComp
Wire Notes Line
	5575 3925 7575 3925
Wire Notes Line
	5575 5875 7575 5875
Wire Wire Line
	5700 4775 5700 4925
Wire Wire Line
	5700 5175 5700 5025
$Comp
L power:GND #PWR?
U 1 1 5CF1F420
P 8425 5925
AR Path="/5CF1F420" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5CF1F420" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 8425 5675 50  0001 C CNN
F 1 "GND" V 8425 5725 50  0000 C CNN
F 2 "" H 8425 5925 50  0001 C CNN
F 3 "" H 8425 5925 50  0001 C CNN
	1    8425 5925
	0    1    1    0   
$EndComp
Wire Wire Line
	8425 6025 8425 5925
Connection ~ 8425 5925
$Comp
L Device:R R27
U 1 1 5D350260
P 4900 3050
F 0 "R27" V 4975 3050 50  0000 C CNN
F 1 "470" V 4900 3050 39  0000 C CNN
F 2 "DC_Load_NewBrain:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4830 3050 50  0001 C CNN
F 3 "~" H 4900 3050 50  0001 C CNN
	1    4900 3050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D350541
P 4900 3250
AR Path="/5D350541" Ref="#PWR?"  Part="1" 
AR Path="/5CEB517F/5D350541" Ref="#PWR084"  Part="1" 
F 0 "#PWR084" H 4900 3000 50  0001 C CNN
F 1 "GND" H 4905 3077 50  0000 C CNN
F 2 "" H 4900 3250 50  0001 C CNN
F 3 "" H 4900 3250 50  0001 C CNN
	1    4900 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3200 4900 3250
Wire Wire Line
	4900 2900 4900 2525
$EndSCHEMATC
