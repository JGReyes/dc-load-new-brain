EESchema Schematic File Version 4
LIBS:DC_Load_NewBrain-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "DC Load NewBrain"
Date "18/04/2019"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4500 1125 0    60   ~ 0
DC Load Shield PCB
$Comp
L w_connectors:Arduino_Nano_Header J?
U 1 1 5D32A662
P 5450 2275
AR Path="/5D32A662" Ref="J?"  Part="1" 
AR Path="/5D283494/5D32A662" Ref="J8"  Part="1" 
F 0 "J8" H 5450 3212 60  0000 C CNN
F 1 "Arduino_Nano_Header" H 5450 3106 60  0000 C CNN
F 2 "DC_Load_NewBrain:arduino_nano_header" H 5450 2275 60  0001 C CNN
F 3 "" H 5450 2275 60  0000 C CNN
	1    5450 2275
	1    0    0    -1  
$EndComp
NoConn ~ 5800 1575
NoConn ~ 5800 1775
NoConn ~ 5800 1975
NoConn ~ 5800 2775
NoConn ~ 5800 2875
NoConn ~ 5100 1575
NoConn ~ 5100 1675
NoConn ~ 5100 1775
$Comp
L power:GND #PWR?
U 1 1 5D32A671
P 6250 1675
AR Path="/5D32A671" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A671" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 6250 1425 50  0001 C CNN
F 1 "GND" V 6255 1547 50  0000 R CNN
F 2 "" H 6250 1675 50  0001 C CNN
F 3 "" H 6250 1675 50  0001 C CNN
	1    6250 1675
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A677
P 4850 1875
AR Path="/5D32A677" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A677" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 4850 1625 50  0001 C CNN
F 1 "GND" V 4855 1747 50  0000 R CNN
F 2 "" H 4850 1875 50  0001 C CNN
F 3 "" H 4850 1875 50  0001 C CNN
	1    4850 1875
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D32A67D
P 6250 1875
AR Path="/5D32A67D" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A67D" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 6250 1725 50  0001 C CNN
F 1 "+5V" V 6265 2003 50  0000 L CNN
F 2 "" H 6250 1875 50  0001 C CNN
F 3 "" H 6250 1875 50  0001 C CNN
	1    6250 1875
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 1875 5950 1875
Wire Wire Line
	5100 1875 4850 1875
Wire Wire Line
	5800 2075 6025 2075
Text Label 6025 2075 2    39   ~ 0
TEMP1
Wire Wire Line
	5800 2175 6025 2175
Wire Wire Line
	5800 2275 6025 2275
Wire Wire Line
	5800 2375 6025 2375
Wire Wire Line
	5800 2475 6025 2475
Wire Wire Line
	5800 2575 6025 2575
Wire Wire Line
	5800 2675 6025 2675
Wire Wire Line
	5800 2975 6025 2975
Text Label 6025 2175 2    39   ~ 0
SCL
Text Label 6025 2275 2    39   ~ 0
SDA
Text Label 6025 2375 2    39   ~ 0
SW2
Text Label 6025 2475 2    39   ~ 0
SW3
Text Label 6025 2575 2    39   ~ 0
TEMP2
Text Label 6025 2675 2    39   ~ 0
RLY_ON
Text Label 6025 2975 2    39   ~ 0
TRIGGER
Wire Wire Line
	5100 1975 4825 1975
Wire Wire Line
	5100 2075 4825 2075
Wire Wire Line
	5100 2175 4825 2175
Wire Wire Line
	5100 2275 4825 2275
Wire Wire Line
	5100 2375 4825 2375
Wire Wire Line
	5100 2475 4825 2475
Wire Wire Line
	5100 2575 4825 2575
Wire Wire Line
	5100 2675 4825 2675
Wire Wire Line
	5100 2775 4825 2775
Wire Wire Line
	5100 2875 4825 2875
Wire Wire Line
	5100 2975 4825 2975
Text Label 4825 1975 0    39   ~ 0
ENC_B
Text Label 4825 2075 0    39   ~ 0
FAN
Text Label 4825 2175 0    39   ~ 0
ENC_A
Text Label 4825 2275 0    39   ~ 0
KP_8
Text Label 4825 2375 0    39   ~ 0
KP_7
Text Label 4825 2475 0    39   ~ 0
KP_6
Text Label 4825 2575 0    39   ~ 0
KP_5
Text Label 4825 2675 0    39   ~ 0
KP_4
Text Label 4825 2775 0    39   ~ 0
KP_3
Text Label 4825 2875 0    39   ~ 0
KP_2
Text Label 4825 2975 0    39   ~ 0
KP_1
$Comp
L Connector_Generic:Conn_01x30 J?
U 1 1 5D32A6AC
P 5800 4575
AR Path="/5D32A6AC" Ref="J?"  Part="1" 
AR Path="/5D283494/5D32A6AC" Ref="J9"  Part="1" 
F 0 "J9" H 5880 4567 50  0000 L CNN
F 1 "FPC/FFC" H 5880 4476 39  0000 L CNN
F 2 "DC_Load_NewBrain:Omron_XF2M-3015_1x30_P0.50mm_Horizontal" H 5800 4575 50  0001 C CNN
F 3 "~" H 5800 4575 50  0001 C CNN
	1    5800 4575
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D32A6B3
P 5050 3175
AR Path="/5D32A6B3" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A6B3" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 5050 3025 50  0001 C CNN
F 1 "+5V" V 5065 3303 50  0000 L CNN
F 2 "" H 5050 3175 50  0001 C CNN
F 3 "" H 5050 3175 50  0001 C CNN
	1    5050 3175
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D32A6B9
P 5050 6075
AR Path="/5D32A6B9" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A6B9" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 5050 5925 50  0001 C CNN
F 1 "+5V" V 5065 6203 50  0000 L CNN
F 2 "" H 5050 6075 50  0001 C CNN
F 3 "" H 5050 6075 50  0001 C CNN
	1    5050 6075
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A6BF
P 5050 3275
AR Path="/5D32A6BF" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A6BF" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 5050 3025 50  0001 C CNN
F 1 "GND" V 5055 3147 50  0000 R CNN
F 2 "" H 5050 3275 50  0001 C CNN
F 3 "" H 5050 3275 50  0001 C CNN
	1    5050 3275
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3175 5050 3175
Wire Wire Line
	5600 3275 5050 3275
Wire Wire Line
	5600 3575 5050 3575
Wire Wire Line
	5600 3675 5050 3675
Wire Wire Line
	5600 3775 5050 3775
Wire Wire Line
	5600 3875 5050 3875
Wire Wire Line
	5600 3975 5050 3975
Wire Wire Line
	5600 4075 5050 4075
Wire Wire Line
	5600 4175 5050 4175
Wire Wire Line
	5600 4375 5050 4375
Wire Wire Line
	5600 4475 5050 4475
Wire Wire Line
	5600 4575 5050 4575
Wire Wire Line
	5600 4675 5050 4675
Wire Wire Line
	5600 4775 5050 4775
Wire Wire Line
	5600 4875 5050 4875
Wire Wire Line
	5600 4975 5050 4975
Wire Wire Line
	5600 5075 5050 5075
Wire Wire Line
	5600 5175 5050 5175
Wire Wire Line
	5600 5275 5050 5275
Wire Wire Line
	5600 5475 5050 5475
Wire Wire Line
	5600 5575 5050 5575
Wire Wire Line
	5600 5675 5050 5675
Wire Wire Line
	5600 5775 5050 5775
Wire Wire Line
	5600 5875 5050 5875
Wire Wire Line
	5600 5975 5050 5975
Wire Wire Line
	5600 6075 5050 6075
Text Label 5050 3475 0    39   ~ 0
KP_8
Text Label 5050 3575 0    39   ~ 0
KP_7
Text Label 5050 3675 0    39   ~ 0
KP_6
Text Label 5050 3775 0    39   ~ 0
KP_5
Text Label 5050 3875 0    39   ~ 0
KP_4
Text Label 5050 3975 0    39   ~ 0
KP_3
Text Label 5050 4075 0    39   ~ 0
KP_2
Text Label 5050 4175 0    39   ~ 0
KP_1
$Comp
L power:GND #PWR?
U 1 1 5D32A6EB
P 5050 5975
AR Path="/5D32A6EB" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A6EB" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 5050 5725 50  0001 C CNN
F 1 "GND" V 5055 5847 50  0000 R CNN
F 2 "" H 5050 5975 50  0001 C CNN
F 3 "" H 5050 5975 50  0001 C CNN
	1    5050 5975
	0    1    1    0   
$EndComp
Text Label 5050 5875 0    39   ~ 0
TRIGGER
Text Label 5050 4375 0    39   ~ 0
SCL
Text Label 5050 4575 0    39   ~ 0
SDA
Text Label 5050 4775 0    39   ~ 0
TEMP1
Text Label 5050 4975 0    39   ~ 0
TEMP2
Text Label 5050 5175 0    39   ~ 0
SW2
Text Label 5050 5275 0    39   ~ 0
SW3
Text Label 5050 3375 0    39   ~ 0
FAN
Text Label 5050 5475 0    39   ~ 0
RLY_ON
Text Label 5050 5575 0    39   ~ 0
ENC_B
Text Label 5050 5675 0    39   ~ 0
ENC_A
$Comp
L power:GND #PWR?
U 1 1 5D32A6FC
P 5050 5775
AR Path="/5D32A6FC" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A6FC" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 5050 5525 50  0001 C CNN
F 1 "GND" V 5055 5647 50  0000 R CNN
F 2 "" H 5050 5775 50  0001 C CNN
F 3 "" H 5050 5775 50  0001 C CNN
	1    5050 5775
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A702
P 5050 4275
AR Path="/5D32A702" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A702" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 5050 4025 50  0001 C CNN
F 1 "GND" V 5055 4147 50  0000 R CNN
F 2 "" H 5050 4275 50  0001 C CNN
F 3 "" H 5050 4275 50  0001 C CNN
	1    5050 4275
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A708
P 5050 4475
AR Path="/5D32A708" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A708" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 5050 4225 50  0001 C CNN
F 1 "GND" V 5055 4347 50  0000 R CNN
F 2 "" H 5050 4475 50  0001 C CNN
F 3 "" H 5050 4475 50  0001 C CNN
	1    5050 4475
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A70E
P 5050 4675
AR Path="/5D32A70E" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A70E" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 5050 4425 50  0001 C CNN
F 1 "GND" V 5055 4547 50  0000 R CNN
F 2 "" H 5050 4675 50  0001 C CNN
F 3 "" H 5050 4675 50  0001 C CNN
	1    5050 4675
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A714
P 5050 4875
AR Path="/5D32A714" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A714" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 5050 4625 50  0001 C CNN
F 1 "GND" V 5055 4747 50  0000 R CNN
F 2 "" H 5050 4875 50  0001 C CNN
F 3 "" H 5050 4875 50  0001 C CNN
	1    5050 4875
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D32A71A
P 5050 5075
AR Path="/5D32A71A" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D32A71A" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 5050 4825 50  0001 C CNN
F 1 "GND" V 5055 4947 50  0000 R CNN
F 2 "" H 5050 5075 50  0001 C CNN
F 3 "" H 5050 5075 50  0001 C CNN
	1    5050 5075
	0    1    1    0   
$EndComp
Wire Notes Line
	6625 1000 6625 6175
Wire Notes Line
	4450 6175 4450 1000
Text Notes 7050 6975 0    50   ~ 0
Autor: JGReyes
Text Notes 7025 6750 0    118  ~ 0
WWW.CIRCUITEANDO.NET
Wire Wire Line
	5600 3375 5050 3375
Wire Wire Line
	5600 3475 5050 3475
Wire Wire Line
	5600 4275 5050 4275
$Comp
L power:+5V #PWR?
U 1 1 5D0434E6
P 5050 5375
AR Path="/5D0434E6" Ref="#PWR?"  Part="1" 
AR Path="/5D283494/5D0434E6" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 5050 5225 50  0001 C CNN
F 1 "+5V" V 5065 5503 50  0000 L CNN
F 2 "" H 5050 5375 50  0001 C CNN
F 3 "" H 5050 5375 50  0001 C CNN
	1    5050 5375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 5375 5050 5375
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D0FE41A
P 5950 1875
F 0 "#FLG0101" H 5950 1950 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 2000 20  0000 C CNN
F 2 "" H 5950 1875 50  0001 C CNN
F 3 "~" H 5950 1875 50  0001 C CNN
	1    5950 1875
	1    0    0    -1  
$EndComp
Connection ~ 5950 1875
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5D32552D
P 5950 1675
F 0 "#FLG0103" H 5950 1750 50  0001 C CNN
F 1 "PWR_FLAG" H 5950 1800 20  0000 C CNN
F 2 "" H 5950 1675 50  0001 C CNN
F 3 "~" H 5950 1675 50  0001 C CNN
	1    5950 1675
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1675 5950 1675
Connection ~ 5950 1675
$Comp
L Device:CP_Small C24
U 1 1 5D067EA9
P 6100 1775
F 0 "C24" H 6025 1750 39  0000 R CNN
F 1 "1000uF" H 6025 1825 39  0000 R CNN
F 2 "DC_Load_NewBrain:CP_Elec_10x10" H 6100 1775 50  0001 C CNN
F 3 "~" H 6100 1775 50  0001 C CNN
	1    6100 1775
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 1675 6100 1675
Wire Wire Line
	5950 1875 6100 1875
Connection ~ 6100 1875
Wire Wire Line
	6100 1875 6250 1875
Connection ~ 6100 1675
Wire Wire Line
	6100 1675 6250 1675
Wire Notes Line
	4450 1000 6625 1000
Wire Notes Line
	4450 6175 6625 6175
$EndSCHEMATC
