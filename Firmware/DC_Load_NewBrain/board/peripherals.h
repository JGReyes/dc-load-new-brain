/***********************************************************************************************************************
 * This file was generated by the MCUXpresso Config Tools. Any manual edits made to this file
 * will be overwritten if the respective MCUXpresso Config Tools is used to update this file.
 **********************************************************************************************************************/

#ifndef _PERIPHERALS_H_
#define _PERIPHERALS_H_

/***********************************************************************************************************************
 * Included files
 **********************************************************************************************************************/
#include "fsl_common.h"
#include "fsl_spi.h"
#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"
#include "fsl_adc16.h"
#include "fsl_lptmr.h"
#include "fsl_gpio.h"
#include "fsl_port.h"
#include "fsl_clock.h"
#include "fsl_tpm.h"
#include "fsl_pit.h"
#include "fsl_rtc.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/***********************************************************************************************************************
 * Definitions
 **********************************************************************************************************************/
/* Definitions for BOARD_InitPeripherals functional group */
/* BOARD_InitPeripherals defines for SPI1 */
/* Definition of peripheral ID */
#define SPI_1_PERIPHERAL SPI1
/* Definition of the clock source */
#define SPI_1_CLOCK_SOURCE SPI1_CLK_SRC
/* Definition of the clock source frequency */
#define SPI_1_CLK_FREQ 48000000UL
/* BOARD_InitPeripherals defines for I2C0 */
/* Definition of peripheral ID */
#define I2C_0_PERIPHERAL I2C0
/* Definition of the clock source */
#define I2C_0_CLOCK_SOURCE I2C0_CLK_SRC
/* Definition of the clock source frequency */
#define I2C_0_CLK_FREQ CLOCK_GetFreq(I2C_0_CLOCK_SOURCE)
/* Alias for ADC0 peripheral */
#define ADC16_0_PERIPHERAL ADC0
/* ADC16_0 interrupt vector ID (number). */
#define ADC16_0_IRQN ADC0_IRQn
/* ADC16_0 interrupt handler identifier. */
#define ADC16_0_IRQHANDLER ADC0_IRQHandler
/* BOARD_InitPeripherals defines for LPTMR0 */
/* Definition of peripheral ID */
#define LPTMR_0_PERIPHERAL LPTMR0
/* Definition of the clock source frequency */
#define LPTMR_0_CLK_FREQ 1000000UL
/* Definition of the pulses (ticks) count to compare */
#define LPTMR_0_PULSES_COUNT 1UL
/* LPTMR_0 interrupt vector ID (number). */
#define LPTMR_0_IRQN LPTMR0_IRQn
/* LPTMR_0 interrupt handler identifier. */
#define Encoder_Handler LPTMR0_IRQHandler
/* Alias for GPIOB peripheral */
#define GPIO_B_GPIO GPIOB
/* Alias for GPIOC peripheral */
#define GPIO_C_GPIO GPIOC
/* Alias for PORTC */
#define GPIO_C_PORT PORTC
/* GPIO_C interrupt vector ID (number). */
#define GPIO_C_IRQN PORTB_PORTC_PORTD_PORTE_IRQn
/* GPIO_C interrupt handler identifier. */
#define GPIO_C_IRQHANDLER PORTB_PORTC_PORTD_PORTE_IRQHandler
/* Alias for GPIOA peripheral */
#define GPIO_A_GPIO GPIOA
/* Alias for PORTA */
#define GPIO_A_PORT PORTA
/* GPIO_A interrupt vector ID (number). */
#define GPIO_A_IRQN PORTA_IRQn
/* GPIO_A interrupt handler identifier. */
#define GPIO_A_IRQHANDLER PORTA_IRQHandler
/* Alias for GPIOE peripheral */
#define GPIO_E_GPIO GPIOE
/* Definition of peripheral ID */
#define TPM_0_PERIPHERAL TPM0
/* Definition of the clock source frequency */
#define TPM_0_CLOCK_SOURCE 1000000UL
/* TPM_0 interrupt vector ID (number). */
#define TPM_0_IRQN TPM0_IRQn
/* TPM_0 interrupt handler identifier. */
#define TPM_0_IRQHANDLER TPM0_IRQHandler
/* Definition of peripheral ID */
#define TPM_1_PERIPHERAL TPM1
/* Definition of the clock source frequency */
#define TPM_1_CLOCK_SOURCE 1000000UL
/* TPM_1 interrupt vector ID (number). */
#define TPM_1_IRQN TPM1_IRQn
/* TPM_1 interrupt handler identifier. */
#define TPM_1_IRQHANDLER TPM1_IRQHandler
/* BOARD_InitPeripherals defines for PIT */
/* Definition of peripheral ID. */
#define PIT_1_PERIPHERAL PIT
/* Definition of clock source. */
#define PIT_1_CLOCK_SOURCE kCLOCK_BusClk
/* Definition of clock source frequency. */
#define PIT_1_CLK_FREQ 24000000UL
/* Definition of ticks count for channel 0. */
#define PIT_1_0_TICKS 6239U
/* Definition of ticks count for channel 1. */
#define PIT_1_1_TICKS 2639U
/* PIT_1 interrupt vector ID (number). */
#define PIT_1_IRQN PIT_IRQn
/* PIT_1 interrupt handler identifier. */
#define PIT_1_IRQHANDLER PIT_IRQHandler
/* Definition of peripheral ID */
#define TPM_2_PERIPHERAL TPM2
/* Definition of the clock source frequency */
#define TPM_2_CLOCK_SOURCE 1000000UL
/* TPM_2 interrupt vector ID (number). */
#define TPM_2_IRQN TPM2_IRQn
/* TPM_2 interrupt handler identifier. */
#define TPM_2_IRQHANDLER TPM2_IRQHandler
/* Definition of peripheral ID */
#define RTC_1_PERIPHERAL RTC
/* RTC_1 interrupt vector ID (number). */
#define RTC_1_SECONDS_IRQN RTC_Seconds_IRQn
/* RTC_1 interrupt handler identifier. */
#define RTC_1_SECONDS_IRQHANDLER RTC_Seconds_IRQHandler
/* Alias for GPIOD peripheral */
#define GPIO_D_GPIO GPIOD

/***********************************************************************************************************************
 * Global variables
 **********************************************************************************************************************/
extern const spi_master_config_t SPI_1_config;
extern i2c_rtos_handle_t I2C_0_rtosHandle;
extern const i2c_master_config_t I2C_0_config;
extern adc16_channel_config_t ADC16_0_channelsConfig[2];
extern const adc16_config_t ADC16_0_config;
extern const adc16_channel_mux_mode_t ADC16_0_muxMode;
extern const adc16_hardware_average_mode_t ADC16_0_hardwareAverageMode;
extern const lptmr_config_t LPTMR_0_config;
extern const tpm_config_t TPM_0_config;
extern const tpm_config_t TPM_1_config;
extern const pit_config_t PIT_1_config;
extern const tpm_config_t TPM_2_config;
extern const rtc_config_t RTC_1_config;
/* Date and time structure */
extern rtc_datetime_t RTC_1_dateTimeStruct;

/***********************************************************************************************************************
 * Initialization functions
 **********************************************************************************************************************/
void BOARD_InitPeripherals(void);

/***********************************************************************************************************************
 * BOARD_InitBootPeripherals function
 **********************************************************************************************************************/
void BOARD_InitBootPeripherals(void);

#if defined(__cplusplus)
}
#endif

#endif /* _PERIPHERALS_H_ */
