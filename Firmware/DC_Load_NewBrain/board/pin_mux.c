/***********************************************************************************************************************
 * This file was generated by the MCUXpresso Config Tools. Any manual edits made to this file
 * will be overwritten if the respective MCUXpresso Config Tools is used to update this file.
 **********************************************************************************************************************/

/* clang-format off */
/*
 * TEXT BELOW IS USED AS SETTING FOR TOOLS *************************************
!!GlobalInfo
product: Pins v6.0
processor: MKL27Z64xxx4
package_id: MKL27Z64VLH4
mcu_data: ksdk2_0
processor_version: 6.0.1
pin_labels:
- {pin_num: '37', pin_signal: ADC0_SE12/PTB2/I2C0_SCL/TPM2_CH0, label: SD_PROTECT, identifier: SD_PROTECT}
- {pin_num: '38', pin_signal: ADC0_SE13/PTB3/I2C0_SDA/TPM2_CH1, label: SD_DETECT, identifier: SD_DETECT}
- {pin_num: '41', pin_signal: PTB18/TPM2_CH0, label: VBAT_DETECT, identifier: VBAT_DETECT}
- {pin_num: '10', pin_signal: ADC0_DM0/ADC0_SE4a/PTE21/TPM1_CH1/LPUART0_RX/FXIO0_D5, label: KP_8, identifier: KP_8}
- {pin_num: '11', pin_signal: ADC0_DP3/ADC0_SE3/PTE22/TPM2_CH0/UART2_TX/FXIO0_D6, label: KP_7, identifier: KP_7}
- {pin_num: '12', pin_signal: ADC0_DM3/ADC0_SE7a/PTE23/TPM2_CH1/UART2_RX/FXIO0_D7, label: KP_6, identifier: KP_6}
- {pin_num: '20', pin_signal: PTE24/TPM0_CH0/I2C0_SCL, label: KP_5, identifier: KP_5}
- {pin_num: '21', pin_signal: PTE25/TPM0_CH1/I2C0_SDA, label: KP_4, identifier: KP_4}
- {pin_num: '17', pin_signal: CMP0_IN5/ADC0_SE4b/PTE29/TPM0_CH2/TPM_CLKIN0, label: KP_3, identifier: KP_3}
- {pin_num: '18', pin_signal: ADC0_SE23/CMP0_IN4/PTE30/TPM0_CH3/TPM_CLKIN1/LPUART1_TX/LPTMR0_ALT1, label: KP_2, identifier: KP_2}
- {pin_num: '19', pin_signal: PTE31/TPM0_CH4, label: KP_1, identifier: KP_1}
- {pin_num: '44', pin_signal: ADC0_SE15/PTC1/LLWU_P6/RTC_CLKIN/I2C1_SCL/TPM0_CH0, label: TEMP1, identifier: TEMP1}
- {pin_num: '45', pin_signal: ADC0_SE11/PTC2/I2C1_SDA/TPM0_CH1, label: TEMP2, identifier: TEMP2}
- {pin_num: '51', pin_signal: CMP0_IN0/PTC6/LLWU_P10/SPI0_MOSI/EXTRG_IN/SPI0_MISO, label: ENC_A, identifier: ENC_A}
- {pin_num: '50', pin_signal: PTC5/LLWU_P9/SPI0_SCK/LPTMR0_ALT2/CMP0_OUT, label: ENC_B, identifier: ENC_B}
- {pin_num: '52', pin_signal: CMP0_IN1/PTC7/SPI0_MISO/USB_SOF_OUT/SPI0_MOSI, label: TRIGGER, identifier: TRIGGER}
- {pin_num: '53', pin_signal: CMP0_IN2/PTC8/I2C0_SCL/TPM0_CH4, label: BUZZER, identifier: BUZZER}
- {pin_num: '54', pin_signal: CMP0_IN3/PTC9/I2C0_SDA/TPM0_CH5, label: FAN, identifier: FAN}
- {pin_num: '55', pin_signal: PTC10/I2C1_SCL, label: RLY, identifier: RLY}
- {pin_num: '27', pin_signal: PTA5/USB_CLKIN/TPM0_CH2, label: LOAD, identifier: LOAD}
- {pin_num: '28', pin_signal: PTA12/TPM1_CH0, label: CURSOR, identifier: CURSOR}
- {pin_num: '43', pin_signal: ADC0_SE14/PTC0/EXTRG_IN/USB_SOF_OUT/CMP0_OUT, label: VBUS_DETECT, identifier: VBUS_DETECT}
- {pin_num: '26', pin_signal: PTA4/I2C1_SDA/TPM0_CH1/NMI_b, label: BOOT, identifier: BOOT}
- {pin_num: '59', pin_signal: PTD2/SPI0_MOSI/UART2_RX/TPM0_CH2/SPI0_MISO/FXIO0_D2, label: LS_OE, identifier: LS_;LS_OE}
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR TOOLS ***********
 */
/* clang-format on */

#include "fsl_common.h"
#include "fsl_port.h"
#include "fsl_gpio.h"
#include "pin_mux.h"

/* FUNCTION ************************************************************************************************************
 *
 * Function Name : BOARD_InitBootPins
 * Description   : Calls initialization functions.
 *
 * END ****************************************************************************************************************/
void BOARD_InitBootPins(void)
{
    BOARD_InitPins();
}

/* clang-format off */
/*
 * TEXT BELOW IS USED AS SETTING FOR TOOLS *************************************
BOARD_InitPins:
- options: {callFromInitBoot: 'true', coreID: core0, enableClock: 'true'}
- pin_list:
  - {pin_num: '34', peripheral: RCM, signal: RESET, pin_signal: PTA20/RESET_b}
  - {pin_num: '32', peripheral: OSC0, signal: EXTAL0, pin_signal: EXTAL0/PTA18/LPUART1_RX/TPM_CLKIN0}
  - {pin_num: '33', peripheral: OSC0, signal: XTAL0, pin_signal: XTAL0/PTA19/LPUART1_TX/TPM_CLKIN1/LPTMR0_ALT1}
  - {pin_num: '39', peripheral: SPI1, signal: MOSI, pin_signal: PTB16/SPI1_MOSI/LPUART0_RX/TPM_CLKIN0/SPI1_MISO}
  - {pin_num: '40', peripheral: SPI1, signal: MISO, pin_signal: PTB17/SPI1_MISO/LPUART0_TX/TPM_CLKIN1/SPI1_MOSI}
  - {pin_num: '46', peripheral: SPI1, signal: SCK, pin_signal: PTC3/LLWU_P7/SPI1_SCK/LPUART1_RX/TPM0_CH2/CLKOUT}
  - {pin_num: '49', peripheral: SPI1, signal: PCS0, pin_signal: PTC4/LLWU_P8/SPI0_PCS0/LPUART1_TX/TPM0_CH3/SPI1_PCS0}
  - {pin_num: '37', peripheral: GPIOB, signal: 'GPIO, 2', pin_signal: ADC0_SE12/PTB2/I2C0_SCL/TPM2_CH0, direction: INPUT, gpio_interrupt: kPORT_InterruptEitherEdge}
  - {pin_num: '38', peripheral: GPIOB, signal: 'GPIO, 3', pin_signal: ADC0_SE13/PTB3/I2C0_SDA/TPM2_CH1, direction: INPUT, gpio_interrupt: kPORT_InterruptEitherEdge}
  - {pin_num: '41', peripheral: GPIOB, signal: 'GPIO, 18', pin_signal: PTB18/TPM2_CH0, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled, pull_select: down,
    pull_enable: enable}
  - {pin_num: '35', peripheral: I2C0, signal: SCL, pin_signal: ADC0_SE8/PTB0/LLWU_P5/I2C0_SCL/TPM1_CH0/SPI1_MOSI/SPI1_MISO}
  - {pin_num: '36', peripheral: I2C0, signal: SDA, pin_signal: ADC0_SE9/PTB1/I2C0_SDA/TPM1_CH1/SPI1_MISO/SPI1_MOSI}
  - {pin_num: '10', peripheral: GPIOE, signal: 'GPIO, 21', pin_signal: ADC0_DM0/ADC0_SE4a/PTE21/TPM1_CH1/LPUART0_RX/FXIO0_D5, direction: OUTPUT, gpio_init_state: 'true'}
  - {pin_num: '11', peripheral: GPIOE, signal: 'GPIO, 22', pin_signal: ADC0_DP3/ADC0_SE3/PTE22/TPM2_CH0/UART2_TX/FXIO0_D6, direction: OUTPUT, gpio_init_state: 'true'}
  - {pin_num: '12', peripheral: GPIOE, signal: 'GPIO, 23', pin_signal: ADC0_DM3/ADC0_SE7a/PTE23/TPM2_CH1/UART2_RX/FXIO0_D7, direction: OUTPUT, gpio_init_state: 'true'}
  - {pin_num: '20', peripheral: GPIOE, signal: 'GPIO, 24', pin_signal: PTE24/TPM0_CH0/I2C0_SCL, direction: OUTPUT, gpio_init_state: 'true'}
  - {pin_num: '21', peripheral: GPIOE, signal: 'GPIO, 25', pin_signal: PTE25/TPM0_CH1/I2C0_SDA, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled, pull_select: down,
    pull_enable: enable}
  - {pin_num: '17', peripheral: GPIOE, signal: 'GPIO, 29', pin_signal: CMP0_IN5/ADC0_SE4b/PTE29/TPM0_CH2/TPM_CLKIN0, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled,
    pull_select: down, pull_enable: enable}
  - {pin_num: '18', peripheral: GPIOE, signal: 'GPIO, 30', pin_signal: ADC0_SE23/CMP0_IN4/PTE30/TPM0_CH3/TPM_CLKIN1/LPUART1_TX/LPTMR0_ALT1, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled,
    pull_select: down, pull_enable: enable}
  - {pin_num: '19', peripheral: GPIOE, signal: 'GPIO, 31', pin_signal: PTE31/TPM0_CH4, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled, pull_select: down,
    pull_enable: enable, passive_filter: disable}
  - {pin_num: '44', peripheral: ADC0, signal: 'SE, 15', pin_signal: ADC0_SE15/PTC1/LLWU_P6/RTC_CLKIN/I2C1_SCL/TPM0_CH0}
  - {pin_num: '45', peripheral: ADC0, signal: 'SE, 11', pin_signal: ADC0_SE11/PTC2/I2C1_SDA/TPM0_CH1}
  - {pin_num: '52', peripheral: GPIOC, signal: 'GPIO, 7', pin_signal: CMP0_IN1/PTC7/SPI0_MISO/USB_SOF_OUT/SPI0_MOSI, direction: INPUT, gpio_interrupt: kPORT_InterruptOrDMADisabled,
    slew_rate: slow}
  - {pin_num: '27', peripheral: GPIOA, signal: 'GPIO, 5', pin_signal: PTA5/USB_CLKIN/TPM0_CH2, direction: INPUT, gpio_interrupt: kPORT_InterruptFallingEdge, pull_enable: enable,
    passive_filter: disable}
  - {pin_num: '28', peripheral: GPIOA, signal: 'GPIO, 12', pin_signal: PTA12/TPM1_CH0, direction: INPUT, gpio_interrupt: kPORT_InterruptFallingEdge, pull_enable: enable,
    passive_filter: disable}
  - {pin_num: '43', peripheral: GPIOC, signal: 'GPIO, 0', pin_signal: ADC0_SE14/PTC0/EXTRG_IN/USB_SOF_OUT/CMP0_OUT, direction: INPUT, gpio_interrupt: kPORT_InterruptEitherEdge,
    pull_select: down, pull_enable: enable}
  - {pin_num: '54', peripheral: TPM0, signal: 'CH, 5', pin_signal: CMP0_IN3/PTC9/I2C0_SDA/TPM0_CH5, direction: OUTPUT}
  - {pin_num: '55', peripheral: GPIOC, signal: 'GPIO, 10', pin_signal: PTC10/I2C1_SCL, direction: OUTPUT}
  - {pin_num: '26', peripheral: SystemControl, signal: NMI, pin_signal: PTA4/I2C1_SDA/TPM0_CH1/NMI_b, pull_enable: disable, passive_filter: disable}
  - {pin_num: '59', peripheral: GPIOD, signal: 'GPIO, 2', pin_signal: PTD2/SPI0_MOSI/UART2_RX/TPM0_CH2/SPI0_MISO/FXIO0_D2, identifier: LS_OE, direction: OUTPUT}
  - {pin_num: '53', peripheral: GPIOC, signal: 'GPIO, 8', pin_signal: CMP0_IN2/PTC8/I2C0_SCL/TPM0_CH4, direction: OUTPUT}
  - {pin_num: '50', peripheral: LPTMR0, signal: 'ALT, 2', pin_signal: PTC5/LLWU_P9/SPI0_SCK/LPTMR0_ALT2/CMP0_OUT}
  - {pin_num: '51', peripheral: GPIOC, signal: 'GPIO, 6', pin_signal: CMP0_IN0/PTC6/LLWU_P10/SPI0_MOSI/EXTRG_IN/SPI0_MISO, direction: INPUT}
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR TOOLS ***********
 */
/* clang-format on */

/* FUNCTION ************************************************************************************************************
 *
 * Function Name : BOARD_InitPins
 * Description   : Configures pin routing and optionally pin electrical features.
 *
 * END ****************************************************************************************************************/
void BOARD_InitPins(void)
{
    /* Port A Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortA);
    /* Port B Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortB);
    /* Port C Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortC);
    /* Port D Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortD);
    /* Port E Clock Gate Control: Clock enabled */
    CLOCK_EnableClock(kCLOCK_PortE);

    gpio_pin_config_t LOAD_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTA5 (pin 27)  */
    GPIO_PinInit(BOARD_INITPINS_LOAD_GPIO, BOARD_INITPINS_LOAD_PIN, &LOAD_config);

    gpio_pin_config_t CURSOR_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTA12 (pin 28)  */
    GPIO_PinInit(BOARD_INITPINS_CURSOR_GPIO, BOARD_INITPINS_CURSOR_PIN, &CURSOR_config);

    gpio_pin_config_t SD_PROTECT_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTB2 (pin 37)  */
    GPIO_PinInit(BOARD_INITPINS_SD_PROTECT_GPIO, BOARD_INITPINS_SD_PROTECT_PIN, &SD_PROTECT_config);

    gpio_pin_config_t SD_DETECT_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTB3 (pin 38)  */
    GPIO_PinInit(BOARD_INITPINS_SD_DETECT_GPIO, BOARD_INITPINS_SD_DETECT_PIN, &SD_DETECT_config);

    gpio_pin_config_t VBAT_DETECT_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTB18 (pin 41)  */
    GPIO_PinInit(BOARD_INITPINS_VBAT_DETECT_GPIO, BOARD_INITPINS_VBAT_DETECT_PIN, &VBAT_DETECT_config);

    gpio_pin_config_t VBUS_DETECT_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTC0 (pin 43)  */
    GPIO_PinInit(BOARD_INITPINS_VBUS_DETECT_GPIO, BOARD_INITPINS_VBUS_DETECT_PIN, &VBUS_DETECT_config);

    gpio_pin_config_t ENC_A_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTC6 (pin 51)  */
    GPIO_PinInit(BOARD_INITPINS_ENC_A_GPIO, BOARD_INITPINS_ENC_A_PIN, &ENC_A_config);

    gpio_pin_config_t TRIGGER_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTC7 (pin 52)  */
    GPIO_PinInit(BOARD_INITPINS_TRIGGER_GPIO, BOARD_INITPINS_TRIGGER_PIN, &TRIGGER_config);

    gpio_pin_config_t BUZZER_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTC8 (pin 53)  */
    GPIO_PinInit(BOARD_INITPINS_BUZZER_GPIO, BOARD_INITPINS_BUZZER_PIN, &BUZZER_config);

    gpio_pin_config_t RLY_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTC10 (pin 55)  */
    GPIO_PinInit(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, &RLY_config);

    gpio_pin_config_t LS_OE_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTD2 (pin 59)  */
    GPIO_PinInit(BOARD_INITPINS_LS_OE_GPIO, BOARD_INITPINS_LS_OE_PIN, &LS_OE_config);

    gpio_pin_config_t KP_8_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 1U
    };
    /* Initialize GPIO functionality on pin PTE21 (pin 10)  */
    GPIO_PinInit(BOARD_INITPINS_KP_8_GPIO, BOARD_INITPINS_KP_8_PIN, &KP_8_config);

    gpio_pin_config_t KP_7_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 1U
    };
    /* Initialize GPIO functionality on pin PTE22 (pin 11)  */
    GPIO_PinInit(BOARD_INITPINS_KP_7_GPIO, BOARD_INITPINS_KP_7_PIN, &KP_7_config);

    gpio_pin_config_t KP_6_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 1U
    };
    /* Initialize GPIO functionality on pin PTE23 (pin 12)  */
    GPIO_PinInit(BOARD_INITPINS_KP_6_GPIO, BOARD_INITPINS_KP_6_PIN, &KP_6_config);

    gpio_pin_config_t KP_5_config = {
        .pinDirection = kGPIO_DigitalOutput,
        .outputLogic = 1U
    };
    /* Initialize GPIO functionality on pin PTE24 (pin 20)  */
    GPIO_PinInit(BOARD_INITPINS_KP_5_GPIO, BOARD_INITPINS_KP_5_PIN, &KP_5_config);

    gpio_pin_config_t KP_4_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTE25 (pin 21)  */
    GPIO_PinInit(BOARD_INITPINS_KP_4_GPIO, BOARD_INITPINS_KP_4_PIN, &KP_4_config);

    gpio_pin_config_t KP_3_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTE29 (pin 17)  */
    GPIO_PinInit(BOARD_INITPINS_KP_3_GPIO, BOARD_INITPINS_KP_3_PIN, &KP_3_config);

    gpio_pin_config_t KP_2_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTE30 (pin 18)  */
    GPIO_PinInit(BOARD_INITPINS_KP_2_GPIO, BOARD_INITPINS_KP_2_PIN, &KP_2_config);

    gpio_pin_config_t KP_1_config = {
        .pinDirection = kGPIO_DigitalInput,
        .outputLogic = 0U
    };
    /* Initialize GPIO functionality on pin PTE31 (pin 19)  */
    GPIO_PinInit(BOARD_INITPINS_KP_1_GPIO, BOARD_INITPINS_KP_1_PIN, &KP_1_config);

    /* PORTA12 (pin 28) is configured as PTA12 */
    PORT_SetPinMux(BOARD_INITPINS_CURSOR_PORT, BOARD_INITPINS_CURSOR_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTA12 (pin 28): Interrupt on falling edge */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_CURSOR_PORT, BOARD_INITPINS_CURSOR_PIN, kPORT_InterruptFallingEdge);

    PORTA->PCR[12] = ((PORTA->PCR[12] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Enable: Internal pullup or pulldown resistor is enabled on the corresponding pin. */
                      | (uint32_t)(PORT_PCR_PE_MASK));

    /* PORTA18 (pin 32) is configured as EXTAL0 */
    PORT_SetPinMux(PORTA, 18U, kPORT_PinDisabledOrAnalog);

    /* PORTA19 (pin 33) is configured as XTAL0 */
    PORT_SetPinMux(PORTA, 19U, kPORT_PinDisabledOrAnalog);

    /* PORTA20 (pin 34) is configured as RESET_b */
    PORT_SetPinMux(PORTA, 20U, kPORT_MuxAlt7);

    /* PORTA4 (pin 26) is configured as NMI_b */
    PORT_SetPinMux(BOARD_INITPINS_BOOT_PORT, BOARD_INITPINS_BOOT_PIN, kPORT_MuxAlt7);

    PORTA->PCR[4] = ((PORTA->PCR[4] &
                      /* Mask bits to zero which are setting */
                      (~(PORT_PCR_PE_MASK | PORT_PCR_PFE_MASK | PORT_PCR_ISF_MASK)))

                     /* Pull Enable: Internal pullup or pulldown resistor is not enabled on the corresponding pin. */
                     | PORT_PCR_PE(kPORT_PullDisable)

                     /* Passive Filter Enable: Passive input filter is disabled on the corresponding pin. */
                     | PORT_PCR_PFE(kPORT_PassiveFilterDisable));

    /* PORTA5 (pin 27) is configured as PTA5 */
    PORT_SetPinMux(BOARD_INITPINS_LOAD_PORT, BOARD_INITPINS_LOAD_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTA5 (pin 27): Interrupt on falling edge */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_LOAD_PORT, BOARD_INITPINS_LOAD_PIN, kPORT_InterruptFallingEdge);

    PORTA->PCR[5] = ((PORTA->PCR[5] &
                      /* Mask bits to zero which are setting */
                      (~(PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                     /* Pull Enable: Internal pullup or pulldown resistor is enabled on the corresponding pin. */
                     | (uint32_t)(PORT_PCR_PE_MASK));

    /* PORTB0 (pin 35) is configured as I2C0_SCL */
    PORT_SetPinMux(PORTB, 0U, kPORT_MuxAlt2);

    /* PORTB1 (pin 36) is configured as I2C0_SDA */
    PORT_SetPinMux(PORTB, 1U, kPORT_MuxAlt2);

    /* PORTB16 (pin 39) is configured as SPI1_MOSI */
    PORT_SetPinMux(PORTB, 16U, kPORT_MuxAlt2);

    /* PORTB17 (pin 40) is configured as SPI1_MISO */
    PORT_SetPinMux(PORTB, 17U, kPORT_MuxAlt2);

    /* PORTB18 (pin 41) is configured as PTB18 */
    PORT_SetPinMux(BOARD_INITPINS_VBAT_DETECT_PORT, BOARD_INITPINS_VBAT_DETECT_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTB18 (pin 41): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_VBAT_DETECT_PORT, BOARD_INITPINS_VBAT_DETECT_PIN, kPORT_InterruptOrDMADisabled);

    PORTB->PCR[18] = ((PORTB->PCR[18] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                       * corresponding PE field is set. */
                      | (uint32_t)(kPORT_PullDown));

    /* PORTB2 (pin 37) is configured as PTB2 */
    PORT_SetPinMux(BOARD_INITPINS_SD_PROTECT_PORT, BOARD_INITPINS_SD_PROTECT_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTB2 (pin 37): Interrupt on either edge */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_SD_PROTECT_PORT, BOARD_INITPINS_SD_PROTECT_PIN, kPORT_InterruptEitherEdge);

    /* PORTB3 (pin 38) is configured as PTB3 */
    PORT_SetPinMux(BOARD_INITPINS_SD_DETECT_PORT, BOARD_INITPINS_SD_DETECT_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTB3 (pin 38): Interrupt on either edge */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_SD_DETECT_PORT, BOARD_INITPINS_SD_DETECT_PIN, kPORT_InterruptEitherEdge);

    /* PORTC0 (pin 43) is configured as PTC0 */
    PORT_SetPinMux(BOARD_INITPINS_VBUS_DETECT_PORT, BOARD_INITPINS_VBUS_DETECT_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTC0 (pin 43): Interrupt on either edge */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_VBUS_DETECT_PORT, BOARD_INITPINS_VBUS_DETECT_PIN, kPORT_InterruptEitherEdge);

    PORTC->PCR[0] = ((PORTC->PCR[0] &
                      /* Mask bits to zero which are setting */
                      (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                     /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                      * corresponding PE field is set. */
                     | (uint32_t)(kPORT_PullDown));

    /* PORTC1 (pin 44) is configured as ADC0_SE15 */
    PORT_SetPinMux(BOARD_INITPINS_TEMP1_PORT, BOARD_INITPINS_TEMP1_PIN, kPORT_PinDisabledOrAnalog);

    /* PORTC10 (pin 55) is configured as PTC10 */
    PORT_SetPinMux(BOARD_INITPINS_RLY_PORT, BOARD_INITPINS_RLY_PIN, kPORT_MuxAsGpio);

    /* PORTC2 (pin 45) is configured as ADC0_SE11 */
    PORT_SetPinMux(BOARD_INITPINS_TEMP2_PORT, BOARD_INITPINS_TEMP2_PIN, kPORT_PinDisabledOrAnalog);

    /* PORTC3 (pin 46) is configured as SPI1_SCK */
    PORT_SetPinMux(PORTC, 3U, kPORT_MuxAlt2);

    /* PORTC4 (pin 49) is configured as SPI1_PCS0 */
    PORT_SetPinMux(PORTC, 4U, kPORT_MuxAlt5);

    /* PORTC5 (pin 50) is configured as LPTMR0_ALT2 */
    PORT_SetPinMux(BOARD_INITPINS_ENC_B_PORT, BOARD_INITPINS_ENC_B_PIN, kPORT_MuxAlt3);

    /* PORTC6 (pin 51) is configured as PTC6 */
    PORT_SetPinMux(BOARD_INITPINS_ENC_A_PORT, BOARD_INITPINS_ENC_A_PIN, kPORT_MuxAsGpio);

    /* PORTC7 (pin 52) is configured as PTC7 */
    PORT_SetPinMux(BOARD_INITPINS_TRIGGER_PORT, BOARD_INITPINS_TRIGGER_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTC7 (pin 52): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_TRIGGER_PORT, BOARD_INITPINS_TRIGGER_PIN, kPORT_InterruptOrDMADisabled);

    PORTC->PCR[7] = ((PORTC->PCR[7] &
                      /* Mask bits to zero which are setting */
                      (~(PORT_PCR_SRE_MASK | PORT_PCR_ISF_MASK)))

                     /* Slew Rate Enable: Slow slew rate is configured on the corresponding pin, if the pin is
                      * configured as a digital output. */
                     | PORT_PCR_SRE(kPORT_SlowSlewRate));

    /* PORTC8 (pin 53) is configured as PTC8 */
    PORT_SetPinMux(BOARD_INITPINS_BUZZER_PORT, BOARD_INITPINS_BUZZER_PIN, kPORT_MuxAsGpio);

    /* PORTC9 (pin 54) is configured as TPM0_CH5 */
    PORT_SetPinMux(BOARD_INITPINS_FAN_PORT, BOARD_INITPINS_FAN_PIN, kPORT_MuxAlt3);

    /* PORTD2 (pin 59) is configured as PTD2 */
    PORT_SetPinMux(BOARD_INITPINS_LS_OE_PORT, BOARD_INITPINS_LS_OE_PIN, kPORT_MuxAsGpio);

    /* PORTE21 (pin 10) is configured as PTE21 */
    PORT_SetPinMux(BOARD_INITPINS_KP_8_PORT, BOARD_INITPINS_KP_8_PIN, kPORT_MuxAsGpio);

    /* PORTE22 (pin 11) is configured as PTE22 */
    PORT_SetPinMux(BOARD_INITPINS_KP_7_PORT, BOARD_INITPINS_KP_7_PIN, kPORT_MuxAsGpio);

    /* PORTE23 (pin 12) is configured as PTE23 */
    PORT_SetPinMux(BOARD_INITPINS_KP_6_PORT, BOARD_INITPINS_KP_6_PIN, kPORT_MuxAsGpio);

    /* PORTE24 (pin 20) is configured as PTE24 */
    PORT_SetPinMux(BOARD_INITPINS_KP_5_PORT, BOARD_INITPINS_KP_5_PIN, kPORT_MuxAsGpio);

    /* PORTE25 (pin 21) is configured as PTE25 */
    PORT_SetPinMux(BOARD_INITPINS_KP_4_PORT, BOARD_INITPINS_KP_4_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTE25 (pin 21): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_KP_4_PORT, BOARD_INITPINS_KP_4_PIN, kPORT_InterruptOrDMADisabled);

    PORTE->PCR[25] = ((PORTE->PCR[25] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                       * corresponding PE field is set. */
                      | (uint32_t)(kPORT_PullDown));

    /* PORTE29 (pin 17) is configured as PTE29 */
    PORT_SetPinMux(BOARD_INITPINS_KP_3_PORT, BOARD_INITPINS_KP_3_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTE29 (pin 17): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_KP_3_PORT, BOARD_INITPINS_KP_3_PIN, kPORT_InterruptOrDMADisabled);

    PORTE->PCR[29] = ((PORTE->PCR[29] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                       * corresponding PE field is set. */
                      | (uint32_t)(kPORT_PullDown));

    /* PORTE30 (pin 18) is configured as PTE30 */
    PORT_SetPinMux(BOARD_INITPINS_KP_2_PORT, BOARD_INITPINS_KP_2_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTE30 (pin 18): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_KP_2_PORT, BOARD_INITPINS_KP_2_PIN, kPORT_InterruptOrDMADisabled);

    PORTE->PCR[30] = ((PORTE->PCR[30] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                       * corresponding PE field is set. */
                      | (uint32_t)(kPORT_PullDown));

    /* PORTE31 (pin 19) is configured as PTE31 */
    PORT_SetPinMux(BOARD_INITPINS_KP_1_PORT, BOARD_INITPINS_KP_1_PIN, kPORT_MuxAsGpio);

    /* Interrupt configuration on PORTE31 (pin 19): Interrupt/DMA request is disabled */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_KP_1_PORT, BOARD_INITPINS_KP_1_PIN, kPORT_InterruptOrDMADisabled);

    PORTE->PCR[31] = ((PORTE->PCR[31] &
                       /* Mask bits to zero which are setting */
                       (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ISF_MASK)))

                      /* Pull Select: Internal pulldown resistor is enabled on the corresponding pin, if the
                       * corresponding PE field is set. */
                      | (uint32_t)(kPORT_PullDown));
}
/***********************************************************************************************************************
 * EOF
 **********************************************************************************************************************/
