/*
 *
 * File: opmodes.c
 * Project: DC Load NewBrain
 * Created Date: 21/5/2019, 15:32
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include "peripherals.h"
#include "pin_mux.h"
#include "main.h"
#include "mcp3426/mcp3426.h"
#include "keypad/keypad.h"
#include "lcd/lcdmenu.h"
#include "stdlib.h"
#include "opmodes.h"
#include <sysview_enable_conf.h>

// Contiene la lógica de aplicación de los diferentes modos de operación.

// Voltajes de corte de las diferentes tipos de baterías.
static float LiPoCutOffVoltage = 3.0;
static float LiFeCutOffVoltage = 2.8;
static float NiCdCutOffVoltage = 1.0;
static float ZiZnCutOffVoltage = 1.0;

// Almacena el valor de la corriente mínima y máxima usada en los modos Tx.
static float setHighCurrent = 0;
static float setLowCurrent = 0;

// Tiempo en ms entre transiciones en el modo transitorio.
static uint32_t setTransientPeriod = 0;

// Almacena el valor del DAC para la corriente mínima y máxima usada en los modos Tx.
volatile uint16_t lowCurrent_dac = 0;
volatile uint16_t highCurrent_dac = 0;

uint32_t eBatteryCapacity = 0; // Comparte la descarga de la batería para el logging.

/**
 * Mide la resistencia interna de una batería.
 *
 */
static void opmodes_batteryIntResist(){
    float voltage20ma = 0;        // Almacena el voltaje con 20 mA de carga.
    float voltage520ma = 0;       // Almacena el voltaje con 520 mA de carga.
    float internalResistance = 0;

    static char buffer[6];
    for (uint8_t i=0; i < 6; i++){
        buffer[i] = '\0';
    }

    lcdmenu_batteryIntResistMenu();

    actualVoltage = readVoltage(false, true);
    if (checkPolarity()){
        GPIO_PinWrite(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, 1); // Cierro relés.
        delay_us(50000);

        UBaseType_t priority = uxTaskPriorityGet(NULL);
        // Establece la máxima prioridad para que no la interrumpan otras tareas.
        vTaskPrioritySet(NULL, configMAX_PRIORITIES -1);

        mcp4725_set_voltage(&dac, 6); // 20 mA load.
        delay_us(20000);
        voltage20ma = readVoltage(false, false);

        mcp4725_set_voltage(&dac, 141); // 519 mA load.
        delay_us(50000);
        delay_us(50000);
        voltage520ma =readVoltage(false, false);

        mcp4725_set_voltage(&dac, 0);
        delay_us(5);
        GPIO_PinWrite(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, 0); // Abro los relés.
        internalResistance = ((voltage20ma - voltage520ma) / 0.499) * 1000;
        // Reestablece la prioridad.
        vTaskPrioritySet(NULL, priority);

        lcdmenu_moveCursor(6, 2);
        sprintf(buffer, "%5.0f", internalResistance);
        lcdmenu_writeString(buffer);
    }

    // Espera a que se pulse una tecla.
    lcdmenu_requestSelection(true);
    lcdmenu_clear();
}

/**
 * Convierte un tiempo en segundos a cadena de texto.
 *
 * @param timeSeconds Tiempo en segundos.
 * @return Cadena de texto en formato hh:mm:ss.
 */
static char * lcdmenu_SecondsToStrTime(uint32_t timeSeconds){

    static char buffer[9] = {0};
    uint8_t seconds;
    uint8_t minutes;
    uint8_t hours;
    uint32_t tTimeSeconds = timeSeconds;

    hours = tTimeSeconds / 3600;
    tTimeSeconds -= 3600 *  hours;
    minutes = tTimeSeconds / 60;
    tTimeSeconds -= 60 * minutes;
    seconds = tTimeSeconds;

    sprintf(buffer, "%02d:%02d:%02d", hours, minutes, seconds);
    return buffer;
}

/**
 * Lógica y menús en pantalla del modo de descarga de batería.
 *
 * @param[in] actualVoltage Voltaje actual.
 * @param[in] actualCurrent Corriente actual.
 * @param[in] actualPower Potencia actual.
 * @param[in] selectBattery True para mostrar los menús de selección y poner las variables a 0.
 */
void opmodes_batteryCapacity(const float actualVoltage,
                             const float actualCurrent,
                             const float actualPower,
                             bool selectBattery){

    static uint32_t BatteryLife = 0;
    static uint32_t BatteryLifePrevious = 0;
    static float BatteryCutoffVolts = 0;
    static const char BatteryTypes[6][5] = {"LiPo", "LiFe", "NiCd", "ZiZn", "SetV",  "    "};
    typedef enum {LiPo=0, LiFe, NiCd, ZiZn, SetV, None} BatteryType_t;
    static BatteryType_t batteryType;
    static float BatteryCurrent = 0;
    static char buffer[6];
    static char buffer2[6];
    static uint32_t BatteryDischarge = 0;
    for (uint8_t i=0; i < 6; i++){
        buffer[i] = '\0';
        buffer2[i] = '\0';
    }

    float tmp = 0;
    float loadCurrent = 0; // Almacena la corriente actual de descarga.

    if (selectBattery){
        showTemp = false;
        // Se asegura que la interrupción por segundo esté desactivada.
        DisableIRQ(RTC_1_SECONDS_IRQN);
        BatteryLife = 0;
        BatteryLifePrevious = 0;
        Seconds = 0;
        BatteryCutoffVolts = 0;
        BatteryCurrent = 0;
        batteryType = None;
        eBatteryCapacity = 0;

        // Se selecciona el tipo de batería.
        lcdmenu_batteryTypeMenu();
        char key = lcdmenu_requestSelection(true);

        switch (key) {
            case '1':
                BatteryCutoffVolts = LiPoCutOffVoltage;
                batteryType = LiPo;
                break;
            case '2':
                BatteryCutoffVolts = LiFeCutOffVoltage;
                batteryType = LiFe;
                break;
            case '3':
                BatteryCutoffVolts = NiCdCutOffVoltage;
                batteryType = NiCd;
                break;
            case '4':
                BatteryCutoffVolts = ZiZnCutOffVoltage;
                batteryType = ZiZn;
                break;
            case '5':
                BatteryCutoffVolts = lcdmenu_batteryCutOffMenu();
                batteryType = SetV;
                break;
            default:
                BatteryCutoffVolts = 0.0;
                batteryType = None;
                break;
        }

        // Se selecciona el número de celdas de la batería.
        BatteryCutoffVolts = lcdmenu_cellsCountMenu(BatteryCutoffVolts);

        if (BatteryCutoffVolts != 0.0){

            sprintf(buffer, "%5.2f", BatteryCutoffVolts);
            lcdmenu_batterySelectedInfoMenu(BatteryTypes[batteryType], buffer);
            vTaskDelay(5000/portTICK_PERIOD_MS);
            //vTaskResume(xTaskGetHandle("inputValue_task"));
            BatteryDischarge = lcdmenu_batteryDischargeCutOffMenu();
            //vTaskSuspend(xTaskGetHandle("inputValue_task"));
            opmodes_batteryIntResist();
        }
        showTemp = true;
        blockNumKeys = true;
    }

    if (BatteryCutoffVolts != 0.0){
        lcdmenu_cursor(false);
        lcdmenu_moveCursor(16,2);
        // Muestra el tipo de batería en pantalla.
        lcdmenu_writeString(BatteryTypes[batteryType]);
        lcdmenu_moveCursor(0, 3);
        // Muestra el tiempo en pantalla.
        lcdmenu_writeString(lcdmenu_SecondsToStrTime(Seconds));

        loadCurrent = actualCurrent;

        // Si ha parado de contar, se usa la última corriente leída.
        if (NVIC_GetEnableIRQ(RTC_1_SECONDS_IRQN) == 0){
            loadCurrent = BatteryCurrent;
        }

        // Calcula la capacidad en mAh.
        tmp = loadCurrent * 1000.0;
        BatteryLife = (uint32_t)(tmp * (Seconds / 3600.0));
        lcdmenu_moveCursor(9, 3);

        // Solo actualiza la pantalla si la capacidad ha cambiado.
        if(BatteryLife >= BatteryLifePrevious){

            if (BatteryLife < 10) {
                lcdmenu_writeString("0000");
            }

            if ((BatteryLife >= 10) && (BatteryLife <100)){
                lcdmenu_writeString("000");
            }

            if ((BatteryLife >= 100) && (BatteryLife <1000)){
                lcdmenu_writeString("00");
            }

            if ((BatteryLife >= 1000) && (BatteryLife <10000)){
                lcdmenu_writeString("0");
            }

            uitoa(BatteryLife, buffer2, 10);
            lcdmenu_writeString(buffer2);
            lcdmenu_moveCursor(14,3);
            lcdmenu_writeString("mAh");
            BatteryLifePrevious = BatteryLife;
            eBatteryCapacity = BatteryLife;
        }

        // Corta la salida cuando llega al voltage de corte.
        if (actualVoltage <= BatteryCutoffVolts){
            BatteryCurrent = actualCurrent;
            loadOutput = false;
        }

        // Corta la salida cuando llega al nivel de descarga indicado.
        if ((BatteryDischarge > 0) && (BatteryLife >= BatteryDischarge)){
            BatteryCurrent = actualCurrent;
            loadOutput = false;
        }
    }
}

/**
 * Devuelve el valor para el DAC dada la corriente deseada.
 *
 * Se utiliza para el modo de transitorios.
 *
 * @note Utiliza el mismo código de la función dacControl, pero sin
 * el ajuste de error posterior. Por lo que existe un error de unos
 * 150 mA como máximo a media y final de escala. (Entre 7 y 15A).
 * @param setCurrent Corriente deseada.
 * @return Número a mandar al DAC para obtener la corriente deseada.
 */
static uint16_t opmodes_GetDacValue(float setCurrent){
    uint16_t controlVoltage;
    float tSetCurrent = setCurrent;

    tSetCurrent *= 1000; // Se pasa a miliamperios.

    controlVoltage = (uint16_t)(tSetCurrent / 3.87096);

    if (controlVoltage > 100){
        controlVoltage = (1.001593 * controlVoltage) - 4.164079;
    }

    controlVoltage = controlVoltage * 1.029931;
    return controlVoltage;
}

/**
 * Lógica del modo de transitorios.
 *
 * @param[in] mode Modo de funcionamiento de la carga electrónica.
 * @param[in] lowCurrent Valor mínimo de corriente.
 * @param[in] highCurrent Valor máximo de corriente
 * @param[in] transientPeriod Tiempo entre transiciones en milisegundos.
 * @param[in] timer Temporizador utilizado para el modo TC.
 */
void opmodes_transient(dcLoad_Mode_t mode, float lowCurrent, float highCurrent,
                       uint32_t transientPeriod, TimerHandle_t timer){

    static bool active = pdFALSE;   // Indica si el timer del modo TC está activo.

    setHighCurrent = highCurrent;
    setLowCurrent = lowCurrent;

    if (loadOutput)
    {
        if (transientFastMode && (lowCurrent_dac == 0) && (highCurrent_dac == 0)){
           lowCurrent_dac = opmodes_GetDacValue(lowCurrent);
           highCurrent_dac = opmodes_GetDacValue(highCurrent);
           lcdmenu_cleanLine(1);
        }

        if (mode == DCLOAD_MODE_TC){
            if (!active){
                // En el modo FastMode se activa si el periodo es menor de 500 ms.
                if (transientPeriod < 500){
                  transientFastMode = true;
                  lowCurrent_dac = opmodes_GetDacValue(lowCurrent);
                  highCurrent_dac = opmodes_GetDacValue(highCurrent);
                  lcdmenu_cleanLine(1);
                }

                if (slewEnable && (!transientFastMode)){
                    setDacWSlewRate(lowCurrent_dac, true);
                }
                else{
                    mcp4725_set_voltage(&dac, lowCurrent_dac);
                }
                xTimerChangePeriod(timer, transientPeriod/portTICK_PERIOD_MS, 0);
                active = pdTRUE;
            }
        }

        // Necesita un pulso de al menos 10ms.
        if ((mode == DCLOAD_MODE_TT) || (mode == DCLOAD_MODE_TP)){
            if (!active){
                if (slewEnable && (!transientFastMode)){
                    setDacWSlewRate(lowCurrent_dac, true);
                }
                else{
                    mcp4725_set_voltage(&dac, lowCurrent_dac);
                }

                if (mode == DCLOAD_MODE_TP){
                    setTransientPeriod = transientPeriod;
                }
                xTimerChangePeriod(timer, 10/portTICK_PERIOD_MS, 0);
                active = pdTRUE;
            }
        }
    }
    else
    {
        if (active){
            xTimerStop(timer, 0);
            active = pdFALSE;
        }
        else{
            ; // Misra c 15.7
        }
    }
}

/**
 * Función encargada de conmutar la corriente de salida de la carga.
 *
 * Para usar exclusivamente en la función siguiente.
 */
static void toogleCurrent(){
    static state_t lastState = LOW_CURRENT;  // Almacena el último estado (Corriente alta o baja).

    if (lastState == LOW_CURRENT){
        if (transientFastMode){
            mcp4725_set_voltage(&dac, highCurrent_dac);
        }
        else{
            encoder.reading = setHighCurrent;
        }
        lastState = HIGH_CURRENT;
    }
    else {
        if (transientFastMode){
            mcp4725_set_voltage(&dac, lowCurrent_dac);
        }
        else{
            encoder.reading = setLowCurrent;
        }
        lastState = LOW_CURRENT;
    }
}

/**
 * Alterna entra la corrientes fijadas usando un timer de FreeRTOS.
 *
 * @param timer Timer encargado de ejecutar esta función.
 */
void opmodes_transientToggle(TimerHandle_t timer){
    static uint32_t lastTime = 0;            // Almacena el tiempo cuando recibió un pulso.

    if (operationMode == DCLOAD_MODE_TC){
        toogleCurrent();
    }

    if (operationMode == DCLOAD_MODE_TT){
        if (GPIO_PinRead(BOARD_INITPINS_TRIGGER_GPIO,
                         BOARD_INITPINS_TRIGGER_PIN) == 1){
            toogleCurrent();
        }
    }

    if (operationMode == DCLOAD_MODE_TP){
        if (GPIO_PinRead(BOARD_INITPINS_TRIGGER_GPIO,
                         BOARD_INITPINS_TRIGGER_PIN) == 1){
           // Un pulso es recibido, por lo que se pasa a corriente máxima.
            if (transientFastMode){
                mcp4725_set_voltage(&dac, highCurrent_dac);
            }
            else{
                encoder.reading = setHighCurrent;
            }
           // Almacena el tiempo en el que se puso la corriente máxima.
           lastTime = xTaskGetTickCount();
        }
        else{
           // Si ha recibido un pulso y ha pasado el tiempo especificado, se cambia a bajo.
           if ((xTaskGetTickCount() - lastTime) > (setTransientPeriod/portTICK_PERIOD_MS)){
               if (transientFastMode){
                   mcp4725_set_voltage(&dac, lowCurrent_dac);
               }
               else{
                    encoder.reading = setLowCurrent;
               }
           }
           else{
               ; // Misra c 15.7
           }
       }
    }
}
