/*
 *
 * File: opmodes.h
 * Project: DC Load NewBrain
 * Created Date: 23/5/2019, 16:09
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef OPMODES_H_
#define OPMODES_H_

#include "lcd/lcdmenu.h"
#include <timers.h>

typedef enum {HIGH_CURRENT, LOW_CURRENT} state_t;
extern volatile uint16_t lowCurrent_dac;
extern volatile uint16_t highCurrent_dac;
extern uint32_t eBatteryCapacity;

void opmodes_batteryCapacity(const float actualVoltage,
                             const float actualCurrent,
                             const float actualPower,
                             bool selectBattery);

void opmodes_transient(dcLoad_Mode_t mode, float lowCurrent, float highCurrent,
                       uint32_t transientPeriod, TimerHandle_t timer);

void opmodes_transientToggle(TimerHandle_t timer);

#endif /* OPMODES_H_ */
