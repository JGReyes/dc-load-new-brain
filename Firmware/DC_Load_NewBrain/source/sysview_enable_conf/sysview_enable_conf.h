/*
 *
 * File: sysview_enable_conf.h
 * Project: Template_FreeRTOS_,_SystemView_and_MTB
 * Created Date: 2/4/2019, 09:40:00
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef SYSVIEW_ENABLE_CONF_H_
#define SYSVIEW_ENABLE_CONF_H_

#include "SEGGER_SYSVIEW.h"
/* Include internal header to get SEGGER_RTT_CB */
#include "SEGGER_RTT.h"
#include "SEGGER_SYSVIEW_Conf.h"
#include "FreeRTOSConfig.h"
#include "string.h"


// Habilita SystemView y las macros para el envio de mensajes.
// Se recomienda compilar en Release para un mejor rendimiento de SystemView.

//#define SYSVIEW_ENABLE  1


#define SYSVIEW_DEVICE_NAME "MK27Z64VLH4 Cortex-M0+"
#define SYSVIEW_RAM_BASE (0x1FFFF000)

/* The application name to be displayed in SystemViewer */
#define SYSVIEW_APP_NAME "DC Load NewBrain"

/* Frequency of the timestamp. Must match SEGGER_SYSVIEW_GET_TIMESTAMP in SEGGER_SYSVIEW_Conf.h */
#define SYSVIEW_TIMESTAMP_FREQ (configCPU_CLOCK_HZ)

/* System Frequency. SystemcoreClock is used in most CMSIS compatible projects. */
#define SYSVIEW_CPU_FREQ configCPU_CLOCK_HZ



#define SCB_ICSR  (*(volatile U32*) (0xE000ED04uL)) // Interrupt Control State Register
#define SCB_ICSR_PENDSTSET_MASK     (1UL << 26)     // SysTick pending bit
#define SYST_RVR  (*(volatile U32*) (0xE000E014uL)) // SysTick Reload Value Register
#define SYST_CVR  (*(volatile U32*) (0xE000E018uL)) // SysTick Current Value Register

#ifdef SYSVIEW_ENABLE

#define svPrint(x)                  SEGGER_SYSVIEW_Print(x)
#define svPrintfTarget(x, args...)  SEGGER_SYSVIEW_PrintfTarget(x, args)
#define svWarn(x)                   SEGGER_SYSVIEW_Warn(x)
#define svWarnfTarget(x, args...)   SEGGER_SYSVIEW_WarnfTarget(x, args)
#define svError(x)                  SEGGER_SYSVIEW_Error(x)
#define svErrorfTarget(x, args...)  SEGGER_SYSVIEW_ErrorfTarget(x, args)
#define svEnterISR()                SEGGER_SYSVIEW_RecordEnterISR()
#define svExitISR()                 SEGGER_SYSVIEW_RecordExitISR()
#define svConf()                    SEGGER_SYSVIEW_Conf()
#define svStart()                   SEGGER_SYSVIEW_Start()
#define svStop()                    SEGGER_SYSVIEW_Stop()
#define svEnableStdEvents()         SEGGER_SYSVIEW_EnableEvents()
#define svDisableStdEvents()        SEGGER_SYSVIEW_DisableEvents()
#define svWaitConn()                SEGGER_SYSVIEW_WaitForConnection()

#else

#define svPrint(x)                      ((void)0)
#define svPrintfTarget(x, args...)      ((void)0)
#define svWarn(x)                       ((void)0)
#define svWarnfTarget(x, args...)       ((void)0)
#define svError(x)                      ((void)0)
#define svErrorfTarget(x, args...)      ((void)0)
#define svEnterISR()                    ((void)0)
#define svExitISR()                     ((void)0)
#define svConf()                        ((void)0)
#define svStart()                       ((void)0)
#define svStop()                        ((void)0)
#define svEnableStdEvents()             ((void)0)
#define svDisableStdEvents()            ((void)0)
#define svWaitConn()                    ((void)0)


#endif

extern SEGGER_RTT_CB _SEGGER_RTT;
extern const SEGGER_SYSVIEW_OS_API SYSVIEW_X_OS_TraceAPI;
extern unsigned int SEGGER_SYSVIEW_TickCnt;

U32 SEGGER_SYSVIEW_X_GetTimestamp(void);

#endif /* SYSVIEW_ENABLE_CONF_H_ */
