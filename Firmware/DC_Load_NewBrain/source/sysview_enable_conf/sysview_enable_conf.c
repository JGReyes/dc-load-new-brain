/*
 *
 * File: sysview_enable_conf.c
 * Project: Template_FreeRTOS_,_SystemView_and_MTB
 * Created Date: 2/4/2019, 09:53:00
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include "sysview_enable_conf.h"
#include "SEGGER_SYSVIEW.h"


/*!
 * @brief System View callback
 */
static void _cbSendSystemDesc(void)
{
    SEGGER_SYSVIEW_SendSysDesc("N=" SYSVIEW_APP_NAME ",D=" SYSVIEW_DEVICE_NAME
                               ",O=FreeRTOS");
    SEGGER_SYSVIEW_SendSysDesc("I#46=PortBCDE");
    SEGGER_SYSVIEW_SendSysDesc("I#44=Encoder");
    SEGGER_SYSVIEW_SendSysDesc("I#15=SysTick");
    SEGGER_SYSVIEW_SendSysDesc("I#38=BuzzerPWM");
    SEGGER_SYSVIEW_SendSysDesc("I#47=SD/USB_Detect");

}

/*!
 * @brief System View configuration
 */
void SEGGER_SYSVIEW_Conf(void)
{
    SEGGER_SYSVIEW_Init(SYSVIEW_TIMESTAMP_FREQ, SYSVIEW_CPU_FREQ,
                        &SYSVIEW_X_OS_TraceAPI, _cbSendSystemDesc);
    SEGGER_SYSVIEW_SetRAMBase(SYSVIEW_RAM_BASE);
    //SEGGER_SYSVIEW_DisableEvents(SYSVIEW_EVTMASK_ALL_TASKS);
    //SEGGER_SYSVIEW_DisableEvents(SYSVIEW_EVTMASK_ALL_INTERRUPTS);

    // Además de lo anterior, también se pueden desactivar selectivamente
    // todos los elementos de FreeRTOS modificando el archivo SEGGER_SYSVIEW_FreeRTOS.h
    // Está desactivado el trazado de las funciones de queue y de delay, ya que
    // son muchas las llamadas y la sonda de depuración no tiene velocidad suficiente.
}


/*********************************************************************
*
*       SEGGER_SYSVIEW_X_GetTimestamp()
*
* Function description
*   Returns the current timestamp in ticks using the system tick
*   count and the SysTick counter.
*   All parameters of the SysTick have to be known and are set via
*   configuration defines on top of the file.
*
* Return value
*   The current timestamp.
*
* Additional information
*   SEGGER_SYSVIEW_X_GetTimestamp is always called when interrupts are
*   disabled. Therefore locking here is not required.
*/

U32 SEGGER_SYSVIEW_X_GetTimestamp(void) {
#ifdef SYSVIEW_ENABLE
    U32 TickCount;
    U32 Cycles;
    U32 CyclesPerTick;
    //
    // Get the cycles of the current system tick.
    // SysTick is down-counting, subtract the current value from the number of cycles per tick.
    //
    CyclesPerTick = SYST_RVR + 1;
    Cycles = (CyclesPerTick - SYST_CVR);
    //
    // Get the system tick count.
    //
    TickCount = SEGGER_SYSVIEW_TickCnt;
    //
    // If a SysTick interrupt is pending, re-read timer and adjust result
    //
    if ((SCB_ICSR & SCB_ICSR_PENDSTSET_MASK) != 0) {
        Cycles = (CyclesPerTick - SYST_CVR);
        TickCount++;
    }
    Cycles += TickCount * CyclesPerTick;

    return Cycles;
#else
    return 0;
#endif
}

