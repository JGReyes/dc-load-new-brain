/*
 *
 * File: mcp3426.c
 * Project: DC Load NewBrain
 * Created Date: 8/5/2019, 11:25
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include <mcp3426/mcp3426.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"

#define MCP3426_CHANNEL_MASK        0x60
#define MCP3426_PGA_MASK            0x03
#define MCP3426_SRATE_MASK          0x0C
#define MCP3426_RDY_MASK            0x80
#define MCP3426_RDY_POS             0x07
#define MCP3426_CONV_MODE_MASK      0x10

#define MCP3426_CHANNEL(config) (((config) & MCP3426_CHANNEL_MASK) >> 5)
#define MCP3426_PGA(config) ((config) & MCP3426_PGA_MASK)
#define MCP3426_SAMPLE_RATE(config) (((config) & MCP3426_SRATE_MASK) >> 2)
#define MCP3426_CONV_MODE(config) (((config) & MCP3426_CONV_MODE_MASK) >> 4)

#define MCP3426_CHANNEL_VALUE(value) (((value) << 5) & MCP3426_CHANNEL_MASK)
#define MCP3426_PGA_VALUE(value) ((value) & MCP3426_PGA_MASK)
#define MCP3426_SAMPLE_RATE_VALUE(value) (((value) << 2) & MCP3426_SRATE_MASK)
#define MCP3426_CONV_MODE_VALUE(value) (((value) << 4) & MCP3426_CONV_MODE_MASK)


/*
 * Constantes con los tiempos de espera en milisegundos.
 * Para asegurarse de que transcurre el tiempo de adquisición del ADC.
 */
static const uint8_t mcp3426_read_times[3] = {
    1000 / 240,   // 12 bits 240/s
    1000 / 60,     // 14 bits  60/s
    1000 / 15      // 16 bits  15/s
};

// Estructura para el uso del bus i2c con el SDK de NXP en FreeRTOS.
static i2c_master_transfer_t i2c_Data = {
            .flags = kI2C_TransferDefaultFlag,
            .slaveAddress = 0,
            .direction = kI2C_Write,
            .subaddress = 0,
            .subaddressSize = 0,
            .data = NULL,
            .dataSize = 0
};

/**
 * Convierte una estructura de configuración en un entero sin signo.
 *
 * Usado para escribir en el registro de configuración del ADC.
 * @param[in] config Estructura de configuración.
 * @return Número que representa la configuración.
 */
static inline uint8_t mcp3426_config_to_uint(const mcp3426_config_t *const config){
    uint8_t tempConfig = 0;
    tempConfig |= MCP3426_CHANNEL_VALUE(config->channel);
    tempConfig |= MCP3426_CONV_MODE_VALUE(config->mode);
    tempConfig |= MCP3426_PGA_VALUE(config->gain[config->channel]);
    tempConfig |= MCP3426_SAMPLE_RATE_VALUE(config->srate);
    return tempConfig;
}

/**
 * Convierte un número entero sin signo a una estructura de configuración.
 *
 * Usado para leer el registro de configuración del ADC.
 * @param[in] config Número que representa la configuración.
 * @return Estructura con la configuración.
 */
static inline mcp3426_config_t mcp3426_uint_to_config(const uint8_t config){
    mcp3426_config_t tempConfig;
    tempConfig.channel = MCP3426_CHANNEL(config);
    tempConfig.mode = MCP3426_CONV_MODE(config);
    tempConfig.gain[tempConfig.channel] = MCP3426_PGA(config);
    tempConfig.srate = MCP3426_SAMPLE_RATE(config);
    return tempConfig;
}

/**
 * Actualiza la configuración del ADC.
 *
 * @param[in,out] adc Dispositivo ADC a utilizar.
 * @param[in] newconfig Nueva configuración para actualizar.
 * @param[in] sample True para realizar un muestreo del ADC, False para no hacer nada.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp3426_update_config(mcp3426_t *const adc, const mcp3426_config_t newconfig,
                              const bool sample)
{
    int32_t result;
    uint8_t txConfig=0;

    txConfig = mcp3426_config_to_uint(&newconfig);
    if (sample){
        txConfig |= (1U << MCP3426_RDY_POS); // Realiza una conversión del adc.
    }

    i2c_Data.direction = kI2C_Write;
    i2c_Data.slaveAddress = adc->address;
    i2c_Data.data = &txConfig;
    i2c_Data.dataSize = sizeof(txConfig);
    result = I2C_RTOS_Transfer(adc->i2c, &i2c_Data);

    if (result == kStatus_Success) {
        adc->config = newconfig;
    }

    return result;
}

/**
 * Lee el valor de ADC.
 *
 * @param[in] adc Dispositivo ADC a utilizar.
 * @param[out] value Valor devuelto por el ADC.
 * @param[out] config Configuración devuelta por el ADC.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
static status_t mcp3426_read(const mcp3426_t *const adc, int16_t *const value,
                            mcp3426_config_t *const config)
{
    int32_t result;
    static uint8_t buffer[3] = {0, 0, 0};
    sample_rate_t sample_rate = adc->config.srate;
    uint16_t temp;
    uint16_t sing;

    i2c_Data.direction = kI2C_Read;
    i2c_Data.slaveAddress = adc->address;
    i2c_Data.data = buffer;
    i2c_Data.dataSize = sizeof(buffer);

    result = I2C_RTOS_Transfer(adc->i2c, &i2c_Data);

    if (result == kStatus_Success) {
        temp = (buffer[0] << 8) | buffer[1];
        *config = mcp3426_uint_to_config(buffer[2]);

        switch (sample_rate) {
            case sRate_15:  // 16 bits
                *value = temp;
                break;
            case sRate_60:  // 14 bits
                sing = (temp & 0x2000U) << 2;     // Muevo el bit de signo al bit 15 (MSB).
                *value = (temp & 0x1fffU) | sing; // El valor son los 13 bits + el bit de signo.
                break;
            case sRate_240: // 12 bits
                sing = (temp & 0x0800U) << 4;     // Muevo el bit de signo al bit 15 (MSB).
                *value = (temp & 0x07ffU) | sing; // El valor son los 11 bits + el bit de signo.
                break;
            default:
                *value = temp; // 16 bits por defecto
                break;
        }
    }
    return result;
}

/**
 * Lee el valor del ADC en el canal especificado.
 *
 * @param[in,out] adc Dispositivo ADC a utilizar.
 * @param[in] req_channel Canal a utilizar.
 * @param[out] value Valor devuelto por el ADC.
 * @param[in] wait Indica si se quiere esperar el tiempo de sampleo o no.
 *       (Ponerlo a True salvo que se sepa lo que se hace)
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp3426_read_channel(mcp3426_t *const adc, const channel_t req_channel,
                              int16_t *const value, bool wait)
{
    int32_t result;
    mcp3426_config_t config;
    static sampling_mode_t oldMode;
    bool update_config = false;

    config = adc->config;
    if (req_channel != adc->config.channel){
        config.channel = req_channel;
        update_config = true;
    }

    if (adc->config.mode == one_shot_sampling){
       result = mcp3426_update_config(adc, config, true);
       oldMode = one_shot_sampling;
       update_config = true;
    }
    else {
        // Solo actualiza la configuración en modo continuo si anteriormente
        // estaba en one shot o se ha cambiado de canal.
        if ((oldMode != continuous_sampling) || update_config){
            oldMode = continuous_sampling;
            result = mcp3426_update_config(adc, config, false);
            update_config = true;
        }
        else{
            ; // Misra c 15.7
        }
    }

    if (update_config && (result != kStatus_Success)){
        return result;
    }

    if (wait == true){
        vTaskDelay((mcp3426_read_times[adc->config.srate]+5)/portTICK_PERIOD_MS);
    }

    return mcp3426_read(adc, value, &config);
}

/**
 * Inicializa un dispositivo mcp3426.
 *
 * Debe llamarse a esta función antes de utilizar cualquier otra.
 * @param[out] mcp3426 Estructura que representa un dispositivo mcp3426.
 * @param[in] i2c Manejador para uso de i2c en FreeRTOS.
 * @param[in] address Dirección del dispositivo.
 * @param[in] default_channel Canal por defecto.
 * @param[in] mode Modo de muestreo.
 * @param[in] srate Velocidad de muestreo.
 * @param[in] gain_channel_1 Ganancia del canal 1.
 * @param[in] gain_channel_2 Ganancia del canal 2.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp3426_init(mcp3426_t *const mcp3426, i2c_rtos_handle_t * const i2c,
                     const uint8_t address, const channel_t default_channel,
                     const sampling_mode_t mode, const sample_rate_t srate,
                     const pga_gain_t gain_channel_1,const pga_gain_t gain_channel_2){

    mcp3426->i2c = i2c;
    mcp3426->address = address;
    mcp3426->config.channel = default_channel;
    mcp3426->config.mode = mode;
    mcp3426->config.srate = srate;
    mcp3426->config.gain[0] = gain_channel_1;
    mcp3426->config.gain[1] = gain_channel_2;

    return mcp3426_update_config(mcp3426, mcp3426->config, false);
}
