/*
 *
 * File: mcp3426.h
 * Project: DC Load NewBrain
 * Created Date: 8/5/2019, 11:25
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

//mcp3426

#ifndef MCP3426_MCP3426_H_
#define MCP3426_MCP3426_H_

#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"

/**
 * Número de canal del ADC.
 */
typedef enum {
    channel_1 = 0, ///< Canal 1.
    channel_2,     ///< Canal 2.
}channel_t;

/**
 * Modo de muestreo.
 */
typedef enum {
    one_shot_sampling = 0, ///< Un solo muestreo.
    continuous_sampling,   ///< Muestreo contínuo.
}sampling_mode_t;

/**
 * Velocidad de muestreo.
 */
typedef enum {
    sRate_240 = 0, ///< 240 muestras por minuto.
    sRate_60,      ///<  60 muestras por minuto.
    sRate_15,      ///<  15 muestras por minuto.
}sample_rate_t;

/**
 * Ganancia del amplificador integrado.
 */
typedef enum {
    gain_x1 = 0, ///< Ganancia 1.
    gain_x2,     ///< Ganancia 2.
    gain_x4,     ///< Ganancia 4.
    gain_x8,     ///< Ganancia 8.
}pga_gain_t;

/**
 * Estructura con la configuración del ADC.
 */
typedef struct {
    channel_t channel;    ///< Canal por defecto.
    sampling_mode_t mode; ///< Modo de muestreo.
    sample_rate_t srate;  ///< Velocidad de muestreo.
    pga_gain_t gain[2];   ///< Ganancia de cada canal.
}mcp3426_config_t;

/**
 * Estructura que representa un dispositivo mcp3426.
 */
typedef struct {
    i2c_rtos_handle_t * i2c;  ///< Manejador para uso de i2c en FreeRTOS.
    uint8_t address;          ///< Dirección del dispositivo.
    mcp3426_config_t config;  ///< Configuración del dispositivo.
}mcp3426_t;

status_t mcp3426_init(mcp3426_t *const mcp3426, i2c_rtos_handle_t * const i2c,
                     const uint8_t address, const channel_t default_channel,
                     const sampling_mode_t mode, const sample_rate_t srate,
                     const pga_gain_t gain_channel_1,const pga_gain_t gain_channel_2);

status_t mcp3426_update_config(mcp3426_t *const adc, const mcp3426_config_t newconfig,
                              const bool sample);
status_t mcp3426_read_channel(mcp3426_t *const adc, const channel_t req_channel,
                              int16_t *const value, bool wait);

#endif /* MCP3426_MCP3426_H_ */
