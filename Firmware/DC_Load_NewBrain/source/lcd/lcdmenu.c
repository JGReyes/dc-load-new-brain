/*
 *
 * File: lcdmenu.c
 * Project: DC Load NewBrain
 * Created Date: 16/5/2019, 10:27
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: 24/08/2023
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * 24/08/23     1.1     JGReyes     Se añade menú para desactivar la protección de polaridad.
 *
*/

#include "FreeRTOS.h"
#include "task.h"
#include "lcd.h"
#include "sysview_enable_conf.h"
#include "keypad/keypad.h"
#include "lcdmenu.h"
#include "peripherals.h"
#include "main.h"
#include "opmodes.h"
#include "sdlogging.h"

/**
 * Almacena la columna donde se encuentra el cursor.
 */
static uint8_t cursorColumPos = 8;

/**
 * Almacena los datos del encoder.
 */
encoder_t encoder;

/**
 * Cola para almacenar los datos de control de la pantalla.
 *
 * Cada tarea que desee escribir en la pantalla debe dejar una estructura de datos
 * en la cola y la tarea lcd_task se encarga de ir procesando las peticiones por orden.
 */
QueueHandle_t lcd_queue = NULL;

/**
 * Cola para almacenar las peticiones de introducción de datos por teclado.
 */
QueueHandle_t input_queue = NULL;

/**
 * Bloquea el uso de las teclas [1-9, * y #]
 */
volatile bool blockNumKeys = false;

/**
 * Se encaga de procesar los comandos dejados por las otras tareas en la cola
 * lcd_queue.
 *
 * @note Es la única tarea que controla directamente la pantalla LCD.
 *
 */
void lcdControl(void *pvParameter)
{

    portENTER_CRITICAL();
    encoder.position = 0;
    encoder.lastCount = 50;
    encoder.reading = 0;
    encoder.factor = 0;
    encoder.max = 999000;
    portEXIT_CRITICAL();

    lcdData_t rxData;
    i2c_lcd_t lcd;
    i2c_lcd_init(&lcd, &I2C_0_rtosHandle, 0x27, true, 4, 40, 20);

    //github.com/agnunez/ESP8266-I2C-LCD1602/blob/master/examples/CustomChars/CustomChars.ino
    uint8_t bell[8] = {0x4, 0xe, 0xe, 0xe, 0x1f, 0x0, 0x4};
    uint8_t clock[8] = {0x0, 0xe, 0x15, 0x17, 0x11, 0xe, 0x0};
    uint8_t retarrow[8] = {0x1, 0x1, 0x5, 0x9, 0x1f, 0x8, 0x4};
    uint8_t ohms[8] = {0x00, 0x0e, 0x11, 0x11, 0x11, 0x0a, 0x1b};
    uint8_t temp[8] = {0x0c, 0x12, 0x12, 0x0c, 0x00, 0x00, 0x00};
    i2c_lcd_define_char(&lcd, I2C_LCD_INDEX_CUSTOM_0, bell);
    i2c_lcd_define_char(&lcd, I2C_LCD_INDEX_CUSTOM_1, clock);
    i2c_lcd_define_char(&lcd, I2C_LCD_INDEX_CUSTOM_2, retarrow);
    i2c_lcd_define_char(&lcd, I2C_LCD_INDEX_CUSTOM_3, ohms);
    i2c_lcd_define_char(&lcd, I2C_LCD_INDEX_CUSTOM_4, temp);
    i2c_lcd_home(&lcd);

    lcd_queue = xQueueCreate(15, sizeof(lcdData_t));

    if (lcd_queue == NULL){
        svError("lcd_queue creation failed");
    }
    else
    {
        vQueueAddToRegistry(lcd_queue, "lcd_queue");
        xTaskNotify(xTaskGetHandle("task_Control"), 0, eNoAction);
        for (;;)
        {
            xQueueReceive(lcd_queue, (void *)&rxData, portMAX_DELAY);

            switch (rxData.command)
            {
            case LCD_CLEAR:
                i2c_lcd_clear(&lcd);
                break;
            case LCD_HOME:
                i2c_lcd_home(&lcd);
                break;
            case LCD_MOVE_CURSOR:
                i2c_lcd_move_cursor(&lcd, rxData.data.cursor_pos.col,
                                    rxData.data.cursor_pos.row);
                break;
            case LCD_MOVE_CURSOR_LEFT:
                i2c_lcd_move_cursor_left(&lcd);
                break;
            case LCD_MOVE_CURSOR_RIGHT:
                i2c_lcd_move_cursor_right(&lcd);
                break;
            case LCD_SCROLL_LEFT:
                i2c_lcd_scroll_display_left(&lcd);
                break;
            case LCD_SCROLL_RIGHT:
                i2c_lcd_scroll_display_right(&lcd);
                break;
            case LCD_SET_AUTO_SCROLL:
                i2c_lcd_set_auto_scroll(&lcd, rxData.data.enable);
                break;
            case LCD_SET_BACKLIGHT:
                i2c_lcd_set_backlight(&lcd, rxData.data.enable);
                break;
            case LCD_SET_BLINK:
                i2c_lcd_set_blink(&lcd, rxData.data.enable);
                break;
            case LCD_SET_CURSOR:
                i2c_lcd_set_cursor(&lcd, rxData.data.enable);
                break;
            case LCD_SET_LEFT_TO_RIGHT:
                i2c_lcd_set_left_to_right(&lcd);
                break;
            case LCD_SET_RIGHT_TO_LEFT:
                i2c_lcd_set_right_to_left(&lcd);
                break;
            case LCD_WRITE_CHAR:
                i2c_lcd_write_char(&lcd, rxData.data.c);
                break;
            case LCD_WRITE_STRING:
                i2c_lcd_write_string(&lcd, (const char *)rxData.data.str);
                break;
            default:
                break;
            }
        }
    }
}

/**
 * Enciende/apaga la luz de fondo de la pantalla.
 * @param[in] enable True para encenderla, en caso contrario False.
 *
 */
void lcdmenu_backLight(bool enable)
{
    lcdData_t data;
    data.command = LCD_SET_BACKLIGHT;
    data.data.enable = enable;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}
/**
 * Muestra una cadena de caracteres en pantalla.
 * @param[in] string Cadena a visualizar.
 */
void lcdmenu_writeString(const char *str)
{
    lcdData_t data;
    data.command = LCD_WRITE_STRING;
    data.data.str = str;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Muestra un carácter en pantalla. Incluidos los personalizados.
 * @param c Carácter a mostrar.
 */
void lcdmenu_writeChar(const char c)
{
    lcdData_t data;
    data.command = LCD_WRITE_CHAR;
    data.data.c = c;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Mueve el cursor a la posición indicada.
 * @param[in] col Columna.
 * @param[in] row Fila.
 */
void lcdmenu_moveCursor(const uint8_t col, const uint8_t row)
{
    lcdData_t data;
    cursor_position_t cursor_pos;
    cursor_pos.row = row;
    cursor_pos.col = col;
    data.command = LCD_MOVE_CURSOR;
    data.data.cursor_pos = cursor_pos;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Límpia el contenido de la pantalla.
 */
void lcdmenu_clear()
{
    lcdData_t data;
    data.command = LCD_CLEAR;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Muestra u oculta el cursor en pantalla.
 * @param[in] enable True para mostrarlo, en caso contrario False.
 */
void lcdmenu_cursor(bool enable)
{
    lcdData_t data;
    data.command = LCD_SET_CURSOR;
    data.data.enable = enable;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Hace parpadear el caracter encima del cursor.
 * @param[in] enable True para activarlo, en caso contrario False.
 */
void lcdmenu_blink(bool enable)
{
    lcdData_t data;
    data.command = LCD_SET_BLINK;
    data.data.enable = enable;
    xQueueSend(lcd_queue, (void *)&data, portMAX_DELAY);
}

/**
 * Borra una línea de la pantalla.
 * @param[in] line Número de línea a borrar (0-3).
 */
void lcdmenu_cleanLine(uint8_t line)
{
    if (line <= 3)
    {
        lcdmenu_cursor(false);
        lcdmenu_moveCursor(0, line);
        if (line == 3){
            lcdmenu_writeString("                  ");
        }
        else{
            lcdmenu_writeString("                    ");
        }
    }
}

/**
 * Muestra el estado de la carga.
 * @param[in] on True para mostrar ON en la pantalla, False para mostrar OFF.
 */
void lcdmenu_load(bool on)
{
    lcdmenu_cursor(false);
    if (on)
    {
        lcdmenu_moveCursor(8, 0);
        lcdmenu_writeString("ON ");
    }
    else
    {
        lcdmenu_moveCursor(8, 0);
        lcdmenu_writeString("OFF");
    }
}

/**
 * Muestra el mensaje de polaridad inversa en pantalla.
 */
void lcdmenu_polarityMessage()
{
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("V.Low Check Polarity");
    if (sound == true){
        buzzer_ms(1000);
    }
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    lcdmenu_cleanLine(3);
    lcdmenu_modeMenu(operationMode);
}

/**
 * Pide que se realize una entrada por teclado.
 * @param[in] col Columna en la que aparecerá el número introducido.
 * @param[in] row Fila en la que aparecerá el número introducido.
 * @param[in] wait Indica si se quiere esperar a que devuelva el valor.
 * @param[in] id Sirve para identificar la parte del código que llama a la función.
 * @return Valor introducido por teclado, en el modo de selección (0-9),
 *         sino, el valor multiplicado por 1000. (Ej. si se introduce
 *         por teclado 4.560 se devuelve 4560).
 */
uint32_t lcdmenu_requestInput(uint8_t col, uint8_t row, bool wait, uint8_t id)
{
    dcLoad_Mode_t actualMode = operationMode;
    uint32_t res = 0;
    inputValue_t inputConf;
    uint32_t waitTime = 0;

    if (wait){
        waitTime = portMAX_DELAY;
        // Cambia el modo para evitar el reset del watchdog.
        operationMode = DCLOAD_MODE_NO_CHANGE;
    }
    else{
        waitTime = 0;
    }

    inputConf.ikeypadQueue = keypadQueue;
    inputConf.col = col;
    inputConf.row = row;
    inputConf.taskToNotify = xTaskGetCurrentTaskHandle();
    inputConf.id = id;

    blockNumKeys = false;

    // Manda una petición para recibir una entrada por teclado.
    xQueueOverwrite(input_queue, (void *)&inputConf);

    // Espera a que la tarea encargada responda con el valor introducido.
    if (xTaskNotifyWait(0x00, 0xffffffff, &res, waitTime) == pdTRUE){
        // Restaura el modo en el que estaba al entrar a la función.
        operationMode = actualMode;
        return res;
    }

    return 0;
}

/**
 * Pide una selección de menú.
 * @param[in] wait Indica si se espera a la pulsación. (Bloquea).
 * @return Carácter que representa la tecla pulsada.
 */
char lcdmenu_requestSelection(bool wait)
{
    dcLoad_Mode_t actualMode = operationMode;
    uint32_t key;

    if (wait)
    {
        // Cambia el modo para evitar el reset por el watchdog
        operationMode = DCLOAD_MODE_NO_CHANGE;
        vTaskSuspend(xTaskGetHandle("inputValue_task"));
        xQueueReceive(keypadQueue, &key, portMAX_DELAY);
        // Pitido al pulsar una tecla.
        if (sound == true){
            buzzer_ms(100);
        }

        vTaskResume(xTaskGetHandle("inputValue_task"));
        // Restablece el modo de operación.
        operationMode = actualMode;
    }
    else{
        xQueueReceive(keypadQueue, &key, 0);
    }

    return keyToChar(key);
}

/**
 * Muestra el menú para selección de modo de funcionaminto.
 * @return El modo seleccionado mediante teclado.
 */
dcLoad_Mode_t lcdmenu_modeSelectionMenu()
{
    operationMode = DCLOAD_MODE_NO_CHANGE;
    transientFastMode = false;
    eBatteryCapacity = 0;

    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(4, 0);
    lcdmenu_writeString("Select Mode");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString(" 1=CC 2=CP 3=CR 4=BC");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("   5=TC 6=TP 7=TT");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString(" 8=Setup    9=LogSD");

    dcLoad_Mode_t selMode;
    char res = lcdmenu_requestSelection(true);
    switch (res)
    {
    case '1':
        encoder.factor = 100; // Por defecto ajusta las centenas de mA.
        selMode = DCLOAD_MODE_CC;
        break;
    case '2':
        encoder.factor = 1000; // Por defecto ajusta las unidades de W.
        selMode = DCLOAD_MODE_CP;
        break;
    case '3':
        encoder.factor = 10000; // Por defecto en decenas de ohmnio.
        selMode = DCLOAD_MODE_CR;
        break;
    case '4':
        encoder.factor = 100; // Por defecto ajusta las centenas de mA.
        selMode = DCLOAD_MODE_BC;
        break;
    case '5':
        selMode = DCLOAD_MODE_TC;
        break;
    case '6':
        selMode = DCLOAD_MODE_TP;
        break;
    case '7':
        selMode = DCLOAD_MODE_TT;
        break;
    case '8':
        selMode = DCLOAD_MODE_SETUP;
        break;
    case '9':
        selMode = DCLOAD_MODE_LOGSD;
        break;
    default:
        selMode = DCLOAD_MODE_NO_CHANGE;
        break;
    }

    return selMode;
}

/**
 * Muestra el menú del modo seleccionado en pantalla.
 *
 * Los datos mostrados forman un plantilla, y sobre ella se
 * actualizan los campos variables con las funciones 'update' de cada menú.
 *
 * @param[in] mode Modo actual de funcionamiento.
 */
void lcdmenu_modeMenu(dcLoad_Mode_t mode)
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_load(false);
    lcdmenu_moveCursor(0, 0);
    if (mode == DCLOAD_MODE_BC){
        lcdmenu_writeString("BATTERY");
    }
    else{
        lcdmenu_writeString("DC LOAD");
    }
    lcdmenu_cleanLine(2);
    lcdmenu_moveCursor(16, 2);
    lcdmenu_writeString("    ");
    lcdmenu_moveCursor(0, 2);
    switch (mode)
    {
    case DCLOAD_MODE_CC:
    case DCLOAD_MODE_BC:
        lcdmenu_writeString("Set I = 00.000");
        lcdmenu_moveCursor(14, 2);
        lcdmenu_writeString("A");
        lcdmenu_moveCursor(18, 3);
        if (mode == DCLOAD_MODE_CC){
            lcdmenu_writeString("CC");
        }
        else{
            lcdmenu_writeString("BC");
        }
        cursorColumPos = 11;
        break;
    case DCLOAD_MODE_CP:
        lcdmenu_writeString("Set W = 000.00");
        lcdmenu_moveCursor(14, 2);
        lcdmenu_writeString("W");
        lcdmenu_moveCursor(18, 3);
        lcdmenu_writeString("CP");
        cursorColumPos = 10;
        break;
    case DCLOAD_MODE_CR:
        lcdmenu_writeString("Set R = 000.00");
        lcdmenu_moveCursor(14, 2);
        lcdmenu_writeChar(I2C_LCD_INDEX_CUSTOM_3); // Símbolo de ohmnios.
        lcdmenu_moveCursor(18, 3);
        lcdmenu_writeString("CR");
        cursorColumPos = 9;
        break;
    case DCLOAD_MODE_NO_CHANGE:
        lcdmenu_modeSelectionMenu();
        break;
    case DCLOAD_MODE_TT:
        lcdmenu_moveCursor(18, 3);
        lcdmenu_writeString("TT");
        break;
    case DCLOAD_MODE_TC:
        lcdmenu_moveCursor(18, 3);
        lcdmenu_writeString("TC");
        break;
    case DCLOAD_MODE_TP:
        lcdmenu_moveCursor(18, 3);
        lcdmenu_writeString("TP");
        break;
    case DCLOAD_MODE_SETUP:
        // No se hace nada
        break;
    case DCLOAD_MODE_LOGSD:
        // No se hace nada
        break;
    default:
        break;
    }
    lcdmenu_cleanLine(3);
    showingWarnMessage = pdFALSE;
}

/**
 * Actualiza los valores en pantalla en los modos.
 * @param[in] current Valor de la corriente a mostrar.
 * @param[in] voltage Valor del voltage a mostrar.
 * @param[in] power   Valor de la potencia a mostrar.
 * @param[in] force   Indica si se quiere forzar el refresco.
 */
void lcdmenu_modeMenu_update(const float current, const float voltage,
                             const float power, bool force)
{
    static char current_buf[bufferLen];
    static char voltage_buf[bufferLen];
    static char power_buf[bufferLen];

    for (uint8_t i = 0; i < bufferLen; i++)
    {
        current_buf[i] = '\0';
        voltage_buf[i] = '\0';
        power_buf[i] = '\0';
    }

    static float oldCurrent = 0;
    static float oldVoltage = 0;
    static float oldPower = 0;

    // Solo actualiza la pantalla si alguno de los datos ha cambiado y no
    // se está en el modo de transitorios FastMode.
    if ((!transientFastMode) && ((current != oldCurrent) || (voltage != oldVoltage) || (power != oldPower) ||
                               force))
    {
        oldCurrent = current;
        oldVoltage = voltage;
        oldPower = power;

        lcdmenu_cursor(false);
        lcdmenu_moveCursor(0, 1);
        if (current < 10.0){
            sprintf(current_buf, "%5.3f", current);
        }
        else{
            sprintf(current_buf, "%5.2f", current);
        }
        lcdmenu_writeString(current_buf);
        lcdmenu_writeString("A ");

        if (voltage < 10.0){
            sprintf(voltage_buf, "%5.3f", voltage);
        }
        else{
            sprintf(voltage_buf, "%5.2f", voltage);
        }
        lcdmenu_writeString(voltage_buf);
        lcdmenu_writeString("V ");

        if (power < 100.0){
            sprintf(power_buf, "%4.2f", power);
        }
        else{
            sprintf(power_buf, "%4.1f", power);
        }

        lcdmenu_writeString(power_buf);

        if (power < 10.0){
            lcdmenu_writeString("W ");
        }
        else{
            lcdmenu_writeString("W");
        }
        lcdmenu_moveCursor(cursorColumPos, 2);
        if ((operationMode == DCLOAD_MODE_TC) || (operationMode == DCLOAD_MODE_TP)
            || (operationMode == DCLOAD_MODE_TT)){
            lcdmenu_cursor(false);
        }
        else{
            lcdmenu_cursor(true);
        }
    }
}

/**
 * Actualiza la temperatura de los mosfets en pantalla.
 *
 * @param[in] temp    Temperatura del mosfet 1.
 * @param[in] temp2   Temperatura del mosfet 2.
 */
void lcdmenu_temperature_update(const float temp, const float temp2)
{
    static char temp1_buf[3] = {0};
    static char temp2_buf[3] = {0};

    lcdmenu_cursor(false);
    lcdmenu_moveCursor(12, 0);
    sprintf(temp1_buf, "%2.0f", temp);
    lcdmenu_writeString(temp1_buf);
    lcdmenu_writeChar(I2C_LCD_INDEX_CUSTOM_4); // Símbolo º para los grados.
    lcdmenu_writeString("C");
    sprintf(temp2_buf, "%2.0f", temp2);
    lcdmenu_writeString(temp2_buf);
    lcdmenu_writeChar(I2C_LCD_INDEX_CUSTOM_4);
    lcdmenu_writeString("C");
}

/**
 * Muestra el menú de selección del tipo de batería.
 */
void lcdmenu_batteryTypeMenu()
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(0, 0);
    lcdmenu_writeString("Select Battery Type");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("1=LiPo/Li-Ion 2=LiFe");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("3=NiCd/NiMH   4=ZiZn");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("5=Set Voltage 6=Exit");
}

/**
 * Muestra el menú de selección del número de celdas de la batería.
 *
 * @param[in] Voltage de corte del tipo de batería.
 * @return El voltage de corte según el número de celdas indicado. 0 en caso de valor no válido.
 */
float lcdmenu_cellsCountMenu(const float batteryCutoffVolt)
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(1, 1);
    lcdmenu_writeString("Number of cells in");
    lcdmenu_moveCursor(3, 2);
    lcdmenu_writeString("series? [1-8]:");

    char res = lcdmenu_requestSelection(true);
    if ((res > '0') && (res <= '8'))
    {
        lcdmenu_clear();
        return batteryCutoffVolt * (uint8_t)(res - '0');
    }
    return 0;
}

/**
 * Muestra el menú para introducir el voltaje de corte para la descarga.
 *
 * @return El voltage de corte indicado.
 */
float lcdmenu_batteryCutOffMenu()
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(4, 0);
    lcdmenu_writeString("Enter Battery");
    lcdmenu_moveCursor(3, 1);
    lcdmenu_writeString("Cut-Off Voltage");

    uint32_t res = lcdmenu_requestInput(8, 2, true, 1);
    lcdmenu_clear();
    encoder.reading = 0;
    encoder.position = 0;
    return (res / 1000.0);
}

/**
 * Muestra el menú para introducir los mAh de descarga.
 *
 * @return Descarga de corte indicada.
 */
float lcdmenu_batteryDischargeCutOffMenu()
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(4, 0);
    lcdmenu_writeString("Enter Battery");
    lcdmenu_moveCursor(2, 1);
    lcdmenu_writeString("Discharge Cutoff");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("0 to disable.(mAh)");

    uint32_t res = lcdmenu_requestInput(8, 2, true, 35);
    lcdmenu_clear();
    encoder.reading = 0;
    encoder.position = 0;
    return (res / 1000.0);
}

/**
 * Muestra el menú con información de la batería seleccioada.
 *
 * @param[in] batteryType Texto con el tipo de batería seleccionada.
 * @param[in] batteryCutOffVolts Texto con el voltage de corte correspondiente.
 */
void lcdmenu_batterySelectedInfoMenu(const char *const batteryType,
                                     const char *const batteryCutOffVolts)
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(2, 0);
    lcdmenu_writeString("Battery Selected");
    lcdmenu_moveCursor(8, 1);
    lcdmenu_writeString(batteryType);
    lcdmenu_moveCursor(2, 2);
    lcdmenu_writeString("Discharge Cutoff");
    lcdmenu_moveCursor(6, 3);
    lcdmenu_writeString(batteryCutOffVolts);
    lcdmenu_writeString(" volts");
}

/**
 * Función para configurar la fecha y hora del reloj.
 *
 * @note No se sale de la función hasta introducir una fecha y hora válidos.
 */
void lcdmenu_configDateTime()
{
    rtc_datetime_t datetime;
    float tmp;

    do
    {
        lcdmenu_clear();
        lcdmenu_moveCursor(4, 0);
        lcdmenu_writeString("User Set-Up");
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Day[1-31]:");
        tmp = (lcdmenu_requestInput(10, 2, true, 24) / 1000.0);
        datetime.day = (uint8_t)tmp;
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Month[1-12]:");
        tmp = (lcdmenu_requestInput(12, 2, true, 25) / 1000.0);
        datetime.month = (uint8_t)tmp;
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Year[1970-2099]:");
        tmp = (lcdmenu_requestInput(16, 2, true, 26) / 1000.0);
        datetime.year = (uint16_t)tmp;
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Hour[0-23]:");
        tmp = (lcdmenu_requestInput(11, 2, true, 27) / 1000.0);
        datetime.hour = (uint8_t)tmp;
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Minute[0-59]:");
        tmp = (lcdmenu_requestInput(13, 2, true, 28) / 1000.0);
        datetime.minute = (uint8_t)tmp;
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Second[0-59]:");
        tmp = (lcdmenu_requestInput(13, 2, true, 29) / 1000.0);
        datetime.second = (uint8_t)tmp;

        RTC_StopTimer(RTC_1_PERIPHERAL);
    } while (RTC_SetDatetime(RTC_1_PERIPHERAL, &datetime) != kStatus_Success);

    RTC_StartTimer(RTC_1_PERIPHERAL);
    encoder.reading = 0;
    encoder.position = 0;
}

/**
 * Menú para configurar la exportación de datos a SD.
 * @param[in] loggingTime Tiempo entre escrituras (en ms) en la SD.
 *
 * @note También permite la configuración y comprobación del reloj.
 */
void lcdmenu_configSDLogging(uint32_t *const loggingTime)
{
    showTemp = false;
    float tmp;

    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(5, 0);
    lcdmenu_writeString("SD Logging");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString(" 1=Set Clock");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString(" 2=View Clock");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString(" 3=Enable logging");
    char res = lcdmenu_requestSelection(true);
    switch (res)
    {
    case '1':
        lcdmenu_configDateTime();
        break;
    case '2':
        vTaskSuspend(xTaskGetHandle("inputValue_task"));
        lcdmenu_showDateTime();
        vTaskResume(xTaskGetHandle("inputValue_task"));
        break;
    case '3':
        lcdmenu_clear();
        lcdmenu_moveCursor(5, 0);
        lcdmenu_writeString("SD Logging");
        lcdmenu_moveCursor(1, 1);
        lcdmenu_writeString("Press 0 to disable");
        lcdmenu_moveCursor(1, 2);
        lcdmenu_writeString("Press 1 to enable");
        lcdmenu_moveCursor(0, 3);
        lcdmenu_writeString("Re-insert SD if 1");

        if (lcdmenu_requestSelection(true) == '0'){
            enableLogging = false;
        }
        else{
            enableLogging = true;
        }

        lcdmenu_clear();
        lcdmenu_moveCursor(5, 0);
        lcdmenu_writeString("SD Logging");
        lcdmenu_moveCursor(0, 1);
        lcdmenu_writeString("Set sample time[0.2");
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("to 36000 seconds.");
        lcdmenu_moveCursor(0, 3);
        lcdmenu_writeString("Time = ");
        lcdmenu_moveCursor(17, 3);
        lcdmenu_writeString("Sec");

        tmp = (lcdmenu_requestInput(7, 3, true, 30) / 1000.0);

        if (tmp < 0.2){
            *loggingTime = 200;
        }
        else if (tmp > 36000.0){
            *loggingTime = 36000000;
        }
        else{
            *loggingTime = (uint32_t)(tmp * 1000);  // Se vuelve a pasar a ms.
        }

        // La resolución de los timers de FreeRTOS es de 5ms.
        if ((*loggingTime % 5) != 0)
        {
            do
            {
                *loggingTime -= 1;
            } while ((*loggingTime % 5) != 0);
        }
        break;
    default:
        break;
    }

    lcdmenu_load(false);
    encoder.reading = 0;
    encoder.position = 0;
    showTemp = true;
}

/**
 * Muestra la fecha y hora por pantalla.
 *
 * @note Para salir pulsar la tecla * del keypad.
 */
void lcdmenu_showDateTime()
{
    rtc_datetime_t datetime;
    static char buffer[20] = {'\0'};
    TickType_t xLastWakeTime;

    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(3, 0);
    lcdmenu_writeString("Date and Time:");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("* To exit.");

    // Obtiene el tiempo actual.
    xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        lcdmenu_cleanLine(2);
        lcdmenu_moveCursor(0, 2);
        RTC_GetDatetime(RTC_1_PERIPHERAL, &datetime);
        sprintf(buffer, "%02d-%02d-%04d %02d:%02d:%02d", datetime.day, datetime.month,
                datetime.year, datetime.hour, datetime.minute, datetime.second);
        lcdmenu_writeString(buffer);
        // Para actualizarse cada segundo.
        vTaskDelayUntil(&xLastWakeTime, 1000 / portTICK_PERIOD_MS);

        char res = lcdmenu_requestSelection(false);
        if (res == '*'){
            break;
        }
    }
}

/**
 * Muestra el menú de ajustes de usuario.
 *
 * @param[out] currentCutOff Límite de corriente indicado.
 * @param[out] powerCutOff Límite de potencia indicado.
 * @param[out] voltageCutOff Límite de voltage indicado.
 * @param[out] maxBatteryCurrent Límite de corriente para descarga de baterías.
 * @param[out] soundEnable Indica si se activa o no el buzzer.
 * @param[out] polarityEnable Indica si se activa o no la protección por polaridad inversa.
 * @param[out] rTime Tiempo de subida al conectar la carga (en ms).
 * @param[out] fTime Tiempo de bajada al conectar la carga (en ms).
 */
void lcdmenu_userSetUpMenu(float *const currentCutOff,
                           float *const powerCutOff,
                           float *const voltageCutOff,
                           float *const maxBatteryCurrent,
                           volatile bool *const soundEnable,
                           volatile bool *const polarityEnable,
                           volatile uint16_t *const rise,
                           volatile uint16_t *const fall)
{
    static char buffer1[bufferLen];
    static char buffer2[bufferLen];
    static char buffer3[bufferLen];

    for (uint8_t i = 0; i < bufferLen; i++)
    {
        buffer1[i] = '\0';
        buffer2[i] = '\0';
        buffer3[i] = '\0';
    }

    float tmp;
    showTemp = false;
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(4, 0);
    lcdmenu_writeString("User Set-Up");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("Current Lim=");
    lcdmenu_moveCursor(19, 1);
    lcdmenu_writeString("A");

    tmp = (lcdmenu_requestInput(12, 1, true, 20) / 1000.0);
    if (tmp >= 15.5){
        *currentCutOff = 15.2;
    }
    else{
        *currentCutOff = tmp;
    }
    lcdmenu_moveCursor(12, 1);
    sprintf(buffer1, "%5.2f", *currentCutOff);
    lcdmenu_writeString(buffer1);

    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("Power Limit=");
    lcdmenu_moveCursor(19, 2);
    lcdmenu_writeString("W");

    tmp = (lcdmenu_requestInput(12, 2, true, 21) / 1000.0);
    if (tmp >= 310.0){
        *powerCutOff = 310.0;
    }
    else{
        *powerCutOff = tmp;
    }
    lcdmenu_moveCursor(12, 2);
    sprintf(buffer2, "%5.1f", *powerCutOff);
    lcdmenu_writeString(buffer2);

    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("Voltage    =");
    lcdmenu_moveCursor(18, 3);
    lcdmenu_writeString("V");

    tmp = (lcdmenu_requestInput(12, 3, true, 22) / 1000.0);
    if (tmp >= 100.0){
        *voltageCutOff = 100.0;
    }
    else{
        *voltageCutOff = tmp;
    }
    lcdmenu_moveCursor(12, 3);
    sprintf(buffer3, "%5.1f", *voltageCutOff);
    lcdmenu_writeString(buffer3);

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    lcdmenu_clear();

    // Reutilizo los buffers anteriores.
    for (uint8_t i = 0; i < bufferLen; i++)
    {
        buffer1[i] = '\0';
        buffer2[i] = '\0';
        buffer3[i] = '\0';
    }

    lcdmenu_moveCursor(4, 0);
    lcdmenu_writeString("User Set-Up");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("Max.Batt.C =");
    lcdmenu_moveCursor(19, 1);
    lcdmenu_writeString("A");

    tmp = (lcdmenu_requestInput(12, 1, true, 23) / 1000.0);
    if (tmp >= 10.0){
        *maxBatteryCurrent = 10.0;
    }
    else{
        *maxBatteryCurrent = tmp;
    }
    lcdmenu_moveCursor(12, 1);
    sprintf(buffer1, "%5.2f", *maxBatteryCurrent);
    lcdmenu_writeString(buffer1);

    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("Sound= 0 or 1");

    if (lcdmenu_requestSelection(true) == '0'){
        *soundEnable = false;
    }
    else{
        *soundEnable = true;
    }

    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("Polarity= 0 or 1");

    if (lcdmenu_requestSelection(true) == '0'){
        *polarityEnable = false;
    }
    else{
        *polarityEnable = true;
    }

    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(5, 0);
    lcdmenu_writeString("Slew Rate");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("Rise =         mA/ms");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("Fall =         mA/ms");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("[4 - 5000] mA");

    tmp = (lcdmenu_requestInput(7, 1, true, 31) / 1000.0);
    if (tmp < 4){
       *rise = 4;
    }
    else if (tmp > 5000){
       *rise = 5000;
    }
    else{
       *rise = (uint16_t)tmp;
    }

    lcdmenu_moveCursor(8, 1);
    sprintf(buffer2, "%4d", *rise);
    lcdmenu_writeString(buffer2);

    tmp = (lcdmenu_requestInput(7, 2, true, 32) / 1000.0);

    if (tmp < 4){
       *fall = 1;
    }
    else if (tmp > 5000){
       *fall = 5000;
    }
    else{
       *fall = (uint16_t)tmp;
    }

    lcdmenu_moveCursor(8, 2);
    sprintf(buffer3, "%4d", *fall);
    lcdmenu_writeString(buffer3);

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    lcdmenu_configDateTime();

    lcdmenu_load(false);
    encoder.reading = 0;
    encoder.position = 0;
    showTemp = true;
}

/**
 * Muestra el menú del modo transitorio.
 *
 * @param[in] mode Modo de transitorio utilizado actualmente.
 * @param[in] lowCurrent Valor mínimo de corriente.
 * @param[in] highCurrent Valor máximo de corriente
 * @param[in] transientPeriod Tiempo entre transiciones.
 */
void lcdmenu_transientMenu_update(dcLoad_Mode_t mode, const float lowCurrent,
                                  const float highCurrent, const uint32_t transientPeriod)
{
    static char low_buf[bufferLen];
    static char high_buf[bufferLen];
    static char period_buf[bufferLen];

    for (uint8_t i = 0; i < bufferLen; i++)
    {
        low_buf[i] = '\0';
        high_buf[i] = '\0';
        period_buf[i] = '\0';
    }

    lcdmenu_cursor(false);

    if ((mode == DCLOAD_MODE_TC) || (mode == DCLOAD_MODE_TP) || (mode == DCLOAD_MODE_TT))
    {
        lcdmenu_moveCursor(0, 0);
        lcdmenu_writeString("DC LOAD");
        lcdmenu_moveCursor(0, 2);
        lcdmenu_writeString("Lo=");
        lcdmenu_moveCursor(3, 2);
        if (lowCurrent < 10.0){
            sprintf(low_buf, "%5.3f", lowCurrent);
        }
        else{
            sprintf(low_buf, "%5.2f", lowCurrent);
        }
        lcdmenu_writeString(low_buf);
        lcdmenu_moveCursor(8, 2);
        lcdmenu_writeString("A");
        lcdmenu_moveCursor(11, 2);
        lcdmenu_writeString("Hi=");
        lcdmenu_moveCursor(14, 2);
        if (highCurrent < 10.0){
            sprintf(high_buf, "%5.3f", highCurrent);
        }
        else{
            sprintf(high_buf, "%5.2f", highCurrent);
        }
        lcdmenu_writeString(high_buf);
        lcdmenu_moveCursor(19, 2);
        lcdmenu_writeString("A");
    }

    if ((mode == DCLOAD_MODE_TC) || (mode == DCLOAD_MODE_TP))
    {
        lcdmenu_moveCursor(0, 3);
        lcdmenu_writeString("Time = ");
        lcdmenu_moveCursor(6, 3);
        sprintf(period_buf, "%5u", transientPeriod);
        lcdmenu_writeString(period_buf);
        lcdmenu_moveCursor(12, 3);
        lcdmenu_writeString("mSecs");
    }
    else
    {
        lcdmenu_moveCursor(0, 3);
        lcdmenu_writeString("  ");
    }
}

/**
 * Muestra el menú del modo de transitorio seleccionado.
 *
 * @param[in] mode Modo de transitorio utilizado actualmente.
 * @param[out] lowCurrent Valor mínimo de corriente.
 * @param[out] highCurrent Valor máximo de corriente
 * @param[out] transientPeriod Tiempo entre transiciones en milisegundos.
 * @param[in] currentCutOff Límite de corriente de la carga electrónica.
 */
void lcdmenu_transientModeMenu(dcLoad_Mode_t mode,
                               float *const lowCurrent,
                               float *const highCurrent,
                               uint32_t *const transientPeriod,
                               const float currentCutOff)
{
    static char low_buf[bufferLen];
    static char high_buf[bufferLen];
    static char period_buf[bufferLen];

    for (uint8_t i = 0; i < bufferLen; i++)
    {
        low_buf[i] = '\0';
        high_buf[i] = '\0';
        period_buf[i] = '\0';
    }

    float tmp;
    lcdmenu_cursor(false);
    showTemp = false;
    lcdmenu_clear();
    lcdmenu_moveCursor(3, 0);
    lcdmenu_writeString("Transient Mode");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("Set Low  I= ");
    lcdmenu_moveCursor(19, 1);
    lcdmenu_writeString("A");

    tmp = (lcdmenu_requestInput(12, 1, true, 5) / 1000.0);
    if (tmp >= currentCutOff){
        *lowCurrent = currentCutOff;
    }
    else{
        *lowCurrent = tmp;
    }
    lcdmenu_moveCursor(12, 1);
    sprintf(low_buf, "%5.3f", *lowCurrent);
    lcdmenu_writeString(low_buf);

    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("Set High I= ");
    lcdmenu_moveCursor(19, 2);
    lcdmenu_writeString("A");

    tmp = (lcdmenu_requestInput(12, 2, true, 6) / 1000.0);
    if (tmp >= currentCutOff){
        *highCurrent = currentCutOff;
    }
    else{
        *highCurrent = tmp;
    }
    lcdmenu_moveCursor(12, 2);
    sprintf(high_buf, "%5.3f", *highCurrent);
    lcdmenu_writeString(high_buf);

    if ((mode == DCLOAD_MODE_TC) || (mode == DCLOAD_MODE_TP))
    {
        lcdmenu_moveCursor(0, 3);
        lcdmenu_writeString("Set Time = ");
        lcdmenu_moveCursor(16, 3);
        lcdmenu_writeString("mSec");

        uint32_t utmp = lcdmenu_requestInput(10, 3, true, 7) / 1000;
        if (utmp < 10){
            *transientPeriod = 10;
        }
        else if (utmp > 99999){
            *transientPeriod = 99999;
        }
        else{
            *transientPeriod = utmp;
        }

        // La resolución de los timers de FreeRTOS es de 5ms.
        if ((*transientPeriod % 5) != 0)
        {
            do
            {
                *transientPeriod -= 1;
            } while ((*transientPeriod % 5) != 0);
        }

        lcdmenu_moveCursor(9, 3);
        sprintf(period_buf, "%5u", *transientPeriod);
        lcdmenu_writeString(period_buf);
    }

    lcdmenu_clear();
    lcdmenu_load(false);

    lcdmenu_moveCursor(3, 0);
    lcdmenu_writeString("Use FastMode?");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("This option disable");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("display updates.");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("  1-Yes       2-No  ");
    char res = lcdmenu_requestSelection(true);
    switch (res)
    {
    case '1':
        transientFastMode = true;
        break;
    case '2':
        transientFastMode = false;
        break;
    default:
        transientFastMode = false;
        break;
    }

    // Se ponen a 0 cada vez que se cambia de modo.
    lowCurrent_dac = 0;
    highCurrent_dac = 0;

    lcdmenu_clear();
    lcdmenu_cursor(false);
    encoder.reading = 0;
    encoder.position = 0;
    showTemp = true;
}

/**
 * Muestra el menú de resistencia interna de la batería.
 */
void lcdmenu_batteryIntResistMenu()
{
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(6, 0);
    lcdmenu_writeString("Battery");
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("Internal Resistance");
    lcdmenu_moveCursor(11, 2);
    lcdmenu_writeString("m");
    lcdmenu_writeChar(I2C_LCD_INDEX_CUSTOM_3); // Símbolo de ohmnios.
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("Any key to exit.");
}

/**
 * Muestra la pantalla de introducción.
 */
void lcdmenu_intro()
{
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(2, 0);
    lcdmenu_writeString("CIRCUITEANDO.NET");
    lcdmenu_moveCursor(2, 1);
    lcdmenu_writeString("GitLab: JGReyes");
    lcdmenu_moveCursor(1, 2);
    lcdmenu_writeString("DC Load Power: 300W");
    lcdmenu_moveCursor(1, 3);
    lcdmenu_writeString("Soft. Ver. 1.1 RTOS");
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    lcdmenu_clear();
}


/**
 * Muestra los atajos de teclas disponibles.
 */
void lcdmenu_shortcuts()
{
    showTemp = false;
    lcdmenu_clear();
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(0, 1);
    lcdmenu_writeString("A=Show this info.");
    lcdmenu_moveCursor(0, 2);
    lcdmenu_writeString("B=On/Off Slew Rates");
    lcdmenu_moveCursor(0, 3);
    lcdmenu_writeString("C=Clear entry");
    lcdmenu_moveCursor(0, 0);
    lcdmenu_writeString("D=Lock/Unlock Enc.");
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    lcdmenu_clear();
    showTemp = true;
}

/**
 * Tarea encargada de la introducción de datos por el teclado y
 * su visualización en pantalla.
 */
void lcdmenu_inputValueTask(void *pvParameter)
{

    bool decimalPoint = false; // Para comprobar que ya existe un punto decimal.
    uint8_t actualCol = 0;     // Almacenan la posición actual conforme se van
    uint8_t actualRow = 0;     // pulsando las teclas.
    char numbers[20] = {0};    // Almacena la entrada por teclado en caracteres.
    uint8_t index = 0;         // Índice para el array numbers.
    uint32_t key;              // Almacena las tecla recibidas mediante la cola.
    static char ckey;          // Almacena la tecla recibida en formato carácter.
    bool receivedKey = false;  // Indica si se ha recibido una tecla.
    bool clearNumbers = false; // Indica si se deben poner a cero el array numbers.
    inputValue_t rxData;       // Datos con la posición en pantalla y tarea a notificar.
    static uint8_t callingId;  // Identificador del código que solicitó la entrada por teclado.
    input_queue = xQueueCreate(1, sizeof(inputValue_t));

    if (input_queue == NULL){
        svError("input_queue creation failed");
    }
    else{
        vQueueAddToRegistry(input_queue, "input_queue");
    }

    xQueueReceive(input_queue, (void *)&rxData, portMAX_DELAY);
    for (;;)
    {
        receivedKey = xQueueReceive(rxData.ikeypadQueue, &key, portMAX_DELAY);

        // Comprueba que si la petición viene de una nueva parte del código.
        if (xQueueReceive(input_queue, (void *)&rxData, 0) == pdTRUE)
        {
            if (rxData.id != callingId)
            {
                callingId = rxData.id;
                clearNumbers = true;
            }
        }

        if (receivedKey)
        {
            lcdmenu_cursor(false);
            // Pitido al pulsar una tecla.
            if (sound == true){
                buzzer_ms(100);
            }

            if (clearNumbers)
            {
                clearNumbers = false;
                index = 0;
                actualCol = rxData.col;
                actualRow = rxData.row;
                decimalPoint = false;
                for (uint8_t i = 0; i < 20; i++){
                    numbers[i] = '\0';
                }
            }

            ckey = keyToChar(key);
            svPrint("lcdmenu input key: ");
            svPrint(&ckey);

            if (ckey == 'D')
            {   // Tecla para bloquear el encoder.
                if (lockEncoder == true){
                    lockEncoder = false;
                }
                else{
                    lockEncoder = true;
                }

                continue;
            }

            if (ckey == 'B')
            {   // Activa/desactiva los tiempos de subida y bajada.
                if (slewEnable == true){
                    slewEnable = false;
                }
                else{
                    slewEnable = true;
                }
                continue;
            }

            if (ckey == 'A')
            {  // Muestra pantalla con las funciones de las teclas A-D.
                xTaskNotify(rxData.taskToNotify, MENU_SHOTCUTS_VALUE,
                            eSetValueWithOverwrite);
                continue;
            }

            // El el modo de batería solo se bloquean las teclas numéricas.
            if (blockNumKeys == true){
                continue;
            }

            if ((ckey >= '0') && (ckey <= '9'))
            {
                numbers[index] = ckey;
                index++;
                lcdmenu_moveCursor(actualCol, actualRow);
                lcdmenu_writeChar(ckey);
                actualCol = actualCol + 1;
            }

            if (ckey == '*')
            { // Representa en punto decimal.
                if (!decimalPoint)
                { // Comprueba que no se haya introducido anteriormente.
                    numbers[index] = '.';
                    index++;
                    lcdmenu_moveCursor(actualCol, actualRow);
                    lcdmenu_writeString(".");
                    actualCol = actualCol + 1;
                    decimalPoint = true;
                }
            }

            if (ckey == 'C')
            { // Tecla par borrar los datos introducidos.
                actualCol = rxData.col;
                lcdmenu_moveCursor(actualCol, actualRow);
                lcdmenu_writeString("      ");
                clearNumbers = true;
            }

            if ((ckey == '#') && (index > 0))
            { // Acepta el dato introducido.
                clearNumbers = true;
                actualCol = rxData.col;
                lcdmenu_moveCursor(actualCol, actualRow);
                lcdmenu_writeString("      ");
                portENTER_CRITICAL();
                // Actualiza el valor del encoder al introducido por teclado.
                encoder.reading = strtof(numbers, NULL);
                encoder.position = encoder.reading * 1000.0;
                portEXIT_CRITICAL();
                xTaskNotify(rxData.taskToNotify, (uint32_t)(strtof(numbers, NULL) * 1000),
                            eSetValueWithOverwrite);
                callingId = 0;
            }
        }
    }
}

/**
 * Cambia la posición del cursor según el modo.
 *
 * @note Esta función debe llamarse cuando se pulse el botón Cursor.
 * @param[in] mode Modo de funcionamiento de la carga electrónica.
 * @return El factor de multiplicación correspondiente a la posición del cursor.
 */
uint16_t lcdmenu_cursorPosition(dcLoad_Mode_t mode)
{
    // Por defecto 2 dígitos antes del punto decimal y 3 después.
    uint8_t unitPosition = 9;
    uint16_t factor = 0; // Factor de multiplicación.

    if ((mode == DCLOAD_MODE_TC) || (mode == DCLOAD_MODE_TP) ||
        (mode == DCLOAD_MODE_TT)){
        return 0;
    }
    // La potencia puede tener 3 dígitos antes del punto decimal, pero solo 2 decimales.
    if ((mode == DCLOAD_MODE_CP) || (mode == DCLOAD_MODE_CR)){
        unitPosition = 10;
    }

    cursorColumPos += 1;

    if (cursorColumPos == (unitPosition + 1)){
        cursorColumPos = cursorColumPos + 1;
    }

    // Si el cursor llega a la última posición, vuelve a la primera.
    if (cursorColumPos > 13)
    {
        if ((mode == DCLOAD_MODE_CP) || (mode == DCLOAD_MODE_CR)){
            cursorColumPos = unitPosition - 1;
        }
        else{
            cursorColumPos = unitPosition;
        }
    }

    if (cursorColumPos == (unitPosition + 4))
    {
        factor = 1;
    }
    if (cursorColumPos == (unitPosition + 3))
    {
        factor = 10;
    }
    if (cursorColumPos == (unitPosition + 2))
    {
        factor = 100;
    }
    if (cursorColumPos == unitPosition)
    {
        factor = 1000;
    }
    if (cursorColumPos == (unitPosition - 1))
    {
        factor = 10000;
    }

    lcdmenu_moveCursor(cursorColumPos, 2);
    lcdmenu_cursor(true);
    return factor;
}

/**
 * Muestra el valor del dato a modificar según la posición del encoder.
 * @param[in] mode Modo de funcionamiento de la carga electrónica.
 */
void lcdmenu_displayEncReading(dcLoad_Mode_t mode)
{
    static char encoder_buf[bufferLen];
    static float lastReading = 0;

    if (encoder.reading != lastReading)
    {
        for (uint8_t i = 0; i < bufferLen; i++)
        {
            encoder_buf[i] = '\0';
        }

        lcdmenu_moveCursor(8, 2); // Posición inicial.

        if (((mode == DCLOAD_MODE_CP) || (mode == DCLOAD_MODE_CR)) && (encoder.reading < 100.0))
        {
            lcdmenu_writeString("0");
        }

        if (encoder.reading < 10.0)
        { // Se añade un cero si la lectura es menos de 10.
            lcdmenu_writeString("0");
        }

        //Muestra la lectura del encoder en pantalla.
        if ((mode == DCLOAD_MODE_CP) || (mode == DCLOAD_MODE_CR))
        {
            sprintf(encoder_buf, "%4.2f", encoder.reading);
            lcdmenu_writeString(encoder_buf);
        }
        else
        {
            sprintf(encoder_buf, "%5.3f", encoder.reading);
            lcdmenu_writeString(encoder_buf);
        }

        lcdmenu_moveCursor(cursorColumPos, 2);
        lcdmenu_cursor(true);
        lastReading = encoder.reading;
        if (sound == true){
            buzzer_ms(100);
        }
    }
}

/**
 * Limita el valor introducido con el encoder a un máximo.
 *
 * @param[in] mode Modo de funcionamiento de la carga electrónica.
 * @param[in] maxCurrent Corriente máxima.
 * @param[in] maxVoltage Voltage máximo.
 * @param[in] maxPower Potencia máxima.
 * @param[in] maxBatteryCurrent Corriente máxima al descargar un batería.
 * @param[in] maxResistor Resistencia máxima.
 */
void lcdmenu_maxSettings(dcLoad_Mode_t mode, float maxCurrent,
                         float maxVoltage, float maxPower,
                         float maxBatteryCurrent, int16_t maxResistor)
{

    if ((mode == DCLOAD_MODE_CC) && (encoder.reading > maxCurrent))
    {
        portENTER_CRITICAL();
        encoder.reading = maxCurrent;
        encoder.position = maxCurrent * 1000.0;
        portEXIT_CRITICAL();
        lcdmenu_cleanLine(3);
        lcdmenu_moveCursor(cursorColumPos, 2);
    }

    if ((mode == DCLOAD_MODE_CP) && (encoder.reading > maxPower))
    {
        portENTER_CRITICAL();
        encoder.reading = maxPower;
        encoder.position = maxPower * 1000.0;
        portEXIT_CRITICAL();
        lcdmenu_cleanLine(3);
    }

    if ((mode == DCLOAD_MODE_CR) && (encoder.reading > (float)maxResistor))
    {
        portENTER_CRITICAL();
        encoder.reading = (float)maxResistor;
        encoder.position = ((float)maxResistor) * 1000.0;
        portEXIT_CRITICAL();
        lcdmenu_cleanLine(3);
    }

    if ((mode == DCLOAD_MODE_BC) && (encoder.reading > maxBatteryCurrent))
    {
        portENTER_CRITICAL();
        encoder.reading = maxBatteryCurrent;
        encoder.position = maxBatteryCurrent * 1000.0;
        portEXIT_CRITICAL();
    }
}

/**
 * Muentra mensajes de advertencia de los modos de protección.
 *
 * @param[in] Tipo de mensaje a mostrar.
 */
void lcdmenu_warning_message(warning_t message)
{
    showingWarnMessage = pdTRUE;
    resetEncoder();
    lcdmenu_cleanLine(3);
    lcdmenu_cursor(false);
    lcdmenu_moveCursor(0, 3);
    switch (message){
        case WARNING_OVER_TEMPERATURE:
            lcdmenu_writeString("Over Temperature");
            break;
        case WARNING_OVER_VOLTAGE:
            lcdmenu_writeString("Over Voltage");
            break;
        case WARNING_OVER_POWER:
            lcdmenu_writeString("Exceeded Power");
            break;
        case WARNING_OVER_CURRENT:
            lcdmenu_writeString("Over Current");
            break;
        case WARNING_SD_FAILED:
            lcdmenu_writeString("SD operation failed");
            break;
        case WARNING_SD_LOCKED:
            lcdmenu_writeString("SD card is lock");
            break;
        case WARNING_USB_CONNECTED:
            lcdmenu_writeString("USB Connected");
            break;
        default:
            break;
    }

    // Se asegura de informar al hilo del wachtdog antes de hacer el delay.
    isAlive = true;

    if (sound == true){
        buzzer_ms(1000);
    }
    else{
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    lcdmenu_modeMenu(operationMode);
}
