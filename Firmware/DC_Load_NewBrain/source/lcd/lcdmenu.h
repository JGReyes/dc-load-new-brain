/*
 *
 * File: lcdmenu.h
 * Project: DC Load NewBrain
 * Created Date: 16/5/2019, 18:43
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: 24/08/2023
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * 24/08/23     1.1     JGReyes     Se añade menú para desactivar la protección de polaridad.
 *
*/
#ifndef LCD_LCDMENU_H_
#define LCD_LCDMENU_H_



#include "task.h"
#include <stdio.h>

#define bufferLen 6 // Longitud de los buffers.

/**
 * Comandos para controlar la pantalla LCD.
 */
typedef enum
{
    LCD_CLEAR = 1,         ///< Límpia la pantalla.
    LCD_HOME,              ///< Mueve el cursor a la posición inicial.
    LCD_MOVE_CURSOR,       ///< Mueve el cursor.
    LCD_SET_BACKLIGHT,     ///< Enciende/apaga la luz de fondo.
    LCD_SET_CURSOR,        ///< Activa/desactiva el cursor.
    LCD_SET_BLINK,         ///< Pone a parpadear el cursor.
    LCD_SET_LEFT_TO_RIGHT, ///< El texto se escribe de izquierda a derecha.
    LCD_SET_RIGHT_TO_LEFT, ///< El texto se escribe de derecha a izquierda.
    LCD_SET_AUTO_SCROLL,   ///< Activa/desactiva el desplazamiento automático.
    LCD_SCROLL_LEFT,       ///< Se desplaza el texto a la izquierda.
    LCD_SCROLL_RIGHT,      ///< Se desplaza el texto a la derecha.
    LCD_MOVE_CURSOR_RIGHT, ///< Mueve el cursor a la derecha.
    LCD_MOVE_CURSOR_LEFT,  ///< Mueve el cursor a la izquierda.
    LCD_WRITE_CHAR,        ///< Escribe un carácter en la pantalla.
    LCD_WRITE_STRING       ///< Escribe una cadena de texto en la pantalla.
} lcdCommand_t;

/**
 * Tipos de mensajes de protección.
 */
typedef enum
{
    WARNING_OVER_TEMPERATURE = 0, ///< Mensaje de temperatura elevada.
    WARNING_OVER_VOLTAGE,         ///< Mensaje de sobre voltaje.
    WARNING_OVER_POWER,           ///< Mensaje de potencia excesiva.
    WARNING_OVER_CURRENT,         ///< Mensaje de corriente excesiva.
    WARNING_SD_FAILED,            ///< Mensaje de fallo en la tarjeta SD.
    WARNING_SD_LOCKED,            ///< Mensaje de tarjeta SD con protección contra escritura.
    WARNING_USB_CONNECTED         ///< Mensaje de conexión USB con la carga encencida.
} warning_t;

/**
 * Indica la posición del cursor en pantalla.
 */
typedef struct
{
    uint8_t col; ///< Número de columna. Empieza en 0, siendo la más a la izquierda.
    uint8_t row; ///< Número de fila. Empieza en 0, siendo la situada más hacia arriba.
} cursor_position_t;

/**
 * Almacena cuaquiera de los parámetros que esperan los comandos de pantalla.
 */
union lcd_command_data {
    bool enable;                  ///< Si debe Activarse/desactivarse el comando.
    cursor_position_t cursor_pos; ///< La posición del cursor.
    const char *str;              ///< Cadena de texto a mostrar.
    char c;                       ///< Carácter a mostrar.
};

/**
 * Estructura con los datos para controlar la pantalla.
 */
typedef struct
{
    lcdCommand_t command;        ///< Comando a ejecutar por la pantalla.
    union lcd_command_data data; ///< Datos asociados al comando.
} lcdData_t;

/**
 * Estructura con los datos para usar el encoder.
 */
typedef struct
{
    volatile int32_t position;  ///< Posición actual del encoder.
    volatile int32_t lastCount; ///< Posición anterior del encoder.
    volatile float reading;     ///< Almacena el valor de la posición del encoder dividido por 1000.
    volatile uint32_t factor;   ///< Factor de multiplicación según la posición del cursor.
    uint32_t max;               ///< Establece el máximo valor permitido para el encoder.
} encoder_t;

/**
 * Estructura con los datos para pedir una entrada por teclado y mostrarlo en pantalla.
 */
typedef struct
{
    QueueHandle_t ikeypadQueue; ///< Manejador de la cola que contiene las teclas pulsadas.
    uint8_t col;               ///< Columna en la que se empezará a escribir.
    uint8_t row;               ///< Fila en la que se empezará a escribir.
    TaskHandle_t taskToNotify; ///< Tarea a la cual notificar.
    uint8_t id;                ///< Identificador para determinar si se cambia de tarea, o se ejecuta desde otra parte del código.
} inputValue_t;

/**
 * Modos de funcionamiento de la carga electrónica.
 */
typedef enum
{
    DCLOAD_MODE_CC = 0,    ///< Modo de corriente contínua.
    DCLOAD_MODE_CP,        ///< Modo de potencia contínua.
    DCLOAD_MODE_CR,        ///< Modo de resistencia contínua.
    DCLOAD_MODE_BC,        ///< Modo de capacidad de batería.
    DCLOAD_MODE_TC,        ///< Modo de transitorio contínuo.
    DCLOAD_MODE_TP,        ///< Modo de transitorio con pulso.
    DCLOAD_MODE_TT,        ///< Modo de transitorio de conmutación.
    DCLOAD_MODE_NO_CHANGE, ///< No se desea cambiar de modo. Se mantiene al anterior.
    DCLOAD_MODE_SETUP,     ///< Modo de configuración.
    DCLOAD_MODE_LOGSD      ///< Modo de configuración para el paso de datos a tarjeta SD.
} dcLoad_Mode_t;

extern QueueHandle_t lcd_queue;
extern encoder_t encoder;
extern QueueHandle_t input_queue;
extern volatile bool blockNumKeys;

// Prototipos
void lcdControl(void *pvParameter);

void lcdmenu_backLight(bool enable);

void lcdmenu_writeString(const char *str);

void lcdmenu_writeChar(const char c);

void lcdmenu_moveCursor(const uint8_t col, const uint8_t row);

void lcdmenu_clear();

void lcdmenu_cursor(bool enable);

void lcdmenu_blink(bool enable);

void lcdmenu_cleanLine(uint8_t line);

void lcdmenu_load(bool on);

void lcdmenu_polarityMessage();

uint32_t lcdmenu_requestInput(uint8_t col, uint8_t row, bool wait, uint8_t id);

char lcdmenu_requestSelection(bool wait);

dcLoad_Mode_t lcdmenu_modeSelectionMenu();

void lcdmenu_modeMenu(dcLoad_Mode_t mode);

void lcdmenu_modeMenu_update(const float current, const float voltage,
                             const float power, bool force);

void lcdmenu_temperature_update(const float temp, const float temp2);

void lcdmenu_batteryTypeMenu();

float lcdmenu_cellsCountMenu(const float batteryCutoffVolt);

float lcdmenu_batteryCutOffMenu();

float lcdmenu_batteryDischargeCutOffMenu();

void lcdmenu_batterySelectedInfoMenu(const char *const batteryType,
                                     const char *const batteryCutOffVolts);

void lcdmenu_configDateTime();

void lcdmenu_userSetUpMenu(float *const currentCutOff,
                           float *const powerCutOff,
                           float *const voltageCutOff,
                           float *const maxBatteryCurrent,
                           volatile bool *const soundEnable,
                           volatile bool *const polarityEnable,
                           volatile uint16_t *const rTime,
                           volatile uint16_t *const fTime);

void lcdmenu_transientMenu_update(dcLoad_Mode_t mode, const float lowCurrent,
                                  const float highCurrent, const uint32_t transientPeriod);

void lcdmenu_transientModeMenu(dcLoad_Mode_t mode,
                               float *const lowCurrent,
                               float *const highCurrent,
                               uint32_t *const transientPeriod,
                               const float currentCutOff);

void lcdmenu_batteryIntResistMenu();

void lcdmenu_intro();

void lcdmenu_shortcuts();

void lcdmenu_inputValueTask(void *pvParameter);

uint16_t lcdmenu_cursorPosition(dcLoad_Mode_t mode);

void lcdmenu_displayEncReading(dcLoad_Mode_t mode);

void lcdmenu_maxSettings(dcLoad_Mode_t mode, float maxCurrent,
                         float maxVoltage, float maxPower,
                         float maxBatteryCurrent, int16_t maxResistor);

void lcdmenu_warning_message(warning_t message);

void lcdmenu_showDateTime();

void lcdmenu_configSDLogging(uint32_t *const loggingTime);

void lcdmenu_configDateTime();

#endif /* LCD_LCDMENU_H_ */
