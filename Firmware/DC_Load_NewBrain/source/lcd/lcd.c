/*
 *
 * File: lcd.c
 * Project: DC Load NewBrain
 * Created Date: 3/5/2019, 12:58
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

// Basada en la librería para ESP32 de David Antliff (MIT).

/**
 * @file
 *
 *
 * The LCD controller is an HD44780-compatible controller that normally operates
 * via an 8-bit or 4-bit wide parallel bus.
 *
 * www.sparkfun.com/datasheets/LCD/HD44780.pdf
 *
 * The LCD controller is connected to a PCF8574A I/O expander via the I2C bus.
 * Only the top four bits are connected to the controller's data lines. The lower
 * four bits are used as control lines:
 *
 *   - B7: data bit 3
 *   - B6: data bit 2
 *   - B5: data bit 1
 *   - B4: data bit 0
 *   - B3: backlight (BL): off = 0, on = 1
 *   - B2: enable (EN): change from 1 to 0 to clock data into controller
 *   - B1: read/write (RW): write = 0, read = 1
 *   - B0: register select (RS): command = 0, data = 1
 *
 * Therefore to send a command byte requires the following operations:
 *
 *    First nibble:
 *   val = command & 0xf0               extract top nibble
 *   val |= 0x04                        RS = 0 (command), RW = 0 (write), EN = 1
 *   i2c_write_byte(i2c_address, val)
 *   sleep(2ms)
 *   val &= 0xfb                        EN = 0
 *   i2c_write_byte(i2c_address, val)
 *
 *    Second nibble:
 *   val = command & 0x0f               extract bottom nibble
 *   val |= 0x04                        RS = 0 (command), RW = 0 (write), EN = 1
 *   i2c_write_byte(i2c_address, val)
 *   sleep(2ms)
 *   val &= 0xfb                        EN = 0
 *   i2c_write_byte(i2c_address, val)
 *
 * Sending a data byte is very similar except that RS = 1 (data)
 *
 * When the controller powers up, it defaults to:
 *
 *   - display cleared
 *   - 8-bit interface, 1 line display, 5x8 dots per character
 *   - increment by 1 set
 *   - no shift
 *
 * The controller must be set to 4-bit operation before proper communication can commence.
 * The initialisation sequence for 4-bit operation is:
 *
 *   0. wait > 15ms after Vcc rises to 4.5V, or > 40ms after Vcc rises to 2.7V
 *   1. send nibble 0x03      select 8-bit interface
 *   2. wait > 4.1ms
 *   3. send nibble 0x03      select 8-bit interface again
 *   4. wait > 100us
 *   5. send command 0x32     select 4-bit interface
 *   6. send command 0x28     set 2 lines and 5x7(8?) dots per character
 *   7. send command 0x0c     display on, cursor off
 *   8. send command 0x06     move cursor right when writing, no scroll
 *   9. send command 0x80     set cursor to home position (row 1, column 1)
 */

#include <stddef.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "lcd.h"
#include "sysview_enable_conf.h"
#include "main.h"

// Delays (microseconds)
#define DELAY_POWER_ON            50000  // wait at least 40us after VCC rises to 2.7V
#define DELAY_INIT_1               4500  // wait at least 4.1ms (fig 24, page 46)
#define DELAY_INIT_2               4500  // wait at least 4.1ms (fig 24, page 46)
#define DELAY_INIT_3                120  // wait at least 100us (fig 24, page 46)

#define DELAY_CLEAR_DISPLAY        2000
#define DELAY_RETURN_HOME          2000

#define DELAY_ENABLE_PULSE_WIDTH      1  // enable pulse must be at least 450ns wide
#define DELAY_ENABLE_PULSE_SETTLE    50  // command requires > 37us to settle (table 6 in datasheet)


// Commands
#define COMMAND_CLEAR_DISPLAY       0x01
#define COMMAND_RETURN_HOME         0x02
#define COMMAND_ENTRY_MODE_SET      0x04
#define COMMAND_DISPLAY_CONTROL     0x08
#define COMMAND_SHIFT               0x10
#define COMMAND_FUNCTION_SET        0x20
#define COMMAND_SET_CGRAM_ADDR      0x40
#define COMMAND_SET_DDRAM_ADDR      0x80

// COMMAND_ENTRY_MODE_SET flags
#define FLAG_ENTRY_MODE_SET_INCREMENT       0x02
#define FLAG_ENTRY_MODE_SET_DECREMENT       0x00
#define FLAG_ENTRY_MODE_SET_SHIFT_ON        0x01
#define FLAG_ENTRY_MODE_SET_SHIFT_OFF       0x00

// COMMAND_DISPLAY_CONTROL flags
#define FLAG_DISPLAY_CONTROL_ON          0x04
#define FLAG_DISPLAY_CONTROL_OFF         0x00
#define FLAG_DISPLAY_CONTROL_CURSOR_ON   0x02
#define FLAG_DISPLAY_CONTROL_CURSOR_OFF  0x00
#define FLAG_DISPLAY_CONTROL_BLINK_ON    0x01
#define FLAG_DISPLAY_CONTROL_BLINK_OFF   0x00

// COMMAND_SHIFT flags
#define FLAG_SHIFT_MOVE_DISPLAY          0x08
#define FLAG_SHIFT_MOVE_CURSOR           0x00
#define FLAG_SHIFT_MOVE_LEFT             0x04
#define FLAG_SHIFT_MOVE_RIGHT            0x00

// COMMAND_FUNCTION_SET flags
#define FLAG_FUNCTION_SET_MODE_8BIT      0x10
#define FLAG_FUNCTION_SET_MODE_4BIT      0x00
#define FLAG_FUNCTION_SET_LINES_2        0x08
#define FLAG_FUNCTION_SET_LINES_1        0x00
#define FLAG_FUNCTION_SET_DOTS_5X10      0x04
#define FLAG_FUNCTION_SET_DOTS_5X8       0x00

// Control flags
#define FLAG_BACKLIGHT_ON    0b00001000      // backlight enabled (disabled if clear)
#define FLAG_BACKLIGHT_OFF   0b00000000      // backlight disabled
#define FLAG_ENABLE          0b00000100
#define FLAG_READ            0b00000010      // read (write if clear)
#define FLAG_WRITE           0b00000000      // write
#define FLAG_RS_DATA         0b00000001      // data (command if clear)
#define FLAG_RS_COMMAND      0b00000000      // command

// Estructura para el envío de datos por i2c.
static i2c_master_transfer_t i2c_txData = {
            .flags = kI2C_TransferDefaultFlag,
            .slaveAddress = 0,
            .direction = kI2C_Write,
            .subaddress = 0,
            .subaddressSize = 0,
            .data = NULL,
            .dataSize = 0
};

static bool _is_init(const i2c_lcd_t * i2c_lcd)
{
    bool ok = false;
    if (i2c_lcd != NULL)
    {
        if (i2c_lcd->init)
        {
            ok = true;
        }
        else
        {
            svError("i2c_lcd is not initialised");
        }
    }
    else
    {
        svError("i2c_lcd is NULL");
    }
    return ok;
}

 // Set or clear the specified flag depending on condition
static uint8_t _set_or_clear(uint8_t flags, bool condition, uint8_t flag)
 {
     uint8_t tflags = flags;
     if (condition)
     {
         tflags |= flag;
     }
     else
     {
         tflags &= ~flag;
     }
     return tflags;
 }

// send data to the I/O Expander
static err_t _write_to_expander(const i2c_lcd_t * i2c_lcd, uint8_t data)
{
    // backlight flag must be included with every write to maintain backlight state

    uint8_t txdata = data | i2c_lcd->backlight_flag;
    i2c_txData.slaveAddress = i2c_lcd->address;
    i2c_txData.data = &txdata;
    i2c_txData.dataSize = sizeof(txdata);
    return I2C_RTOS_Transfer(i2c_lcd->i2c, &i2c_txData);
}

// IMPORTANT - for the display to stay "in sync" it is important that errors do not interrupt the
// 2 x nibble sequence.

// clock data from expander to LCD by causing a falling edge on Enable
static err_t _strobe_enable(const i2c_lcd_t * i2c_lcd, uint8_t data)
{
    err_t err1 = _write_to_expander(i2c_lcd, data | FLAG_ENABLE);
    delay_us(DELAY_ENABLE_PULSE_WIDTH);
    err_t err2 = _write_to_expander(i2c_lcd, data & ~FLAG_ENABLE);
    delay_us(DELAY_ENABLE_PULSE_SETTLE);
    return err1 ? err1 : err2;
}

// send top nibble to the LCD controller
static err_t _write_nibble(const i2c_lcd_t * i2c_lcd, uint8_t data)
{
    err_t err1 = _write_to_expander(i2c_lcd, data);
    err_t err2 = _strobe_enable(i2c_lcd, data);
    return err1 ? err1 : err2;
}

// send command or data to controller
static err_t _write(const i2c_lcd_t * i2c_lcd, uint8_t value, uint8_t register_select_flag)
{
    err_t err1 = _write_nibble(i2c_lcd, (value & 0xf0U) | register_select_flag);
    err_t err2 = _write_nibble(i2c_lcd, ((value & 0x0fU) << 4) | register_select_flag);
    return err1 ? err1 : err2;
}

// send command to controller
static err_t _write_command(const i2c_lcd_t * i2c_lcd, uint8_t command)
{
    return _write(i2c_lcd, command, FLAG_RS_COMMAND);
}

// send data to controller
static err_t _write_data(const i2c_lcd_t * i2c_lcd, uint8_t data)
{
    return _write(i2c_lcd, data, FLAG_RS_DATA);
}


// Public API

/**
 * Inicializa una estructura especificando las características de la LCD e i2c a utilizar.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] i2c Puntero al manejador i2c en FreeRTOS.
 * @param[in] address Dirección I2C de la pantalla.
 * @param[in] backlight Estado inicial de la luz de fondo.
 * @param[in] num_rows Número máximo de filas del dispositivo. Típicamente valores como 2 (1602) o 4 (2004).
 * @param[in] num_columns Máximo número de columnas soportadas. Típicamente valores como 40 (1602, 2004).
 * @param[in] num_visible_columns Número de columnas visibles. Típicamente valores como 16 (1602) o 20 (2004).
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_init(i2c_lcd_t * i2c_lcd, i2c_rtos_handle_t * i2c, uint8_t address,
                   bool backlight, uint8_t num_rows, uint8_t num_columns,
                   uint8_t num_visible_columns)
{
    err_t err = FAIL;
    if (i2c_lcd != NULL)
    {
        i2c_lcd->i2c = i2c;
        i2c_lcd->address = address;
        i2c_lcd->backlight_flag = backlight ? FLAG_BACKLIGHT_ON : FLAG_BACKLIGHT_OFF;
        i2c_lcd->num_rows = num_rows;
        i2c_lcd->num_columns = num_columns;
        i2c_lcd->num_visible_columns = num_visible_columns;

        // display on, no cursor, no blinking
        i2c_lcd->display_control_flags = FLAG_DISPLAY_CONTROL_ON | FLAG_DISPLAY_CONTROL_CURSOR_OFF | FLAG_DISPLAY_CONTROL_BLINK_OFF;

        // left-justified left-to-right text
        i2c_lcd->entry_mode_flags = FLAG_ENTRY_MODE_SET_INCREMENT | FLAG_ENTRY_MODE_SET_SHIFT_OFF;

        i2c_lcd->init = true;

        // See page 45/46 of HD44780 data sheet for the initialisation procedure.

        // Wait at least 40ms after power rises above 2.7V before sending commands.
        delay_us(DELAY_POWER_ON);

        err = i2c_lcd_reset(i2c_lcd);
    }
    else
    {
        svError("i2c_lcd is NULL");
        err = FAIL;
    }
    return err;
}

/**
 * Resetea la plantalla. Los caracteres personalizados serán borrados.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_reset(const i2c_lcd_t * i2c_lcd)
{
    err_t first_err = OK;
    err_t last_err = FAIL;

    // put Expander into known state - Register Select and Read/Write both low
    last_err = _write_to_expander(i2c_lcd, 0);
    if ( last_err != OK){
        first_err = last_err;
        svError("reset: _write_to_expander failed");
    }

    delay_us(1000);

    // select 4-bit mode on LCD controller - see datasheet page 46, figure 24.
    last_err = _write_nibble(i2c_lcd, 0x03U << 4);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_nibble 1 failed.");
    }

    delay_us(DELAY_INIT_1);

    // repeat
    last_err = _write_nibble(i2c_lcd, 0x03U << 4);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_nibble 2 failed.");
    }

    delay_us(DELAY_INIT_2);

    // repeat
    last_err = _write_nibble(i2c_lcd, 0x03U << 4);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_nibble 3 failed.");
    }

    delay_us(DELAY_INIT_3);

    // select 4-bit mode
    last_err = _write_nibble(i2c_lcd, 0x02U << 4);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_nibble 4 failed.");
    }

    // now we can use the command()/write() functions
    last_err = _write_command(i2c_lcd, COMMAND_FUNCTION_SET |
                              FLAG_FUNCTION_SET_MODE_4BIT |
                              FLAG_FUNCTION_SET_LINES_2 |
                              FLAG_FUNCTION_SET_DOTS_5X8);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_command 1 failed.");
    }

    last_err = _write_command(i2c_lcd, COMMAND_DISPLAY_CONTROL |
                              i2c_lcd->display_control_flags);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_command 2 failed.");
    }

    last_err = i2c_lcd_clear(i2c_lcd);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: i2c_lcd_clear failed.");
    }

    last_err = _write_command(i2c_lcd, COMMAND_ENTRY_MODE_SET |
                              i2c_lcd->entry_mode_flags);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: _write_command 3 failed.");
    }

    last_err = i2c_lcd_home(i2c_lcd);
    if (last_err != OK)
    {
        if (first_err == OK){
            first_err = last_err;
        }
        svError("reset: i2c_lcd_home failed.");
    }

    return first_err;
}

/**
 * Límpia la pantalla por completo (límpia la memoria DDRAM) y devuelve el cursor a la posición inicial.
 * El contenido de la memoria CGRAM no cambia.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_clear(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        err = _write_command(i2c_lcd, COMMAND_CLEAR_DISPLAY);
        if (err == OK)
        {
            delay_us(DELAY_CLEAR_DISPLAY);
        }
    }
    return err;
}

/**
 * Mueve el cursor a la posición inicial. También resetea cualquier desplazamiento que esté ocurriendo en la pantalla.
 * El contenido de la DDRAM y la CGRAM no cambia.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_home(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        err = _write_command(i2c_lcd, COMMAND_RETURN_HOME);
        if (err == OK)
        {
            delay_us(DELAY_RETURN_HOME);
        }
    }
    return err;
}

/**
 * Mueve el cursor a la columna y fila especificada. El nuevo carácter aparecerá en esta posición.
 *
 * @param[in] i2c_lcd Puntero a estructura LCD.
 * @param[in] col Indice horizontal empezando en 0. La columna 0 es la primera a la izquierda.
 * @param[in] row Indece vertical empezando en 0. La fila 0 es la de más arriba.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_move_cursor(const i2c_lcd_t * i2c_lcd, uint8_t col, uint8_t row)
{
    err_t err = FAIL;
    uint8_t tcol = col;
    uint8_t trow = row;

    if (_is_init(i2c_lcd))
    {
        const int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
        if (row > i2c_lcd->num_rows)
        {
            trow = i2c_lcd->num_rows - 1;
        }
        if (col > i2c_lcd->num_columns)
        {
            tcol = i2c_lcd->num_columns - 1;
        }
        err = _write_command(i2c_lcd, COMMAND_SET_DDRAM_ADDR | (tcol + row_offsets[trow]));
    }
    return err;
}

/**
 * Habilita o deshabilita la luz de fondo.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] enable True para habilitar, false para deshabilitar.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_backlight(i2c_lcd_t * i2c_lcd, bool enable)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->backlight_flag = _set_or_clear(i2c_lcd->backlight_flag, enable, FLAG_BACKLIGHT_ON);
        err = _write_to_expander(i2c_lcd, 0);
    }
    return err;
}

/**
 * Activa o desactiva la pantalla. Cuando está desactivada, la luz de fondo no se ve afectada, pero
 * cualquier contenido de la DDRAM no será mostrado, así como el cursor. La pantalla está en "blanco".
 * Re-activar la pantalla no afecta el contenido de la DDRAM o el estado o posición del cursor.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] enable True para activar, false para desactivar.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_display(i2c_lcd_t * i2c_lcd, bool enable)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->display_control_flags = _set_or_clear(i2c_lcd->display_control_flags, enable, FLAG_DISPLAY_CONTROL_ON);
        err = _write_command(i2c_lcd, COMMAND_DISPLAY_CONTROL | i2c_lcd->display_control_flags);
    }
    return err;
}

/**
 * Activa o desactiva la visualización de la línea bajo el cursor.
 * Si se activa, se indica visualmente donde se escribirá el próximo carácter.
 * Puede ser activado junto con el cursor parpadeando, sin embargo el resultado visual no es muy elegante.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] enable True para activar, false para desactivar.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_cursor(i2c_lcd_t * i2c_lcd, bool enable)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->display_control_flags = _set_or_clear(i2c_lcd->display_control_flags, enable, FLAG_DISPLAY_CONTROL_CURSOR_ON);
        err = _write_command(i2c_lcd, COMMAND_DISPLAY_CONTROL | i2c_lcd->display_control_flags);
    }
    return err;
}

/**
* Activa o desactiva el parpadeo del cursor.
* Si se activa, se indica visualmente donde se escribirá el próximo carácter.
* Puede ser activado junto con el cursor parpadeando, sin embargo el resultado visual no es muy elegante.
*
* @param[in] i2c_lcd Puntero a estructura de LCD.
* @param[in] enable True para activar, false para desactivar.
* @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
*/
err_t i2c_lcd_set_blink(i2c_lcd_t * i2c_lcd, bool enable)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->display_control_flags = _set_or_clear(i2c_lcd->display_control_flags, enable, FLAG_DISPLAY_CONTROL_BLINK_ON);
        err = _write_command(i2c_lcd, COMMAND_DISPLAY_CONTROL | i2c_lcd->display_control_flags);
    }
    return err;
}

/**
 * Fija la dirección del movimiento del cursor hacia la derecha.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_left_to_right(i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->entry_mode_flags |= FLAG_ENTRY_MODE_SET_INCREMENT;
        err = _write_command(i2c_lcd, COMMAND_ENTRY_MODE_SET | i2c_lcd->entry_mode_flags);
    }
    return err;
}

/**
 * Fija la dirección del movimiento del cursor hacia la izquierda.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_right_to_left(i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->entry_mode_flags &= ~FLAG_ENTRY_MODE_SET_INCREMENT;
        err = _write_command(i2c_lcd, COMMAND_ENTRY_MODE_SET | i2c_lcd->entry_mode_flags);
    }
    return err;
}

/**
 * Activa o desactiva el desplazamiento automático de la pantalla.
 *
 * Cuando está activado, la pantalla se desplaza conforme se escriben los caracteres para que el cursor aparezca en pantalla.
 * El texto escrito de izquierda a derecha parecerá justificado a derechas desde la posición del cursor.
 * Cuando está desactivado la pantalla no se desplaza y el cursor se mueve en los caracteres que se ven en pantalla.
 * El texto escrito de izquierda a derecha parecerá justificado a izquierdas desde la posición del cursor.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] enable True para activar, false para desactivar.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_set_auto_scroll(i2c_lcd_t * i2c_lcd, bool enable)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        i2c_lcd->entry_mode_flags = _set_or_clear(i2c_lcd->entry_mode_flags, enable, FLAG_ENTRY_MODE_SET_SHIFT_ON);
        err = _write_command(i2c_lcd, COMMAND_ENTRY_MODE_SET | i2c_lcd->entry_mode_flags);
    }
    return err;
}

/**
 * Se desplaza una posición hacia la izquierda. En pantalla el texto parecerá moverse hacia la derecha.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_scroll_display_left(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        // RAM is not changed
        err = _write_command(i2c_lcd, COMMAND_SHIFT | FLAG_SHIFT_MOVE_DISPLAY | FLAG_SHIFT_MOVE_LEFT);
    }
    return err;
}

/**
 * Se desplaza una posición hacia la derecha. En pantalla el texto parecerá moverse hacia la izquierda.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_scroll_display_right(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        // RAM is not changed
        err = _write_command(i2c_lcd, COMMAND_SHIFT | FLAG_SHIFT_MOVE_DISPLAY | FLAG_SHIFT_MOVE_RIGHT);
    }
    return err;
}

/**
 * Mueve el cursor una posición hacia la izquierda, aunque sea invisible.
 * Esto afecta a la posición en la que aparecerá el próximo carácter en pantalla.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_move_cursor_left(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        // RAM is not changed. Shift direction is inverted.
        err = _write_command(i2c_lcd, COMMAND_SHIFT | FLAG_SHIFT_MOVE_CURSOR | FLAG_SHIFT_MOVE_RIGHT);
    }
    return err;
}

/**
 * Mueve el cursor una posición hacia la derecha, aunque sea invisible.
 * Esto afecta a la posición en la que aparecerá el próximo carácter en pantalla.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_move_cursor_right(const i2c_lcd_t * i2c_lcd)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        // RAM is not changed. Shift direction is inverted.
        err = _write_command(i2c_lcd, COMMAND_SHIFT | FLAG_SHIFT_MOVE_CURSOR | FLAG_SHIFT_MOVE_LEFT);
    }
    return err;
}

/**
 * Define una carácter personalizado a partir de un array de pixeles.
 *
 * Hay hasta ocho posibles caracteres personalizados, y se usa un indice basado en 0
 * para seleccionar el carácter definido. Cuanquier carácter definido anteriormente en el indice se perderá.
 * Los caracteres tienen 5 pixeles de ancho y 8 pixeles de alto.
 * El array de mapeado de los pixeles consiste en hasta 8 bytes, cada byte representa el estado de los pixeles por fila.
 * El primer byte representa la fila de arriba. El octavo byte es dejado habitualmente a 0 (para dejar espacio a la línea de cursor).
 * Por cada fila, los 5 bits más bajos representan los pixeles que se quieren iluminar. El bit menos significativo
 * representa el pixel más a la derecha. Una fila vacía es un 0.
 *
 * @note: Despueś de llamar a esta función, la memoria DDRAM no será seleccionada y la posición del cursor será desconocida. Por lo tanto
 * es importante que la dirección DDRAM sea fijada después de llamar a esta función si se va ha escribir texto en la pantalla.
 * Esto se puede conseguir llamando a i2c_lcd_home() o i2c_lcd_move_cursor().
 *
 * Los caracteres personalizados son escritos usando las definiciones I2C_LCD1602_CHARACTER_CUSTOM_X.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @param[in] index Indice basado en 0 del carácter a definir. Sólo valores entre 0-7 son válidos.
 * @param[in] pixelmap Array de 8 bytes que define el mapa de pixeles para la definición del nuevo carácter.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_define_char(const i2c_lcd_t * i2c_lcd, i2c_lcd_custom_index_t index, const uint8_t pixelmap[])
{
    err_t err = FAIL;
    i2c_lcd_custom_index_t tindex = index;

    if (_is_init(i2c_lcd))
    {
        tindex &= 0x07;  // only the first 8 indexes can be used for custom characters
        err = _write_command(i2c_lcd, COMMAND_SET_CGRAM_ADDR | (tindex << 3));
        for (int i = 0; (err == OK) && (i < 8); ++i)
        {
            err = _write_data(i2c_lcd, pixelmap[i]);
        }
    }
    return err;
}

/**
 * Escribe un solo carácter en la pantalla en la posición actual del cursor.
 *
 * Dependiendo del modo activo, el cursor puede moverse a la izquierda o derecha, o puede desplazarse a izquierda o derecha.
 * Los caracteres personalizados pueden escribirse usando las definiciones I2C_LCD_CHARACTER_CUSTOM_X.
 *
 * La pantalla tiene I2C_LCD_NUM_COLUMNS de ancha, y al encontrarse con el final de la primera línea, el cursor
 * automáticamente se moverá al principio de la segunda línea.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_write_char(const i2c_lcd_t * i2c_lcd, uint8_t chr)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        err = _write_data(i2c_lcd, chr);
    }
    return err;
}

/**
 * Escribe una cadena de caracteres en la pantalla a partir de la posición actual del cursor.
 *
 * Dependiendo del modo activo, el cursor puede moverse a la izquierda o derecha, o puede desplazarse a izquierda o derecha
 * después de la escritura de cada carácter.
 *
 * La pantalla tiene I2C_LCD_NUM_COLUMNS de ancha, y al encontrarse con el final de la primera línea, el cursor
 * automáticamente se moverá al principio de la segunda línea.
 *
 * @param[in] i2c_lcd Puntero a estructura de LCD.
 * @return 0 si se ha creado correctamente, otro número distinto si ha ocurrido un error.
 */
err_t i2c_lcd_write_string(const i2c_lcd_t * i2c_lcd, const char * string)
{
    err_t err = FAIL;
    if (_is_init(i2c_lcd))
    {
        err = OK;
        for (int i = 0; (err == OK) && string[i]; ++i)
        {
            err = _write_data(i2c_lcd, string[i]);
        }
    }
    return err;
}

