/*
 *
 * File: lcd.h
 * Project: DC Load NewBrain
 * Created Date: 3/5/2019, 12:56
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

// Basada en la librería para ESP32 de David Antliff (MIT License).

#ifndef LCD_LCD_H_
#define LCD_LCD_H_

#include <stdbool.h>
#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"

/**
 * Estructura con información del módulo LCD.
 */
typedef struct
{
    bool init;                     ///< True si se ha inicializado la estructura.
    i2c_rtos_handle_t * i2c;       ///< Para manejo de i2c en FreeRTOS.
    uint8_t backlight_flag;        ///< Valor mayor que 0 para encender la luz de fondo, 0 para apagarla.
    uint8_t num_rows;              ///< Número de filas.
    uint8_t num_columns;           ///< Número de columnas, incluidas las no visibles.
    uint8_t num_visible_columns;   ///< Número de columnas visibles.
    uint8_t display_control_flags; ///< Banderas de control del panel LCD.
    uint8_t entry_mode_flags;      ///< Banderas del modo de entrada.
    uint8_t address;               ///< Dirección de la pantalla LCD por I2C.
} i2c_lcd_t;


// Símbolos definidos por el usuario

#define I2C_LCD_CHARACTER_CUSTOM_0     0b00001000
#define I2C_LCD_CHARACTER_CUSTOM_1     0b00001001
#define I2C_LCD_CHARACTER_CUSTOM_2     0b00001010
#define I2C_LCD_CHARACTER_CUSTOM_3     0b00001011
#define I2C_LCD_CHARACTER_CUSTOM_4     0b00001100
#define I2C_LCD_CHARACTER_CUSTOM_5     0b00001101
#define I2C_LCD_CHARACTER_CUSTOM_6     0b00001110
#define I2C_LCD_CHARACTER_CUSTOM_7     0b00001111

#define I2C_LCD_CHARACTER_ALPHA        0b11100000   ///< Símbolo alfha en minúscula.
#define I2C_LCD_CHARACTER_BETA         0b11100010   ///< Símbolo beta en minúscula.
#define I2C_LCD_CHARACTER_THETA        0b11110010   ///< Símbolo theta en minúscula.
#define I2C_LCD_CHARACTER_PI           0b11110111   ///< Símbolo pi en minúscula.
#define I2C_LCD_CHARACTER_OMEGA        0b11110100   ///< Símbolo griego omega en mayúscula.
#define I2C_LCD_CHARACTER_SIGMA        0b11110110   ///< Símbolo griego sigma en mayúscula.
#define I2C_LCD_CHARACTER_INFINITY     0b11110011   ///< Símbolo de infinito.
#define I2C_LCD_CHARACTER_DEGREE       0b11011111   ///< Símbolo de grados.
#define I2C_LCD_CHARACTER_ARROW_RIGHT  0b01111110   ///< Símbolo de flecha derecha.
#define I2C_LCD_CHARACTER_ARROW_LEFT   0b01111111   ///< Símbolo de flecha izquierda.
#define I2C_LCD_CHARACTER_SQUARE       0b11011011   ///< Símbolo de cuadrado.
#define I2C_LCD_CHARACTER_DOT          0b10100101   ///< Símbolo de punto.
#define I2C_LCD_CHARACTER_DIVIDE       0b11111101   ///< Simbolo de división.
#define I2C_LCD_CHARACTER_BLOCK        0b11111111   ///< Bloque de 5x8 relleno.

/**
 * Enumeración con índices válidos para los caracteres definidos por el usuario.
 */
typedef enum
{
    I2C_LCD_INDEX_CUSTOM_0 = 0,
    I2C_LCD_INDEX_CUSTOM_1,
    I2C_LCD_INDEX_CUSTOM_2,
    I2C_LCD_INDEX_CUSTOM_3,
    I2C_LCD_INDEX_CUSTOM_4,
    I2C_LCD_INDEX_CUSTOM_5,
    I2C_LCD_INDEX_CUSTOM_6,
    I2C_LCD_INDEX_CUSTOM_7,
} i2c_lcd_custom_index_t;

typedef int32_t err_t; ///< Código de error devuelto por las funciones.

#define OK  0
#define FAIL -1


err_t i2c_lcd_init(i2c_lcd_t * i2c_lcd, i2c_rtos_handle_t * i2c, uint8_t address,
                   bool backlight, uint8_t num_rows, uint8_t num_columns,
                   uint8_t num_visible_columns);

err_t i2c_lcd_reset(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_clear(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_home(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_move_cursor(const i2c_lcd_t * i2c_lcd, uint8_t col, uint8_t row);

err_t i2c_lcd_set_backlight(i2c_lcd_t * i2c_lcd, bool enable);

err_t i2c_lcd_set_display(i2c_lcd_t * i2c_lcd, bool enable);

err_t i2c_lcd_set_cursor(i2c_lcd_t * i2c_lcd, bool enable);

err_t i2c_lcd_set_blink(i2c_lcd_t * i2c_lcd, bool enable);

err_t i2c_lcd_set_left_to_right(i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_set_right_to_left(i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_set_auto_scroll(i2c_lcd_t * i2c_lcd, bool enable);

err_t i2c_lcd_scroll_display_left(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_scroll_display_right(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_move_cursor_left(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_move_cursor_right(const i2c_lcd_t * i2c_lcd);

err_t i2c_lcd_define_char(const i2c_lcd_t * i2c_lcd, i2c_lcd_custom_index_t index, const uint8_t pixelmap[]);

err_t i2c_lcd_write_char(const i2c_lcd_t * i2c_lcd, uint8_t chr);

err_t i2c_lcd_write_string(const i2c_lcd_t * i2c_lcd, const char * string);

#endif /* LCD_LCD_H_ */
