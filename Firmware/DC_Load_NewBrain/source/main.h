/*
 *
 * File: main.h
 * Project: DC Load NewBrain
 * Created Date: 18/5/2019, 18:57
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef MAIN_H_
#define MAIN_H_

#include "mcp3426/mcp3426.h"
#include "mcp4725/mcp4725.h"
#include "lcd/lcdmenu.h"

#define MENU_SELECT_VALUE           1010000
#define MENU_SHOTCUTS_VALUE         1100000

extern mcp3426_t adc;
extern mcp4725_config_t dac;
extern uint32_t SDLoggingTime;

extern volatile bool sound;
extern volatile float actualVoltage;
extern volatile bool lockEncoder;
extern volatile bool transientFastMode;
extern volatile dcLoad_Mode_t operationMode;
extern volatile bool showTemp;
extern volatile bool showingWarnMessage;
extern volatile bool loadOutput;
extern volatile uint32_t Seconds;
extern volatile bool slewEnable;
extern volatile bool isAlive;

void delay_us(uint16_t us);
void buzzer_ms(uint16_t ms);
bool checkPolarity();
void resetEncoder();
float readVoltage(bool offset, bool wait);
void setDacWSlewRate(uint16_t dacValue, bool isRiseTime);

#endif /* MAIN_H_ */
