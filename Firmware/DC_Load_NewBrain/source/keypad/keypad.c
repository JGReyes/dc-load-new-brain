/*
 *
 * File: keypad.c
 * Project: DC Load NewBrain
 * Created Date: 13/7/2019, 11:54
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include "keypad.h"
#include "peripherals.h"
#include "pin_mux.h"
#include <FreeRTOS.h>
#include <task.h>
#include <sysview_enable_conf.h>
#include "keypad/button_debounce.h"

/**
 * Cola por la que se mandan las teclas detectadas.
 */
QueueHandle_t keypadQueue = NULL;

// Pines que forman la matriz del keypad.
const uint8_t cols[4] = {BOARD_INITPINS_KP_8_PIN, BOARD_INITPINS_KP_7_PIN,
                         BOARD_INITPINS_KP_6_PIN, BOARD_INITPINS_KP_5_PIN};
const uint8_t rows[4] = {BOARD_INITPINS_KP_4_PIN, BOARD_INITPINS_KP_3_PIN,
                         BOARD_INITPINS_KP_2_PIN, BOARD_INITPINS_KP_1_PIN};

const char Keys[4][4] = {
    {'1', '2', '3', 'A'},
    {'4', '5', '6', 'B'},
    {'7', '8', '9', 'C'},
    {'*', '0', '#', 'D'}};

/**
 * Se encarga de escanear la matriz del teclado.
 * @return Número que representa la tecla pulsada.
 */
static uint32_t keypadScan(void)
{
    // Se hacen volatiles para prevenir optimizaciones del compilador.

    uint32_t result = 0;
    volatile uint8_t row_selected = 0;
    volatile uint8_t col_selected = 0;
    bool key_pressed = pdFALSE;
    volatile uint8_t col;
    volatile uint8_t row;

    GPIO_PortSet(GPIOE, ((1U << cols[0]) | (1U << cols[1]) | (1U << cols[2]) | (1U << cols[3])));
    for (col = 0; col <= 3; col++)
    {
        if (GPIO_PinRead(GPIOE, rows[col]) == 1)
        { // Si el botón pertenece a la columna.
            col_selected = col;
            key_pressed = pdTRUE;
            GPIO_PortClear(GPIOE, ((1U << cols[0]) | (1U << cols[1]) | (1U << cols[2]) | (1U << cols[3])));
            for (row = 0; row <= 3; row++)
            {
                GPIO_PinWrite(GPIOE, cols[row], 1); // Se activa una columna.
                if (GPIO_PinRead(GPIOE, rows[col_selected]) == 1)
                { // Si el botón pertenece a la fila.
                    row_selected = row;
                    goto exit_loops;
                }
                else
                {
                    GPIO_PinWrite(GPIOE, cols[col], 0); // Si no, se desactiva y se pasa a la siguiente.
                }
            }
        }
    }

exit_loops:;
    static char currentKey = 0;

    if (key_pressed == pdTRUE)
    {
        currentKey = Keys[row_selected][col_selected];
        svPrint("keypadSerialize key:");
        svPrint(&currentKey);
    }
    else{
        currentKey = 'N'; // Ninguna tecla pulsada.
    }

    switch (currentKey)
    {
        case '0':
            result |= KEY00;
            break;
        case '1':
            result |= KEY01;
            break;
        case '2':
            result |= KEY02;
            break;
        case '3':
            result |= KEY03;
            break;
        case '4':
            result |= KEY04;
            break;
        case '5':
            result |= KEY05;
            break;
        case '6':
            result |= KEY06;
            break;
        case '7':
            result |= KEY07;
            break;
        case '8':
            result |= KEY08;
            break;
        case '9':
            result |= KEY09;
            break;
        case 'A':
            result |= KEY10;
            break;
        case 'B':
            result |= KEY11;
            break;
        case 'C':
            result |= KEY12;
            break;
        case 'D':
            result |= KEY13;
            break;
        case '*':
            result |= KEY14;
            break;
        case '#':
            result |= KEY15;
            break;
        default:
            result = 0;
            break;
    }

    return result;
}

/**
 * Función para crear una tarea en FreeRTOS, encargada de leer el teclado
 * y poner la tecla pulsada en la cola keypadQueue.
 *
 * @param[in] param Parámetros de FreeRTOS.
 */
void keypadRead(void *param)
{

    keypadQueue = xQueueCreate(1, sizeof(uint32_t));
    vQueueAddToRegistry(keypadQueue, "keypad_queue");
    uint32_t key = 0; // Tecla a mandar.

    // Guarda el estado de las teclas tras leer los pines o escanear el teclado.
    uint8_t keypad_b1 = 0;
    uint8_t keypad_b2 = 0;

    // Estructuras para las funciones antirrebotes.
    Debouncer dbr_keypad_b1;
    Debouncer dbr_keypad_b2;

    // Inicializa las estructruras.
    ButtonDebounceInit(&dbr_keypad_b1, 0x00);
    ButtonDebounceInit(&dbr_keypad_b2, 0x00);

    for (;;)
    {
        // Lee el teclado.
        uint32_t keys = keypadScan();

        // Guarda el estado de las teclas en el bit correspondiente.
        switch (keys)
        {
            case KEY00:
                keypad_b1 |= (1U << 0);
                break;
            case KEY01:
                keypad_b1 |= (1U << 1);
                break;
            case KEY02:
                keypad_b1 |= (1U << 2);
                break;
            case KEY03:
                keypad_b1 |= (1U << 3);
                break;
            case KEY04:
                keypad_b1 |= (1U << 4);
                break;
            case KEY05:
                keypad_b1 |= (1U << 5);
                break;
            case KEY06:
                keypad_b1 |= (1U << 6);
                break;
            case KEY07:
                keypad_b1 |= (1U << 7);
                break;
            case KEY08:
                keypad_b2 |= (1U << 0);
                break;
            case KEY09:
                keypad_b2 |= (1U << 1);
                break;
            case KEY10:
                keypad_b2 |= (1U << 2);
                break;
            case KEY11:
                keypad_b2 |= (1U << 3);
                break;
            case KEY12:
                keypad_b2 |= (1U << 4);
                break;
            case KEY13:
                keypad_b2 |= (1U << 5);
                break;
            case KEY14:
                keypad_b2 |= (1U << 6);
                break;
            case KEY15:
                keypad_b2 |= (1U << 7);
                break;
            default:
                keypad_b1 = 0;
                keypad_b2 = 0;
                break;
        }

        // Ejecuta la rutina antirrepotes para limpiar la señal.
        ButtonProcess(&dbr_keypad_b1, keypad_b1);
        ButtonProcess(&dbr_keypad_b2, keypad_b2);

        // Manda la tecla correspondiente a la cola.
        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_0) != false){
            key = KEY00;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_1) != false){
            key = KEY01;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_2) != false){
            key = KEY02;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_3) != false){
            key = KEY03;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_4) != false){
            key = KEY04;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_5) != false){
            key = KEY05;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_6) != false){
            key = KEY06;
        }

        if (ButtonReleased(&dbr_keypad_b1, BUTTON_PIN_7) != false){
            key = KEY07;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_0) != false){
            key = KEY08;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_1) != false){
            key = KEY09;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_2) != false){
            key = KEY10;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_3) != false){
            key = KEY11;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_4) != false){
            key = KEY12;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_5) != false){
            key = KEY13;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_6) != false){
            key = KEY14;
        }

        if (ButtonReleased(&dbr_keypad_b2, BUTTON_PIN_7) != false){
            key = KEY15;
        }

        if (key != 0) // Si se ha pulsado una tecla.
        {
            xQueueOverwrite(keypadQueue, &key);
            key = 0;
        }

        vTaskDelay(READ_INTERVAL / portTICK_PERIOD_MS);
    }
}

/**
 * Convierte una tecla en su carácter correspondiente.
 * @param key Número de tecla.
 * @return Carácter de la tecla.
 */
char keyToChar(const uint32_t key)
{
    switch (key)
    {
    case KEY00:
        return '0';
    case KEY01:
        return '1';
    case KEY02:
        return '2';
    case KEY03:
        return '3';
    case KEY04:
        return '4';
    case KEY05:
        return '5';
    case KEY06:
        return '6';
    case KEY07:
        return '7';
    case KEY08:
        return '8';
    case KEY09:
        return '9';
    case KEY10:
        return 'A';
    case KEY11:
        return 'B';
    case KEY12:
        return 'C';
    case KEY13:
        return 'D';
    case KEY14:
        return '*';
    case KEY15:
        return '#';
    default:
        return 'N';
    }
}
