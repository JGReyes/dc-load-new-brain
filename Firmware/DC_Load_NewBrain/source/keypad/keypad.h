/*
 *
 * File: keypad.h
 * Project: DC Load NewBrain
 * Created Date: 13/7/2019, 11:53
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef _H_KEYPAD_H
#define _H_KEYPAD_H

#include <FreeRTOS.h>
#include <queue.h>

#define READ_INTERVAL 5 // Tiempo entre lecturas del keypad.

// Definiciones de los códigos de teclas.
#define KEY00 0x0001
#define KEY01 0x0002
#define KEY02 0x0004
#define KEY03 0x0008
#define KEY04 0x0010
#define KEY05 0x0020
#define KEY06 0x0040
#define KEY07 0x0080
#define KEY08 0x0100
#define KEY09 0x0200
#define KEY10 0x0400
#define KEY11 0x0800
#define KEY12 0x1000
#define KEY13 0x2000
#define KEY14 0x4000
#define KEY15 0x8000


// API
extern QueueHandle_t keypadQueue;

void keypadRead(void *param);
char keyToChar(const uint32_t key);

#endif
