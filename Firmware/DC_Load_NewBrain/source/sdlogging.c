/*
 *
 * File: sdlogging.c
 * Project: DC Load NewBrain
 * Created Date: 6/7/2019, 11:40
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include "sdlogging.h"
#include "peripherals.h"
#include "lcd/lcdmenu.h"
#include "FreeRTOS.h"
#include "timers.h"
#include <sysview_enable_conf.h>
#include "main.h"
#include "pin_mux.h"

volatile bool enableLogging = false; // Indica si está o no habilitado el logging.
volatile bool SDin = false;          // Indica si se ha introducido una tarjeta SD.
volatile bool activeLogging = false; // Indica si actualmente se deben exportar datos.
volatile bool SDblock = false;       // Indica si la tarjeta está bloqueada contra escritura.

static FATFS g_fileSystem; // Representa el sistema de archivos.
static FIL g_fileObject;   // Representa un fichero.
static TimerHandle_t sdTimer; // Marca el tiempo para la escritura en la sd.


typedef struct {
    SemaphoreHandle_t mutex;
    float current;
    float voltage;
    float power;
    uint32_t batteryCapacity;
}logData_t;

//Almacena las variables compartidas con el timer para la SD.s
static logData_t logData;

/**
 * Fija la frecuencia en Hz o Bps del bus SPI.
 *
 * @param[in] frequency Frecuencia del bus SPI.
 * @return kStatus_Success
 */
static status_t setFrequency(uint32_t frequency)
{

    SPI_MasterSetBaudRate(SPI_1_PERIPHERAL, frequency, SPI_1_CLK_FREQ);
    return kStatus_Success;
}

/**
 * Intercambia datos por SPI.
 *
 * Se utiliza para que la libreria de fatfs se comunique con la tarjeta sd.
 *
 * @param[in] in Byte a mandar.
 * @param[out] out Byte recibido.
 * @param[in] size Tamaño en bytes.
 * @return kStatus_Success o el tipo de error.
 */
static status_t exchange(uint8_t *in, uint8_t *out, uint32_t size)
{

    spi_transfer_t transfer;

    transfer.rxData = out;
    transfer.txData = in;
    transfer.dataSize = size;

    return SPI_MasterTransferBlocking(SPI_1_PERIPHERAL, &transfer);
}

/**
 * Estructura con las variable y funciones definidas por el usuario
 * para usar el driver sdspi.
 */
static sdspi_host_t host = {
    .busBaudRate = 10000000U, // 10 Mhz SPI bus.
    .exchange = exchange,
    .setFrequency = setFrequency};

/**
 * Asigna una configuración de host al driver spspi.
 */
static void setHostSPI(sdspi_host_t *host)
{
    g_sdspi.host = host;
}

/**
 * Convierte la fecha y hora del RTC al formato de FatFs.
 *
 * Se usa para el timestamp de los ficheros.
 * @return Fecha y hora en formato fattime.
 */
DWORD get_fattime(void)
{
    rtc_datetime_t datetime;
    RTC_GetDatetime(RTC_1_PERIPHERAL, &datetime);

    //    struct fattime{
    //        uint8_t Year: 7;
    //        uint8_t Month: 4;
    //        uint8_t Day: 5;
    //        uint8_t Hour: 5;
    //        uint8_t Minute: 6;
    //        uint8_t Second: 5;
    //    };
    //
    //    struct fattime ft = {(datetime.year - 1980), datetime.month, datetime.day,
    //                          (datetime.hour + 1), datetime.minute, (datetime.second/2)};
    //
    //    DWORD ret= ft.Year << 25
    //         | ft.Month << 21
    //         | ft.Day << 16
    //         | ft.Hour << 11
    //         | ft.Minute << 5
    //         | ft.Second;

    DWORD ret = (DWORD)((datetime.year - 1980) << 25) | (datetime.month << 21) |
                 (datetime.day << 16) | ((datetime.hour + 1) << 11) |
                 (datetime.minute << 5) | (datetime.second / 2);

    return ret;
}

/**
 * Escribe los datos en la tarjeta SD en el periodo del temporizador.
 * @param[in] timer Timer encargado de ejecutar esta función.
 */
static void sdWrite(TimerHandle_t timer){
    rtc_datetime_t datetime;         // Almacena la fecha y hora del sistema.
    static char dataBuffer[46] = {'\0'};

    // Si hay tarjeta y se debe loggear.
    if (SDin && activeLogging)
    {
        if (xSemaphoreTake(logData.mutex, 0) == pdTRUE){
            RTC_GetDatetime(RTC_1_PERIPHERAL, &datetime);
            sprintf(dataBuffer, "%02d-%02d-%04d,%02d:%02d:%02d,%5.3f,%5.3f,%4.2f,%u\n",
                    datetime.day, datetime.month, datetime.year, datetime.hour,
                    datetime.minute, datetime.second, logData.voltage,
                    logData.current, logData.power, logData.batteryCapacity);
            xSemaphoreGive(logData.mutex);
            f_puts(dataBuffer, &g_fileObject);
            f_sync(&g_fileObject); // Guarda los datos en cache en el archivo.
        }
    }
}

/**
 * Exporta las lectura de corriente, voltaje y potencia a la tarjeta SD.
 *
 * @param[in] current Corriente actual.
 * @param[in] voltage Voltaje actual.
 * @param[in] power   Potencia actual.
 * @param[in] time    Tiempo entre escritura en ms.
 * @return True si no ha sucedido ningún error, de lo contrario False.
 */
bool sd_logging(const float current, const float voltage, const float power,
                const uint32_t batteryCapacity, const uint32_t time)
{
    static bool sdInOldState = false; // Estado anterior de la tarjeta SD.
    static char buffer[24] = {'\0'};
    static uint32_t timeInTicks = 0; // Tiempo entre escritura pasado a ticks de FreeRTOS.
    FRESULT error;          // Almacena la salida de las funciones de FatFS.
    rtc_datetime_t datetime;         // Almacena la fecha y hora del sistema.

    // Si se ha introducido una tarjeta.
    if (SDin && !sdInOldState)
    {
        sdInOldState = SDin;
        setHostSPI(&host);

        if (sound == true){
            buzzer_ms(100);
        }

        // Monta la tarjeta.
        if (f_mount(&g_fileSystem, "", 1) != 0)
        {
            SDin = false;
            svError("Mount volume failed!.");
            lcdmenu_warning_message(WARNING_SD_FAILED);
            return false;
        }

        // Si el logging está habilitado se crea un archivo con la fecha y hora como nombre.
        if (enableLogging)
        {
            // Comprueba que la tarjeta no esté bloqueda contra escritura.
            if (SDblock)
            {
                svError("SD card is lock.");
                SDin = false;
                lcdmenu_warning_message(WARNING_SD_LOCKED);
                return false;
            }

            RTC_GetDatetime(RTC_1_PERIPHERAL, &datetime);
            sprintf(buffer, "%02d-%02d-%04d_%02d_%02d_%02d.csv", datetime.day, datetime.month,
                    datetime.year, datetime.hour, datetime.minute, datetime.second);

            error = f_open(&g_fileObject, _T(buffer), (FA_WRITE | FA_CREATE_ALWAYS));
            if (error > 0)
            {
                if (error == FR_EXIST)
                {
                    svWarn("File exists.");
                }
                else
                {
                    svError("Open file failed!.");
                    SDin = false;
                    lcdmenu_warning_message(WARNING_SD_FAILED);
                    return false;
                }
            }

            // Se añade la descripción de los campos
            error = f_puts("Date,Time,Voltaje,Current,Power,BatteryCapacity(mAh)\n", &g_fileObject);
            if (error == EOF)
            {
                svError("Write to file failed!.");
                return false;
            }
            f_sync(&g_fileObject); // Escribe los datos en cache al archivo.

            // El tiempo en ms se pasa a ticks. (1 tick = 5ms)
            timeInTicks = time / 5;

            logData.mutex = xSemaphoreCreateMutex();
            sdTimer = xTimerCreate("sd write timer",
                                   timeInTicks,
                                   pdTRUE,
                                   (void *)0,
                                    sdWrite);

            if (sdTimer == NULL)
            {
                svError("Task logging sdTimer creation failed!");
                SDin = false;
                return false;
            }

            xTimerStart(sdTimer, 0);
        }
        return true;
    }

    // Si se ha retirado una tarjeta.
    if (!SDin && sdInOldState)
    {
        sdInOldState = SDin;

        if (sound == true){
            buzzer_ms(100);
        }
        // Desmonta la unidad
        f_mount(NULL, "SD:", 1);

        if (enableLogging){
            // Borra el timer y el mutex si se han creado.
            if (sdTimer != NULL){
                xTimerStop(sdTimer, 0);
                xTimerDelete(sdTimer, 0);
            }

            if (logData.mutex != NULL){
                vSemaphoreDelete(logData.mutex);
            }
        }

        return true;
    }

    if (SDin && activeLogging)
    {
        if (xSemaphoreTake(logData.mutex, 0) == pdTRUE){
            logData.current = current;
            logData.voltage = voltage;
            logData.power = power;
            logData.batteryCapacity = batteryCapacity;
            xSemaphoreGive(logData.mutex);
        }
    }

    return true;
}

/**
 * Guarda los datos pendientes en cache y cierra el archivo en caso de
 * detectar una perdida de alimentación.
 */
void sd_power_loss(){
    if (SDin && activeLogging){
        f_puts("Power loss detected.", &g_fileObject);
        f_sync(&g_fileObject); // Guarda los datos en cache en el archivo.
        // Cierra el archivo.
        f_close(&g_fileObject);
        // Desmonta la unidad
        f_mount(NULL, "SD:", 1);
    }
}
