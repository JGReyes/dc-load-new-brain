/*
 *
 * File: sdlogging.h
 * Project: DC Load NewBrain
 * Created Date: 6/7/2019, 11:45
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef SDLOGGING_H_
#define SDLOGGING_H_

#include "fsl_sdspi_disk.h"
#include "ff.h"

extern volatile bool enableLogging;
extern volatile bool activeLogging;
extern volatile bool SDin;
extern volatile bool SDblock;

bool sd_logging(const float current, const float voltage, const float power,
                const uint32_t batteryCapacity, const uint32_t time);

void sd_power_loss();

#endif /* SDLOGGING_H_ */
