/*
 *
 * File: mcp4725.h
 * Project: DC Load NewBrain
 * Created Date: 6/5/2019, 15:52
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see <http: //www.gnu.org/licenses/>.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#ifndef MCP4725_H_
#define MCP4725_H_

#include <stdbool.h>
#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"

#define MCP4725_WRITE_FAST        0x00U
#define MCP4725_WRITE_DAC         0x40U
#define MCP4725_WRITE_DAC_EEPROM  0x60U
#define MCP4725_MASK              0xFFU

/**
 * Modos de apagado del DAC.
 */
typedef enum {
  MCP4725_POWER_DOWN_0 = 0, ///< Modo normal.
  MCP4725_POWER_DOWN_1,     ///< Apagado con resistencia de 1K a tierra.
  MCP4725_POWER_DOWN_100,   ///< Apagado con resistencia de 100K a tierra.
  MCP4725_POWER_DOWN_500    ///< Apagado con resistencia de 500K a tierra.
} mcp4725_power_down_t;

/**
 * Configuración del DAC.
 */
typedef struct {
  i2c_rtos_handle_t * i2c; ///< Para manejo de i2c en el SDK de NXP para FreeRTOS.
  uint8_t address;         ///< Dirección del dispositivo.
} mcp4725_config_t;

/**
 * Configuración a leer o escribir en la EEPROM del dispositivo.
 */
typedef struct {
  mcp4725_power_down_t power_down_mode; ///< Modo de apagado del DAC.
  uint16_t input_data; ///< Valor a usar. Dato de 12 bits. (0-4095)
} mcp4725_eeprom_t;


status_t mcp4725_set_voltage(const mcp4725_config_t *const dac,
                             const uint16_t value);

status_t mcp4725_power_down(const mcp4725_config_t *const dac,
                            const mcp4725_power_down_t mode);

status_t mcp4725_read_eeprom(const mcp4725_config_t *const dac,
                             mcp4725_eeprom_t *const eeprom);

status_t mcp4725_write_eeprom(const mcp4725_config_t *const dac,
                              const mcp4725_eeprom_t eeprom);

#endif /* MCP4725_H_ */
