/*
 *
 * File: mcp4725.h
 * Project: DC Load NewBrain
 * Created Date: 6/5/2019, 15:52
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: (INSERT DATE)         <--!!!!!!!!
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * (dd/mm/yy)  x.xx     JGReyes      xxxx <--!!!!!!!!
 *
*/

#include "mcp4725.h"

// Estructura para el uso del bus i2c con el SDK de NXP en FreeRTOS.
static i2c_master_transfer_t i2c_txMcp4725 = {
            .flags = kI2C_TransferDefaultFlag,
            .slaveAddress = 0,
            .direction = kI2C_Write,
            .subaddress = 0,
            .subaddressSize = 0,
            .data = NULL,
            .dataSize = 0
};

// Buffers para envio y recepción por i2c.
static uint8_t txbuffer[3];
static uint8_t rxbuffer[5];

/**
 * Fija un voltaje en el DAC.
 *
 * @param[in] dac Configuración del DAC a utilizar.
 * @param[in] value Valor del voltaje. Dato de 12 bits. (0-4095)
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp4725_set_voltage(const mcp4725_config_t *const dac,
                             const uint16_t value) {

  i2c_txMcp4725.slaveAddress = dac->address;
  i2c_txMcp4725.direction = kI2C_Write;
  i2c_txMcp4725.data = txbuffer;
  i2c_txMcp4725.dataSize = sizeof(txbuffer)-1;

  portENTER_CRITICAL();
  txbuffer[0] = (value >> 8) | MCP4725_WRITE_FAST; // Escribe los 4 bits más altos.
  txbuffer[1] = value & MCP4725_MASK;              // Escribe los 8 bits más bajos.
  portEXIT_CRITICAL();

  return I2C_RTOS_Transfer(dac->i2c, &i2c_txMcp4725);
}

/**
 * Pone el DAC en uno de los modos de apagado.
 *
 * @param[in] dac Configuración del DAC a utilizar.
 * @param[in] mode Modo de apagado a utilizar.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp4725_power_down(const mcp4725_config_t *const dac,
                            const mcp4725_power_down_t mode) {

    i2c_txMcp4725.slaveAddress = dac->address;
    i2c_txMcp4725.direction = kI2C_Write;
    i2c_txMcp4725.data = txbuffer;
    i2c_txMcp4725.dataSize = sizeof(txbuffer)-1;

    txbuffer[0] = ((uint8_t)mode<<4) | MCP4725_WRITE_FAST;
    txbuffer[1] = 0;

    return I2C_RTOS_Transfer(dac->i2c, &i2c_txMcp4725);
}

/**
 * Lee la configuración escrita en la EEPROM del DAC.
 * @param[in] dac Configuración con el DAC a utilizar.
 * @param[out] eeprom Configuración devuelta por el DAC.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp4725_read_eeprom(const mcp4725_config_t *const dac,
                             mcp4725_eeprom_t *const eeprom) {

    i2c_txMcp4725.slaveAddress = dac->address;
    i2c_txMcp4725.direction = kI2C_Read;
    i2c_txMcp4725.data = rxbuffer;
    i2c_txMcp4725.dataSize = sizeof(rxbuffer);

    status_t result = I2C_RTOS_Transfer(dac->i2c, &i2c_txMcp4725);

    if (result == kStatus_Success) {
        eeprom->power_down_mode =  (uint16_t)rxbuffer[3] >> 5;
        eeprom->input_data      = ((uint16_t)rxbuffer[3] & 0b1111) << 8;
        eeprom->input_data     |=  (uint16_t)rxbuffer[4];
    }
    return result;
}

/**
 * Escribe la configuración en la EEPROM del DAC.
 * @param[in] dac Configuración con el DAC a utilizar.
 * @param[in] eeprom Configuración a escribir en el DAC.
 * @return Estado de la operación. kStatus_Success o código del error correspondiente.
 */
status_t mcp4725_write_eeprom(const mcp4725_config_t *const dac,
                              const mcp4725_eeprom_t eeprom) {

    i2c_txMcp4725.slaveAddress = dac->address;
    i2c_txMcp4725.direction = kI2C_Write;
    i2c_txMcp4725.data = txbuffer;
    i2c_txMcp4725.dataSize = sizeof(txbuffer);

    txbuffer[0] = (eeprom.power_down_mode<<1) | MCP4725_WRITE_DAC_EEPROM;
    txbuffer[1] = eeprom.input_data >> 4;
    txbuffer[2] = (eeprom.input_data<<4) & MCP4725_MASK;

    return I2C_RTOS_Transfer(dac->i2c, &i2c_txMcp4725);
}
