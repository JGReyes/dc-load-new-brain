/*
 *
 * File: main.c
 * Project: DC Load NewBrain
 * Created Date: 30/4/2019, 13:10
 * Author: JGReyes
 * e-mail: josegpuntoreyes@gmail.com
 * web: www.circuiteando.net
 *
 * DC Load NewBrain is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * DC Load NewBrain is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DC Load NewBrain.  If not, see www.gnu.org/licenses/.
 *
 * Copyright (c) 2019 JGReyes
 *
 * ------------------------------------
 * Last Modified: 24/08/2023
 * Modified By: JGReyes
 * ------------------------------------
 *
 * Changes:
 *
 * Date       Version   Author      Detail
 * 24/08/23     1.1     JGReyes     Posibilidad de desactivar la protección de polaridad inversa.
 *
*/

/* FreeRTOS kernel includes. */

#include <clock_config.h>
#include <fsl_adc16.h>
#include <fsl_common.h>
#include <fsl_gpio.h>
#include <fsl_lptmr.h>
#include <fsl_pit.h>
#include <fsl_port.h>
#include <fsl_tpm.h>
#include <fsl_smc.h>
#include <fsl_cop.h>
#include <keypad/keypad.h>
#include <MKL27Z644.h>
#include <opmodes.h>
#include <peripherals.h>
#include <pin_mux.h>
#include <portmacro.h>
#include <projdefs.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sysview_enable_conf.h>
#include <task.h>
#include <timers.h>
#include <math.h>
#include "main.h"
#include "sdlogging.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
//Tamaños de pila para las tareas.
#define DEFAULT_STACK_SIZE 128 // 128 palabras de 4 bytes (32 bits) = 512 bytes.

// Prioridad de las tareas.
#define MAIN_TASK_PRIORITY 1
#define KEYPAD_TASK_PRIORITY 4
#define LCD_TASK_PRIORITY 2
#define CONTROL_TASK_PRIORITY 2
#define INPUT_TASK_PRIORITY 3

#define BOOTLOADER_CONFIG 1

#define CURSOR_NOTIF_VALUE 1000000

// Usada para las estadísticas de FreeRTOS.
static uint32_t RTOS_RunTimeCounter;

/**
 * Variable con la configuración del adc para su uso en las diferentes partes
 * del programa.
 *
 */
mcp3426_t adc;

/**
 * Variable con la configuración del dac para su uso en las diferentes partes
 * del programa.
 */
mcp4725_config_t dac;

/**
 * Almacena el modo actual de operación de la carga electrónica.
 */
volatile dcLoad_Mode_t operationMode;

/**
 * Indica si se debe o no bloquear el encoder.
 */
volatile bool lockEncoder = false;

/**
 * Indica si se debe conectar la carga o no.
 *
 * Al ponerse a true, una tarea se encarga de activar los mosfets y cerrar los relés.
 * Si se pone a false, se desactivan los mosfets y se abren los relés.
 */
volatile bool loadOutput = false;

/**
 * Indica si se deben mostrar las temperaturas en pantalla.
 */
volatile bool showTemp = true;

/**
 * Almacena el manejador del timer anti-rebortes para las interrupciones del puerto A.
 */
static TimerHandle_t debounceTimer = NULL;

/**
 * Almacena el manejador de la tarea task_Control para enviar notificiaciones más adelante.
 */
static TaskHandle_t taskControl_Handler = NULL;

/**
 * Indica si se está mostrando un mensaje de advertencia.
 */
volatile bool showingWarnMessage = false;

/**
 * Indica si se utiliza el módo de transición rápido. (Deshabilitando la corrección de error).
 */
volatile bool transientFastMode = false;

/**
 * Segundos transcurridos mientras se descarga un batería en el modo BC.
 */
volatile uint32_t Seconds = 0;

volatile float actualVoltage = 0; // Voltaje actual.

// Variables para configurar los límites máximos.
static float PowerCutOff = 310.0;     // Máxima potencia permitida en vatios.
static float CurrentCutOff = 15.2;    // Máxima corriente permitida en amperios.
static int16_t ResistorCutOff = 999;  // Máximo valor de resistencia permitida.
static float VoltageCutOff = 100.0;   // Máximo voltaje permitido.
static float TempCutOff = 70.0;       // Máxima temperatura permitida.
static float MaxBatteryCurrent = 2.0; // Máxima corriente permitida durante la descarga
                                      // de baterías.
uint32_t SDLoggingTime = 1000; // Intervalo entre escrituras en la tarjeta SD (en ms).

volatile bool sound = true; // Indica si se activa el buzzer al pulsar una tecla o mover el encoder.
volatile bool slewDone = false; // Indica si se ha realizado un arranque o parada progresivo de la carga.
volatile bool slewEnable = false;         // Indica si el arranque o parada progresivo está activado.
volatile uint16_t riseRate = 100; // Pendiente de subida al conectar la carga en mA/ms.
volatile uint16_t fallRate = 100; // Pendiente de bajada al desconectar la carga en mA/ms.
volatile bool usbIn = false;      // Indica si ha conectado el PC al puerto usb.
volatile bool isAlive = false;    // Indica que el hilo principal no está bloqueado.
volatile bool polarityEn = true;  // Indica si se debe comprobar la polaridad.
/******************************************************************************!
 * Functions
 ******************************************************************************/

/**
 * Función encargada de mapear un valor de un rango a otro distinto.
 * Sólo acepta números de 16 bits sin signo.
 *
 * @param[in] x Valor que se quiere mapear.
 * @param[in] in_min Número mínimo del rango actual.
 * @param[in] in_max Número máximo del rango actual.
 * @param[in] out_min Número mínimo del rango deseado.
 * @param[in] out_max Número máximo del rango deseado.
 * @return El nuevo valor mapeado al rango deseado.
 */
static uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min,
                    uint16_t out_max)
{
    return out_min + (x - in_min) * (out_max - out_min) / (in_max - in_min);
}

/**
 * Establece un tiempo de espera en microsegundos.(Bloquea la ejecución mientras tanto).
 *
 * Esta función no es precisa, solo para asegurarse de que un tiempo mínimo
 * de espera es respetado.
 * @param[in] us Número de microsegundos a esperar.
 */
void delay_us(uint16_t us) // El timer TPM1 es de 16 bits.
{
    portENTER_CRITICAL();
    TPM_StopTimer(TPM_1_PERIPHERAL);
    TPM_1_PERIPHERAL->CNT = 0;    // Resetea el contador a 0 del timer TPM1.
    TPM_1_PERIPHERAL->SC |= 0x80; // Pone el flag de overflow a 0.
    TPM_1_PERIPHERAL->MOD = us;   // Valor a contar.
    TPM_StartTimer(TPM_1_PERIPHERAL, kTPM_SystemClock);
    // Como el timer TPM1 está configurado a 1 MHz, cada tick es igual a 1us.
    while ((TPM_1_PERIPHERAL->SC & 0x80) == 0)
    {
    }; // Comprueba el bit Timer Overflow Flag.
    TPM_StopTimer(TPM_1_PERIPHERAL);
    portEXIT_CRITICAL();
}

/**
 * Resetea las variables asociadas al encoder a 0.
 */
void resetEncoder()
{
    portENTER_CRITICAL();
    encoder.reading = 0;
    encoder.position = 0;
    encoder.lastCount = 0;
    portEXIT_CRITICAL();
}

/**
 * Comprueba que la polaridad de la carga no esté invertida.
 *
 * La circuiteria no es capaz de leer voltajes negativos, dando como resultado
 * 0 V, por lo que se asegura que está correctamente conectada si el voltaje
 * es superior a 50 mV.
 * @param actualVoltage Voltaje actual de la carga.
 * @return True en caso de pasar satisfactoriamente la prueba, False en caso contrario.
 */
bool checkPolarity()
{
    if (polarityEn && (actualVoltage < 0.050))
    { // Protección contra polaridad inversa.
        loadOutput = pdFALSE;
        showingWarnMessage = pdTRUE;
        resetEncoder();
        lcdmenu_polarityMessage();
        svWarn("Reverse Polarity");
        return pdFALSE;
    }
    else{
        return pdTRUE;
    }
}

/**
 * Genera un sonido con el buzzer el tiempo especificado.
 * @param ms Tiempo en milisegundos.
 */
void buzzer_ms(uint16_t ms)
{

    // Asegura que no haya una petición de interrupción antes de activar el buzzer.
    PIT_ClearStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_0, kPIT_TimerFlag);
    PIT_ClearStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_1, kPIT_TimerFlag);

    // Activa el buzzer.
    PIT_StartTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0); // El canal 0 activa el 1.
    vTaskDelay(ms / portTICK_PERIOD_MS);

    // Desactiva el buzzer. (Para ambos canales).
    PIT_StopTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0);
    PIT_StopTimer(PIT_1_PERIPHERAL, kPIT_Chnl_1);
    GPIO_PinWrite(BOARD_INITPINS_BUZZER_GPIO, BOARD_INITPINS_BUZZER_PIN, 0);
}

/**
 * Lee las temperaturas de los mosfets y controla la velocidad del ventilador.
 * @param[in] timer Timer encargado de ejecutar esta función.
 */
static void temp_and_fan(TimerHandle_t timer)
{

    static float temp;  // Temperatura del mosfet 1.
    static float temp2; // Temperatura del mosfet 2.
    float tmp;          // Variable temporal para la temperatura del mosfet 1.
    float tmp2;         // Variable temporal para la temperatura del mosfet 2.
    float hot_temp = 0; // Temperatura más alta de las dos anteriores.

    // Si el hilo taskControl está en un menú se refresca el watchdog.
    if ((operationMode == DCLOAD_MODE_NO_CHANGE) ||
        (operationMode == DCLOAD_MODE_LOGSD) || (operationMode ==
         DCLOAD_MODE_SETUP)){
        // Refresca el watchdog
        COP_Refresh(SIM);
    }else
    {
        // Si no está en un menú comprueba si está vivo.
        if (isAlive){
            COP_Refresh(SIM);
            // Si está vivo el hilo tackControl tiene que cambiarlo a true.
            isAlive = false;
        }
        else{
            ; // Misra c 15.7
        }
    }

    ADC16_SetChannelConfig(ADC0, ADC16_0_muxMode, &ADC16_0_channelsConfig[0]);
    vTaskDelay(5 / portTICK_PERIOD_MS);
    tmp = ADC16_GetChannelConversionValue(ADC0, ADC16_0_muxMode);
    // Milivoltios en el ADC = Valor ADC x resolución.
    // Resolución = Vref (TP6) 3.100V/1023 (valor máximo de 10 bits) = 3.030 mV.
    // Milivoltios reales. El divisor de voltage 5v a 3v tiene un ratio Vout/Vin de 0.6.
    tmp *= 1.6;
    tmp *= 3.030;
    // La temperatura es el valor / 10 mV/ºC (LM35)
    temp = tmp / 10.0;

    ADC16_SetChannelConfig(ADC0, ADC16_0_muxMode, &ADC16_0_channelsConfig[1]);
    vTaskDelay(5 / portTICK_PERIOD_MS);
    tmp2 = ADC16_GetChannelConversionValue(ADC0, ADC16_0_muxMode);
    tmp2 *= 1.667;
    tmp2 *= 3.030;
    temp2 = tmp2 / 10.0;

    svPrint("Temps Updated");

    // Protección contra sobrecalentamiento
    if ((temp >= TempCutOff) || (temp2 >= TempCutOff))
    {
        loadOutput = false;
        resetEncoder();
        lcdmenu_warning_message(WARNING_OVER_TEMPERATURE);
    }

    if ((operationMode != DCLOAD_MODE_NO_CHANGE) && showTemp && (!transientFastMode)){
        lcdmenu_temperature_update(temp, temp2); // Actualiza los datos en pantalla.
    }
    // Controla la velocidad del ventilador.
    if (temp >= temp2)
    {
        hot_temp = temp;
    }
    else
    {
        hot_temp = temp2;
    }


    if (hot_temp >= 40)
    {
        // Cambia la velocidad del ventilador entre el 45% y 100%
        // dependiendo de la temperatura, entre un rango de 40 a 60 grados celsius.
        uint8_t speed = map((uint16_t)hot_temp, 40, 60, 45, 100);
        TPM_UpdatePwmDutycycle(TPM0, kTPM_Chnl_5, kTPM_EdgeAlignedPwm, speed);
        svPrintfTarget("Fan runing at %d %%", speed);
    }
    else
    {
        if (hot_temp < 35)
        {
            TPM_UpdatePwmDutycycle(TPM0, kTPM_Chnl_5, kTPM_EdgeAlignedPwm, 0);
            svPrint("Fan stopped");
        }
        else{
            ; // Misra c 15.7
        }

    }
}

/**
 * Lee el voltaje a la entrada de la carga.
 * @param[in] offset True para realizar un offset del ADC sin nada conectado.
 * @param[in] wait Indica si se quiere esperar el tiempo de sampleo o no.
 *       (Ponerlo a True salvo que se sepa lo que se hace)
 * @return Voltaje, o -1 en caso de error.
 */
float readVoltage(bool offset, bool wait)
{
    float temp = 0.0;               // Almacena el voltaje temporalmente.
    static float voltageOffset = 0; // Almacena el offset a ganancia 1.
    int16_t adcRead = 0;
    static float calibValue = 1; // Valor de calibrado.

    if (mcp3426_read_channel(&adc, channel_1, &adcRead, wait) !=
        kStatus_Success){
        return -1;
    }

    // Valor en V en el ADC.
    temp = ((adcRead - voltageOffset) * (2.048 / 32767));
    // Cada 20 uV representa 1 mV en la entrada de la carga.
    temp = temp / 0.000020;
    // Se ajusta con el valor de calibrado.
    temp = temp * calibValue;
    // Pasa de mV a V.
    temp = temp / 1000;

    // Ajusta el valor de calibrado según el voltaje.
    if (temp > 55.5){
        calibValue = 1.001502;
    }
    else if (temp > 40){
        calibValue = 1.001198;
    }
    else if (temp > 1.3){
        calibValue = 1.002007;
    }
    else{
        calibValue = 1.007065;
    }

    // Fija el offset a ganancia 1.
    // Debería llamarse únicamente al arrancar y sin ningún cable conectado.
    if (offset)
    {
        if (adcRead > 15)
        { // Voltage superior a 1 mV.
            if (sound == true){
                buzzer_ms(1000);
            }

            showTemp = false;
            lcdmenu_clear();
            lcdmenu_moveCursor(0, 0);
            lcdmenu_writeString("Input V. > 1 mV!");
            lcdmenu_moveCursor(0, 2);
            lcdmenu_writeString("Disconnect any cable");
            lcdmenu_moveCursor(0, 3);
            lcdmenu_writeString("and reset the device");
            vTaskDelay(100 / portTICK_PERIOD_MS);
            for (;;){
                ;
            }
        }
        voltageOffset = adcRead;
        showTemp = false;
        lcdmenu_clear();
        lcdmenu_moveCursor(4, 1);
        lcdmenu_writeString("V. Zero Offset");
        svPrint("Set V.Offset");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    return temp;
}

/**
 * Lee la corriente en la carga.
 * @param[in] offset True para realizar un offset del ADC sin nada conectado.
 * @return Corriente, o -1 en caso de error.
 */
static float readCurrent(bool offset)
{
    float temp = 0.0;               // Almacena la corriente temporalmente.
    static float currentOffset = 0; // Almacena la diferencia en corriente al ajustarse a 0 en el arranque.
    int16_t adcRead = 0;
    static uint8_t gain = 4;     // Almacena la ganancia actual.
    static float calibValue = 1; // Valor de calibrado.

    if (mcp3426_update_config(&adc, adc.config, pdFALSE) !=
        kStatus_Success){
        return -1;
    }

    if (mcp3426_read_channel(&adc, channel_2, &adcRead, true) !=
        kStatus_Success){
        return -1;
    }

    // Valor en V en el ADC.
    temp = (((adcRead - currentOffset) * (2.048 / 32767)) / gain);
    // Cada 100 uV representa 1 mA en la entrada de la carga.
    temp = temp / 0.000100;
    // Se ajusta con el valor de calibrado.
    temp = temp * calibValue * 2; // Son dos en paralelo
    // Pasa de mA a A.
    temp = temp / 1000;

    // Ajusta el valor de calibrado según el amperaje.
    if (temp > 9.5){
        calibValue = 0.978829;
    }
    else if (temp >= 7.000){
        calibValue = 0.987187;
    }
    else if (temp >= 2.800){
        calibValue = 0.986351;
    }
    else if (temp >= 0.200){
        calibValue = 0.985222;
    }
    else{
        calibValue = 1.0;
    }

    // Ajusta la ganancia según el amperaje.
    if (temp > 9.5)
    {
        adc.config.gain[channel_2] = gain_x2;
        gain = 2;
    }
    else
    {
        adc.config.gain[channel_2] = gain_x4;
        gain = 4;
    }

    // Fija el offset a ganancia 4.
    // Debería llamarse únicamente al arrancar y sin ningún cable conectado.
    if (offset)
    {
        if (adcRead > (4 * gain))
        { // Corriente superior a 2 mA. (Ganancia 4 por defecto).
            if (sound){
                buzzer_ms(1000);
            }
            showTemp = false;
            lcdmenu_clear();
            lcdmenu_moveCursor(0, 2);
            lcdmenu_writeString("Input C. > 2 mA!");
            vTaskDelay(100 / portTICK_PERIOD_MS);
            for (;;){
                ;
            }
        }
        currentOffset = adcRead;
        showTemp = false;
        lcdmenu_clear();
        lcdmenu_moveCursor(4, 1);
        lcdmenu_writeString("C. Zero Offset");
        svPrint("Set C.Offset");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    return temp;
}

/**
 * Fija el valor especificado del DAC de forma prograsiva según la pendiente indicada.
 *
 * @note La pendiente se mide en mA/ms, y se configura mediante las variables globales
 * riseRate y fallRate.
 * @param dacValue  Valor final del DAC.
 * @param isRiseRate Si es True es pendiente de subida, de lo contrario es
 * pendiente de bajada.
 */
void setDacWSlewRate(uint16_t dacValue, bool isRiseRate){
    static bool fallDone = false;
    static uint16_t lastDacValue = 0;
    int32_t value = 0;
    bool tIsRiseRate = isRiseRate;

    if (dacValue > lastDacValue){
        // Se pide la corriente alta.
        tIsRiseRate = true;
    }
    else{
        // Se pide la corriente baja.
        tIsRiseRate = false;
    }

    value = (int32_t)lastDacValue;
    lastDacValue = dacValue;

    // Si es el tiempo de subida.
    if (tIsRiseRate) {
        uint16_t step = riseRate/3.87096;

        while (value < dacValue){
            value += step;
            if (value > dacValue){
                value = dacValue;
            }
            mcp4725_set_voltage(&dac, (uint16_t)value);
            delay_us(1000);
        }
        fallDone = false;

    }else if (!fallDone && (value > dacValue)){
        // Es el Tiempo de bajada.
        uint16_t step = fallRate/3.87096;

        while (value > dacValue){
            value -= step;
            if (value < dacValue){
                value = dacValue;
            }
            mcp4725_set_voltage(&dac, (uint16_t)value);
            delay_us(1000);
        }
        fallDone = true;
    }
    else{
        ; // Misra c 15.7
    }

    slewDone = true;
}

/**
 * Activa o desactiva la carga y los relés.
 *
 * @note Esta función comprueba periodicamente la variable loadOutput.
 * @param[in] timer Timer encargado de ejecutar esta función.
 */
static void check_loadOutput(TimerHandle_t timer)
{
    static bool relaysOn = pdFALSE; // Almacena el estado de los relés.

    if (loadOutput == pdFALSE)
    {
        if (slewEnable){
            setDacWSlewRate(0, false);
        }
        else{
            mcp4725_set_voltage(&dac, 0);
        }

        delay_us(5); // Tiempo para asegurarse de que el mosfet está apagado.

        if (operationMode == DCLOAD_MODE_BC){
            // Para de contar los segundos.
            DisableIRQ(RTC_1_SECONDS_IRQN);
        }

        if (enableLogging){
            // Deja de loggear a la SD.
            activeLogging = false;
        }

        if (relaysOn == true)
        {
            GPIO_PinWrite(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, 0);

            // Las esperas durante el cambio de estado del relé bloquean la ejecución
            // de otras tareas ya que los timers tienen la máxima prioridad. De esta
            // manera se asegura que no se realiza ninguna acción durante la
            // transición de los contactos.

            delay_us(50000); // Tiempo máximo para abrir los contactos según datasheet de los relés.
            relaysOn = pdFALSE;
            lcdmenu_load(pdFALSE);
            svPrint("Relays OFF");
        }
    }
    else
    {
        if (!relaysOn)
        {
            if (((actualVoltage >= 0.050) || !polarityEn) && (operationMode != DCLOAD_MODE_NO_CHANGE)
                && (operationMode != DCLOAD_MODE_LOGSD) && (operationMode != DCLOAD_MODE_SETUP))
            {
                GPIO_PinWrite(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, 1);
                delay_us(50000); // Tiempo máximo para cerrar los contactos según datasheet de los relés.
                relaysOn = pdTRUE;
                lcdmenu_load(pdTRUE);
                svPrint("Relays ON");

                if (operationMode == DCLOAD_MODE_BC){
                    // Vuelve a contar los segundos.
                    EnableIRQ(RTC_1_SECONDS_IRQN);
                }

                if (enableLogging){
                    // Vuelve a loggear a la SD.
                    activeLogging = true;
                }

                slewDone = false;
            }
            else{
                ; // Misra c 15.7
            }
        }
        else{
            ; // Misra c 15.7
        }
    }
}

/**
 * Comprueba que se mantiene el botón el tiempo suficiente para activar
 * el modo de selección.
 * @param[in] timer Timer encargado de ejecutar esta función.
 */
static void check_selMenu(TimerHandle_t timer)
{
    if (GPIO_PinRead(BOARD_INITPINS_CURSOR_GPIO, BOARD_INITPINS_CURSOR_PIN) == 0)
    {

        loadOutput = pdFALSE;
        resetEncoder();
        xTaskNotify(taskControl_Handler, MENU_SELECT_VALUE,
                    eSetValueWithOverwrite);
    }
}

/**
 * Controla el dac, y por lo tanto, fija la corriente a utilizar según el modo
 * de funcionamiento.
 *
 * @param[in] mode Modo de operación de la carga electrónica.
 * @param[in] actualVoltage Voltaje actual.
 * @param[in] actualCurrent Corriente actual.
 */
static void dacControl(dcLoad_Mode_t mode, const float actualVoltage,
                       const float actualCurrent)
{

    float setCurrent = 0;                   // Almacena la corriente a utilizar.
    float setValue = 0;                     // Almacena el valor introducido con el encoder o teclado.
    uint16_t controlVoltage = 0;            // Almacena el valor para controlar el dac.
    static uint16_t lastControlVoltage = 0; // Almacena el valor anterior mandado al dac.
    static uint16_t compVoltage = 0;        // Almacena el voltaje de compensación para el dac.

    if ((loadOutput == pdTRUE) && (transientFastMode == pdFALSE))
    {
        setValue = encoder.reading * 1000.0;

        if ((mode == DCLOAD_MODE_CC) || (mode == DCLOAD_MODE_TC) ||
            (mode == DCLOAD_MODE_TP) || (mode == DCLOAD_MODE_TT) ||
            (mode == DCLOAD_MODE_BC))
        {
            setCurrent = setValue;
        }

        if (mode == DCLOAD_MODE_CP)
        {
            setCurrent = setValue / actualVoltage; // Calcula la corriente dada la potencia.
            if (setCurrent > (CurrentCutOff * 1000)){
                setCurrent = CurrentCutOff * 1000;
            }
        }

        if (mode == DCLOAD_MODE_CR)
        {
            setCurrent = (actualVoltage / encoder.reading) * 1000; // Calcula la corriente dada la resistencia.
            if (setCurrent > (CurrentCutOff * 1000)){
                setCurrent = CurrentCutOff * 1000;
            }
        }

        /*
         * La salida del DAC max. es de 4096V que pasa por un divisor de voltaje
         * con una relación Vout/Vin de 0.193548.
         *
         * El voltaje a aplicar en los Opams es de 100 uV por mA. 750mV = 7.5A *2 = 15A (Máximo de la carga)
         * La resolución de DAC es de 1mV, lo que equivale a 10mA. Como pasa por el
         * divisor, se aplica la relación anterior quedando:
         * 10mA * 0.193548 = 1.93548mA *2 = 3.87096mA por paso.
         *
         * Según el datasheet la salida se comporta de forma más lineal entre
         * los pasos 100 y 4000.
         *
         * Se usa un métod de calibrado de 2 puntos. (válido para ADCs y DACs).
         * 1- Se toman dos mediciones: al 10% y 90% del rango. Y se comparan con
         * el valor ideal.
         * 10% Obtenido -> 106.6 Ideal -> 103
         * 90% Obtenido -> 3869  Ideal -> 3871
         *
         * 2- Los errores de offset y ganancia se corrigen con una ecuación
         * lineal y = m*x + b, donde m es la pendiente y b el offset.
         *
         * Error de ganancia = (3869-107)/(3871-103) = 0.998407
         *
         * Como el valor obtenido es menor al real 1+(1-0.998407) = 1.001593
         *
         * El error de offset se calcula como b = y-m*x.
         *
         * Error de offset = 107 - (0.998407 * 103) = +4.164079
         *
         */

        controlVoltage = (uint16_t)(setCurrent / 3.87096);
        // Aplica la ecuación anterior cuando está en la zona lineal.
        if (controlVoltage > 100){
            controlVoltage = (1.001593 * controlVoltage) - 4.164079;
        }

        /* Aún quedan los errores del resto de componentes en la ruta de la señal,
         * el divisor de tensión y los dos opamps. Por lo que aunque la salida del
         * DAC es correcta, la corriente resultante no lo es.
         *
         * Para compensar estos errores no lineares se ajusta el valor según la diferencia
         * con la corriente requerida, siempre que no exceda el valor del error
         * máximo.
         *
         * (14.52A-0.454A)/(15A-0.5A) = 0.970069
         * 1+(1-0.970069) = 1.029931
         *
         */

        controlVoltage = controlVoltage * 1.029931;
        float current = actualCurrent * 1000.0;
        float moreCurrent = current - setCurrent;
        float lessCurrent = setCurrent - current;
        uint16_t stepError = 0;
        bool updateDAC = false;

        if ((controlVoltage == lastControlVoltage) && (setCurrent > 0.0))
        {

            if ((moreCurrent >= 4) && (moreCurrent <= 150))
            {
                stepError = moreCurrent / 3.87096;
                compVoltage = compVoltage - stepError;
                updateDAC = true;
            }
            else
            {
                if ((lessCurrent >= 4) && (lessCurrent <= 150))
                {
                    stepError = lessCurrent / 3.87096;
                    compVoltage = compVoltage + stepError;
                    updateDAC = true;
                }
                else
                {
                    // La corriente está estabilizada por lo que se permite hacer
                    // una subida progresiva la próxima vez.
                    if ((operationMode != DCLOAD_MODE_TC) && (operationMode !=
                        DCLOAD_MODE_TP) && (operationMode != DCLOAD_MODE_TT)){
                        slewDone = false;
                    }
                    else{
                        ; // Misra c 15.7
                    }

                    // Si se mantiene el voltaje de control y la corriente es 0
                    // es que se anteriormente se seleccionó la misma corriente.
                    if (actualCurrent < 0.001){
                        updateDAC = true;
                    }
                }
            }
        }
        else
        {
            compVoltage = controlVoltage;
            lastControlVoltage = controlVoltage;
            updateDAC = true;
        }

        if (checkPolarity() && updateDAC){ // Protección contra polaridad inversa.
            if ((operationMode != DCLOAD_MODE_TC) && (operationMode !=
               DCLOAD_MODE_TP) && (operationMode != DCLOAD_MODE_TT)){

                if (slewEnable && !slewDone){
                    setDacWSlewRate(compVoltage, true);
                }
                else{
                    mcp4725_set_voltage(&dac, compVoltage);
                }
            }
            else if (slewEnable){
                    setDacWSlewRate(compVoltage, true);
            }
                else{
                    mcp4725_set_voltage(&dac, compVoltage);
                }
        }
    }
}

/**
 * Tarea encargada de controlar la tarea a usar dependiendo del modo seleccionado.
 * Así como de mostrar el voltaje y amperage en pantalla.
 *
 * @param[in] pvParameter
 */
static void taskControl(void *pvParameter)
{
    float tmpRead = 0;       // Almacena de fora temporal las lecturas.
    float actualCurrent = 0; // Corriente actual.
    float actualPower = 0;   // Potencia actual.
    uint32_t notifValue = 0; // Almacena el valor notificado por otra tarea.

    float lowCurrent = 0;  // Corriente baja en modo transient.
    float highCurrent = 0; // Corriente alta en modo transient.
    uint32_t period = 0;   // Periodo de conmutación en modo transient.
    static bool lastUSBstate = false; // Indica si había o no conectado un cable USB anteriormente.

    // Habilita la salida de los adaptadores de voltaje.
    GPIO_PinWrite(BOARD_INITPINS_LS_OE_GPIO, BOARD_INITPINS_LS_OE_PIN, 1);

    // Inicializa el dac y adc.
    dac.i2c = &I2C_0_rtosHandle;
    dac.address = 0x61;
    mcp3426_init(&adc, &I2C_0_rtosHandle, 0x68, channel_1, one_shot_sampling,
                 sRate_15, gain_x1, gain_x4);
    mcp4725_set_voltage(&dac, 0);

    // Espera a que otra tarea inicialize objetos compartidos y lo notifique.
    xTaskNotifyWait(0x00,
                    0x00,
                    NULL,
                    portMAX_DELAY);

    lcdmenu_backLight(pdTRUE);

    // Asegura que los relés y ventiladores estén apagados.
    TPM_UpdatePwmDutycycle(TPM_0_PERIPHERAL, kTPM_Chnl_5, kTPM_EdgeAlignedPwm, 0);
    GPIO_PinWrite(BOARD_INITPINS_RLY_GPIO, BOARD_INITPINS_RLY_PIN, 0);

    lcdmenu_intro();

    /* Habilita la interrupción que detecta la conexión de la batería.
       La intrucción anterior "lcdmneu_intro()", tiene un delay de 3 segundos.
       Esto es importante puesto que si hay un error y se activa la interrupción
       constantemente el micro pasa a modo de ahorro máximo y es muy dificil
       poder conectar con él para reprogramarlo o depurarlo. Esto ofrece 3
       segundos de margen para poder hacerlo si fuera necesario.
    */
    PORT_SetPinInterruptConfig(BOARD_INITPINS_VBAT_DETECT_PORT,
                               BOARD_INITPINS_VBAT_DETECT_PIN,
                               kPORT_InterruptEitherEdge);

    // Resume la tarea encargada de notificar entradas por teclado.
    TaskHandle_t taskHandle = xTaskGetHandle("inputValue_task");
    vTaskResume(taskHandle);

    // Arranca el timer para el encoder.
    LPTMR_StartTimer(LPTMR_0_PERIPHERAL);

    // Para leer la temperatura y controlar el ventilador cada 5 segundos.
    TimerHandle_t tempTimer = xTimerCreate("Temp and fan timer",
                                           (5000 / portTICK_PERIOD_MS),
                                           pdTRUE,
                                           (void *)0,
                                           temp_and_fan);
    if (tempTimer == NULL)
    {
        svError("Task taskControl tempTimer creation failed!");
        for (;;){
            ;
        }
    }

    // Para comprobar si se debe apagar o no la carga cada 20 ms.
    TimerHandle_t loadTimer = xTimerCreate("check loadOutput timer",
                                           (20 / portTICK_PERIOD_MS),
                                           pdTRUE,
                                           (void *)0,
                                           check_loadOutput);

    if (loadTimer == NULL)
    {
        svError("Task taskControl loadTimer creation failed!");
        for (;;){
            ;
        }
    }

    // Para comprobar si se ha mantenido más de 1 segundos el botón cursor
    // y mostrar el menú de selección.
    TimerHandle_t selTimer = xTimerCreate("check SelectionMode timer",
                                          (1000 / portTICK_PERIOD_MS),
                                          pdFALSE,
                                          (void *)0,
                                          check_selMenu);
    if (selTimer == NULL)
    {
        svError("Task taskControl selTimer creation failed!");
        for (;;){
            ;
        }
    }

    // Para alternar la corriente en el modo TC.
    TimerHandle_t transientTimer = xTimerCreate("transient_Toggle timer",
                                                (1000 / portTICK_PERIOD_MS),
                                                pdTRUE,
                                                (void *)0,
                                                opmodes_transientToggle);
    if (transientTimer == NULL)
    {
        svError("Task taskControl transientTimer creation failed!");
        for (;;){
            ;
        }
    }

    xTimerStart(tempTimer, 0);
    xTimerStart(loadTimer, 0);

    // Realiza un offset
    readVoltage(true, true);
    readCurrent(true);

    // El modo por defecto el el de corriente constante.
    operationMode = DCLOAD_MODE_CC;
    lcdmenu_modeMenu(operationMode);
    encoder.factor = 100;
    showTemp = true;

    // Fuerza la aparición de las lecturas.
    lcdmenu_modeMenu_update(0, 0, 0, true);

    for (;;)
    {
        // Calcula el voltaje.
        tmpRead = readVoltage(false, true);
        if (tmpRead != -1){
            actualVoltage = tmpRead;
        }
        else{
            continue;
        }

        // Calcula la corriente.
        tmpRead = readCurrent(false);
        if (tmpRead != -1){
            actualCurrent = tmpRead;
        }
        else{
            continue;
        }

        // Indica que el hilo está en ejecución.
        // En caso de error constante en las lecturas de voltaje o corriente
        // no se ejecutaría y el watchdog produciría un reset del micro.
        isAlive = true;

        // Calcula la potencia.
        actualPower = actualVoltage * actualCurrent;

        // Para prevenir que aparezcan valores negativos en la pantalla en caso de error.
        if (actualPower <= 0.0){
            actualPower = 0.0;
        }

        if (actualVoltage <= 0.0){
            actualVoltage = 0.0;
        }

        if (actualCurrent <= 0.0){
            actualCurrent = 0.0;
        }

        // Comprueba si se ha insertado/desconectado un cable USB
        // para emitir un sonido.
        if (usbIn != lastUSBstate){
            if (sound == true){
                buzzer_ms(100);
            }
            lastUSBstate = usbIn;
        }

        // Si el logging está activado se exportan los datos a la SD.
        sd_logging(actualCurrent, actualVoltage, actualPower, eBatteryCapacity,
                   SDLoggingTime);

        // Ajusta la corriente a utilizar según el modo de operación.
        dacControl(operationMode, actualVoltage, actualCurrent);

        // Actualiza las lecturas en la pantalla.
        switch (operationMode)
        {
        case DCLOAD_MODE_CC:
            notifValue = lcdmenu_requestInput(0, 3, false, 8);
            break;
        case DCLOAD_MODE_CP:
            notifValue = lcdmenu_requestInput(0, 3, false, 11);
            break;
        case DCLOAD_MODE_CR:
            notifValue = lcdmenu_requestInput(0, 3, false, 12);
            break;
        case DCLOAD_MODE_BC:
            notifValue = lcdmenu_requestInput(0, 3, false, 9);
            opmodes_batteryCapacity(actualVoltage, actualCurrent,
                                    actualPower, false);
            break;
        case DCLOAD_MODE_TC:
        case DCLOAD_MODE_TP:
        case DCLOAD_MODE_TT:
            notifValue = lcdmenu_requestInput(0, 3, false, 10);
            opmodes_transient(operationMode, lowCurrent, highCurrent,
                              period, transientTimer);
            break;
        case DCLOAD_MODE_NO_CHANGE:
            // No hace nada.
            break;
        case DCLOAD_MODE_SETUP:
            // No hace nada.
            break;
        case DCLOAD_MODE_LOGSD:
            // No hace nada.
            break;
        default:
            break;
        }

        if ((operationMode != DCLOAD_MODE_TC) && (operationMode != DCLOAD_MODE_TP)
           && (operationMode != DCLOAD_MODE_TT))
        {

            lcdmenu_maxSettings(operationMode, CurrentCutOff, VoltageCutOff,
                                PowerCutOff, MaxBatteryCurrent, ResistorCutOff);
            lcdmenu_displayEncReading(operationMode);
        }

        lcdmenu_modeMenu_update(actualCurrent, actualVoltage, actualPower, false);

        if (notifValue == MENU_SHOTCUTS_VALUE)
        {
            // Si la carga no está activada se muestra la pantalla de información.
            if (!loadOutput){
                lcdmenu_shortcuts();
                // Vuelve a preguntar por el modo a utilizar.
                xTaskNotify(taskControl_Handler, MENU_SELECT_VALUE,
                            eSetValueWithOverwrite);
            }
        }

        if (notifValue == CURSOR_NOTIF_VALUE)
        {
            xTimerStart(selTimer, 0);
            if (sound == true){
                buzzer_ms(100);
            }

            uint16_t factor = lcdmenu_cursorPosition(operationMode);
            if (factor != encoder.factor){
                encoder.factor = factor;
            }
        }

        if (notifValue == MENU_SELECT_VALUE)
        {
            // Para almacenar el modo al que regresar en caso de usar en modo
            // de configuración.
            dcLoad_Mode_t oldMode = operationMode;

            do
            {
                operationMode = lcdmenu_modeSelectionMenu();
            } while (operationMode == DCLOAD_MODE_NO_CHANGE);

            if (operationMode == DCLOAD_MODE_SETUP)
            {
                lcdmenu_userSetUpMenu(&CurrentCutOff, &PowerCutOff, &VoltageCutOff,
                                      &MaxBatteryCurrent, &sound, &polarityEn,
                                      &riseRate, &fallRate);

                operationMode = oldMode;
                // Para prevenir que el cursor no coincida con el dígito que modifica el encoder.
                if ((operationMode == DCLOAD_MODE_CC) || (operationMode == DCLOAD_MODE_CP)
                    || (operationMode == DCLOAD_MODE_CR)){

                    xTaskNotify(taskControl_Handler, CURSOR_NOTIF_VALUE,
                                eSetValueWithOverwrite);
                }
            }

            if (operationMode == DCLOAD_MODE_LOGSD)
            {
                lcdmenu_configSDLogging(&SDLoggingTime);
                operationMode = oldMode;
                if ((operationMode == DCLOAD_MODE_CC) || (operationMode == DCLOAD_MODE_CP)
                   || (operationMode == DCLOAD_MODE_CR)){

                    xTaskNotify(taskControl_Handler, CURSOR_NOTIF_VALUE,
                                eSetValueWithOverwrite);
                }
            }

            if (operationMode == DCLOAD_MODE_BC){
                opmodes_batteryCapacity(0, 0, 0, true);
            }
            if ((operationMode == DCLOAD_MODE_TC) || (operationMode == DCLOAD_MODE_TP)
                 || (operationMode == DCLOAD_MODE_TT))
            {

                lcdmenu_transientModeMenu(operationMode, &lowCurrent, &highCurrent,
                                          &period, CurrentCutOff);
                lcdmenu_modeMenu(operationMode);
                lcdmenu_transientMenu_update(operationMode, lowCurrent,
                                             highCurrent, period);
                // Se desactiva el encoder, ya que no se utiliza en este modo.
                DisableIRQ(LPTMR0_IRQn);
                // Desactiva el keypad, ya que no se utiliza tampoco.
                vTaskSuspend(xTaskGetHandle("inputValue_task"));
            }
            else
            {
                // Se activa el encoder
                EnableIRQ(LPTMR0_IRQn);
                lcdmenu_modeMenu(operationMode);
            }
            lcdmenu_modeMenu_update(actualCurrent, actualVoltage, actualPower, true);
        }

        // Protecciones
        if (actualVoltage >= VoltageCutOff)
        {
            loadOutput = pdFALSE;
            resetEncoder();
            lcdmenu_warning_message(WARNING_OVER_VOLTAGE);
            svWarn("Over Voltage");
        }

        if (actualPower > PowerCutOff)
        {

            loadOutput = pdFALSE;
            resetEncoder();
            lcdmenu_warning_message(WARNING_OVER_POWER);
            svWarn("Over Power");
        }

        if (operationMode == DCLOAD_MODE_BC)
        {
            if (actualCurrent > MaxBatteryCurrent)
            {
                loadOutput = pdFALSE;
                resetEncoder();
                lcdmenu_warning_message(WARNING_OVER_CURRENT);
                svWarn("Battery Over Current");
            }
        }
        else
        {
            if (actualCurrent > CurrentCutOff)
            {
                loadOutput = pdFALSE;
                resetEncoder();
                lcdmenu_warning_message(WARNING_OVER_CURRENT);
                svWarn("Over Current");
            }
            else{
                ; // Misra c 15.7
            }
        }

        // No permite el funcionamiento con el USB conectado para evitar
        // referenciar el terminal negativo a tierra.
        if (usbIn && loadOutput){
            loadOutput = pdFALSE;
            resetEncoder();
            lcdmenu_warning_message(WARNING_USB_CONNECTED);
            svWarn("USB connected with load ON");
        }
        // Las lecturas de voltaje y corriente tienen un delay de 65ms cada una
        // por lo que no se pone ninguno a esta tarea.
    }
}

/**
 * Filtro anti-rebotes para el puerto A.
 *
 * Usado para evitar rebotes en los botones Cursor y Load.
 *
 * @param[in] timer Timer encargado de ejecutar esta función.
 */
static void portA_debouncer(TimerHandle_t timer)
{

    // Si los botones siguen pulsados, se reinicia el timer.
    if ((GPIO_PinRead(BOARD_INITPINS_LOAD_GPIO, BOARD_INITPINS_LOAD_PIN) == 0) ||
        (GPIO_PinRead(BOARD_INITPINS_CURSOR_GPIO, BOARD_INITPINS_CURSOR_PIN) == 0)){
        xTimerReset(debounceTimer, 10 / portTICK_PERIOD_MS);
    }
    else
    {

        // Posiblemente se ha dejado de pulsar. Se espera 10 ms para confirmar.
        vTaskDelay(10 / portTICK_PERIOD_MS);
        if ((GPIO_PinRead(BOARD_INITPINS_LOAD_GPIO, BOARD_INITPINS_LOAD_PIN) == 1) &&
            (GPIO_PinRead(BOARD_INITPINS_CURSOR_GPIO, BOARD_INITPINS_CURSOR_PIN) == 1) &&
            (showingWarnMessage == pdFALSE))
        {

            // Se vuelven a activar las interrupciones del puerto A.
            PORT_SetPinInterruptConfig(BOARD_INITPINS_LOAD_PORT, BOARD_INITPINS_LOAD_PIN,
                                       kPORT_InterruptFallingEdge);
            PORT_SetPinInterruptConfig(BOARD_INITPINS_CURSOR_PORT, BOARD_INITPINS_CURSOR_PIN,
                                       kPORT_InterruptFallingEdge);
        }
        else
        {
            // Seguramente es un rebote, por lo que se reinicia el timer.
            // O se está mostrando un mensaje de advertencia.
            xTimerReset(debounceTimer, 10 / portTICK_PERIOD_MS);
        }
    }
}

/**
 * Pone al micro en el modo de ahorro de energía más alto.
 * @note Al despertar de este modo se produce un reset del micro.
 */
static void put_in_lowPowerFromISR(){
    UBaseType_t uxSavedInterruptStatus;
    uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();

    // Configura el modo de baja energía.
    smc_power_mode_vlls_config_t vlls_config;
    vlls_config.enablePorDetectInVlls0 = false;
    vlls_config.enableLpoClock = false;
    vlls_config.subMode = kSMC_StopSub0;

    // Guarda los datos pendientes y cierra el archivo abierto en la SD.
    sd_power_loss();

    // Desactiva el timer de bajo consumo y el RTC, ya que puede funcionar en el modo VLLS0.
    LPTMR_Deinit(LPTMR_0_PERIPHERAL);
    RTC_Deinit(RTC_1_PERIPHERAL);
    //Entra en modo de ahorro.
    SMC_PreEnterStopModes();
    SMC_SetPowerModeVlls(SMC, &vlls_config);

    // Las instrucciones de abajo no se ejecutarían en el modo VLLS0.
    // Se dejan por si se cambia de modo.

    // Sale del modo de ahorro.
    SMC_PostExitStopModes();
    LPTMR_Init(LPTMR_0_PERIPHERAL, &LPTMR_0_config);
    RTC_Init(RTC_1_PERIPHERAL, &RTC_1_config);
    taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);
}

/******************************************************************************!
 * IRQs Handlers
 ******************************************************************************/

/**
 * Rutina de interrupción para PWM del buzzer.
 */
void PIT_IRQHandler()
{
    svEnterISR();

    /* Con una frecuencia de 2600 Hz y un ciclo del 30% en vez de los 2700 Hz
     * y 50% especificados en el datasheet, se consigue un nivel sonoro muy
     * parecido y se reduce el consumo de 100mA a unos 50mA aprox.
     */

    // Activa el buzzer al inicio del periodo. (Genera frecuencia 2600 Hz)
    // y activa el canal 1.
    if (PIT_GetStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_0) == kPIT_TimerFlag)
    {
        GPIO_PinWrite(BOARD_INITPINS_BUZZER_GPIO, BOARD_INITPINS_BUZZER_PIN, 1);
        PIT_StopTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0);
        PIT_ClearStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_0, kPIT_TimerFlag);
        PIT_StartTimer(PIT_1_PERIPHERAL, kPIT_Chnl_1);
    }

    // Desactiva el buzzer en el tiempo indicado por el canal 1. (Ciclo de trabajo 30%)
    // y activa el canal 0.
    if (PIT_GetStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_1) == kPIT_TimerFlag)
    {
        GPIO_PinWrite(BOARD_INITPINS_BUZZER_GPIO, BOARD_INITPINS_BUZZER_PIN, 0);
        PIT_StopTimer(PIT_1_PERIPHERAL, kPIT_Chnl_1);
        PIT_ClearStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_1, kPIT_TimerFlag);
        PIT_StartTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0);
    }

    svExitISR();
}

/**
 * Rutina de interrupción por cambio en los pines de los puertos B, C, D y E.
 */
void PORTB_PORTC_PORTD_PORTE_IRQHandler()
{
    // Para saber el id de la interrupción y ponerlo en la función _cbSendSystemDesc
    //uint32_t irq_ID = SEGGER_SYSVIEW_GET_INTERRUPT_ID();

    svEnterISR();

    uint32_t portb_interrupts = PORT_GetPinsInterruptFlags(PORTB);

    if ((portb_interrupts & (1 << BOARD_INITPINS_SD_DETECT_PIN)) != 0)
    {
        PORT_ClearPinsInterruptFlags(PORTB, (1 << BOARD_INITPINS_SD_DETECT_PIN));

        if (GPIO_PinRead(BOARD_INITPINS_SD_DETECT_GPIO,
                         BOARD_INITPINS_SD_DETECT_PIN) == 0){
            // Se inserta una tarjeta SD.
            SDin = true;
        }
        else{
            // Se retira una tarjeta SD.
            SDin = false;
        }
    }

    if ((portb_interrupts & (1 << BOARD_INITPINS_SD_PROTECT_PIN)) != 0)
    {
        PORT_ClearPinsInterruptFlags(PORTB, (1 << BOARD_INITPINS_SD_PROTECT_PIN));

        if (GPIO_PinRead(BOARD_INITPINS_SD_PROTECT_GPIO,
                         BOARD_INITPINS_SD_PROTECT_PIN) == 0){
            // Tarjeta SD sin bloquear.
            SDblock = false;
        }
        else{
            // Tarjeta SD BLOQUEADA.
            SDblock = true;
        }
    }

    if ((portb_interrupts & (1 << BOARD_INITPINS_VBAT_DETECT_PIN)) != 0)
    {
           PORT_ClearPinsInterruptFlags(PORTB, (1 << BOARD_INITPINS_VBAT_DETECT_PIN));

           if (GPIO_PinRead(BOARD_INITPINS_VBAT_DETECT_GPIO,
                            BOARD_INITPINS_VBAT_DETECT_PIN) == 0){
               // Alimentación con batería.
               put_in_lowPowerFromISR();
           }
           else{
               // Alimentación externa.
              ;
           }
    }

    uint32_t portc_interrupts = PORT_GetPinsInterruptFlags(PORTC);

    if ((portc_interrupts & (1 << BOARD_INITPINS_VBUS_DETECT_PIN)) != 0)
    {
           PORT_ClearPinsInterruptFlags(PORTC, (1 << BOARD_INITPINS_VBUS_DETECT_PIN));

           if (GPIO_PinRead(BOARD_INITPINS_VBUS_DETECT_GPIO,
                            BOARD_INITPINS_VBUS_DETECT_PIN) == 0){
               // USB sin conectar
               usbIn = false;
           }
           else{
               // USB conectado.
               usbIn = true;
           }
       }

    svExitISR();
}

/**
 * Rutina de interrupción por cambio en los pines del puerto A.
 */
void PORTA_IRQHandler()
{
    // Para saber el id de la interrupción y ponerlo en la función _cbSendSystemDesc
    //uint32_t irq_ID = SEGGER_SYSVIEW_GET_INTERRUPT_ID();
    // Las únicas interrupciones en el puerto A son el botón Cursor y Load.

    svEnterISR();

    uint32_t porta_interrupts = PORT_GetPinsInterruptFlags(PORTA);

    // Se asume que solo se utiliza un botón a la vez.
    // por lo que se deshabilitan las interrupciones a modo de filtro contra rebotes.

    PORT_SetPinInterruptConfig(BOARD_INITPINS_LOAD_PORT, BOARD_INITPINS_LOAD_PIN,
                               kPORT_InterruptOrDMADisabled);
    PORT_SetPinInterruptConfig(BOARD_INITPINS_CURSOR_PORT, BOARD_INITPINS_CURSOR_PIN,
                               kPORT_InterruptOrDMADisabled);

    if ((porta_interrupts & (1 << BOARD_INITPINS_LOAD_PIN)) != 0)
    {
        PORT_ClearPinsInterruptFlags(PORTA, (1 << BOARD_INITPINS_LOAD_PIN));
        if (loadOutput == true){
            loadOutput = false;
        }
        else{
            loadOutput = true;
        }
    }

    if ((porta_interrupts & (1 << BOARD_INITPINS_CURSOR_PIN)) != 0)
    {
        PORT_ClearPinsInterruptFlags(PORTA, (1 << BOARD_INITPINS_CURSOR_PIN));
        xTaskNotifyFromISR(taskControl_Handler, CURSOR_NOTIF_VALUE,
                           eSetValueWithOverwrite, NULL);
    }

    // Se activa un timer que vuelve a habilitar las interrupciones pasado un tiempo.
    xTimerStartFromISR(debounceTimer, NULL);
    svExitISR();
}

/**
 * Rutina de interrupción por cambio de estado de encoder.
 */
void Encoder_Handler()
{

    // Se utiliza un filtro por hardware del micro. Reloj 1Mhz, 128 pulsos consecutivos.
    svEnterISR();

    LPTMR_ClearStatusFlags(LPTMR_0_PERIPHERAL, LPTMR_CSR_TCF(1));

    if (lockEncoder == false)
    {

        if (GPIO_PinRead(BOARD_INITPINS_ENC_A_GPIO, BOARD_INITPINS_ENC_A_PIN) == 0)
        {
            encoder.position = encoder.position + encoder.factor;
        }
        else
        {
            encoder.position = encoder.position - encoder.factor;
        }
        encoder.position = MIN(encoder.max, MAX(0, encoder.position)); // Fija la posición máxima.
        encoder.reading = (float)encoder.position / 1000.0;
        encoder.lastCount = encoder.position;
    }
    svExitISR();
}

/**
 * Rutina de interrupción para aumentar el contador de tiempo de ejecución a
 * la hora de generar estadísticas.
 */
void TPM2_IRQHandler()
{
    TPM_ClearStatusFlags(TPM2, kTPM_TimeOverflowFlag);

    if (configGENERATE_RUN_TIME_STATS == 1){
        RTOS_RunTimeCounter++;
    }
}

/**
 * Rutina de interrupción para el botón Boot.
 * @note No realiza ninguna acción, solo permite despertar al micro del modo
 * de ahorro VLLS0.
 */
void NMI_Handler(void){

}

/**
 * Rutina de interrupción por cada segundo del RTC.
 *
 * Se usa para contar el tiempo de descarga de las baterías.
 */
void RTC_1_SECONDS_IRQHANDLER()
{
    svEnterISR();

    Seconds++;

    svExitISR();
}

/**
 * Función principal de entrada al programa.
 * @return 0 En caso de error, ya que no debería salir nunca.
 */
int main(void)
{
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    SMC_SetPowerModeProtection(SMC, kSMC_AllowPowerModeAll);
    BOARD_InitBootPeripherals();

    cop_config_t configCop;

    /*Configuración por defecto del watchdog
     *
     * configCop.enableWindowMode = false;
     * configCop.timeoutMode = kCOP_LongTimeoutMode;
     * configCop.enableStop = false;
     * configCop.enableDebug = false;
     * configCop.clockSource = kCOP_LpoClock;
     * configCop.timeoutCycles = kCOP_2Power10CyclesOr2Power18Cycles;
     */
    COP_GetDefaultConfig(&configCop);
    configCop.timeoutCycles = kCOP_2Power5CyclesOr2Power13Cycles; //8182 ms.
    COP_Init(SIM, &configCop); // Inicializa el watchdog.

    svConf(); // Inicializa el segger systemview.

    // Empieza sin tener conectado el JLink. Para ver la memoria utilizada.
    //svStart();

    svPrint("Init of main");
    loadOutput = pdFALSE;

    // Parece que hay un bug en MCUXpresso v11. En el configurador gráfico.
    // Los valores de los condensadores de carga para el cristal del módulo
    // RTC no se guardan en el registro correspondiente.
    // Así como la selección del reloj para el RTC. Por lo que no funcionaba.
    // Por ello se añaden las 2 líneas siguientes, una vez terminada la configuración
    // generada automáticamente.
    RTC_SetOscCapLoad(RTC_1_PERIPHERAL, kRTC_Capacitor_8p | kRTC_Capacitor_4p);
    RTC_SetClockSource(RTC_1_PERIPHERAL);

    // Para el contador de segundos.
    DisableIRQ(RTC_1_SECONDS_IRQN);

    BaseType_t result = 0;
    TaskHandle_t taskHanle = NULL;

    result = xTaskCreate(keypadRead,           /* Función que implementa la tarea a realizar. */
                         "keypad_Reader",      /* Nombre de la tarea - no usado por el RTOS, solo para ayudar con el debugging. */
                         DEFAULT_STACK_SIZE,   /* Tamaño del buffer usado como pila - en palabras, no bytes. */
                         NULL,                 /* Parámetro pasado a la tarea - no usado en este caso. */
                         KEYPAD_TASK_PRIORITY, /* Prioridad de la tarea. */
                         NULL);                /* Usado para pasar el manejador a la tarea creada. */

    if (result != pdPASS)
    {
        svError("Task keypadReader creation failed!");
        for (;;){
            ;
        }
    }

    result = xTaskCreate(lcdControl,
                         "lcd_Control",
                         DEFAULT_STACK_SIZE * 2,
                         NULL,
                         LCD_TASK_PRIORITY,
                         NULL);

    if (result != pdPASS)
    {
        svError("Task lcdControl creation failed!");
        for (;;){
            ;
        }
    }

    result = xTaskCreate(taskControl,
                         "task_Control",
                         DEFAULT_STACK_SIZE * 4,
                         NULL,
                         CONTROL_TASK_PRIORITY,
                         &taskControl_Handler);

    if (result != pdPASS)
    {
        svError("Task task_Control creation failed!");
        for (;;){
            ;
        }
    }

    result = xTaskCreate(lcdmenu_inputValueTask,
                         "inputValue_task",
                         DEFAULT_STACK_SIZE,
                         NULL,
                         INPUT_TASK_PRIORITY,
                         &taskHanle);

    if (result != pdPASS)
    {
        svError("Task lcdmenu_inputValue creation failed!");
        for (;;){
            ;
        }
    }

    // Suspende la tarea hasta que se inicien las demás.
    vTaskSuspend(taskHanle);

    // Habilita las interrupciones del puertoA pasados 20 ms. (Anti-rebotes)
    debounceTimer = xTimerCreate("portA debouncer timer",
                                 (20 / portTICK_PERIOD_MS),
                                 pdFALSE,
                                 (void *)0,
                                 portA_debouncer);

    if (debounceTimer == NULL)
    {
        svError("debounceTimer creation failed!");
        for (;;){
            ;
        }
    }

    svPrint("Start Scheduler");
    vTaskStartScheduler();

    for (;;){
        ;
    }
    return 0;
}

// Sólo para depuración.
void vApplicationStackOverflowHook(TaskHandle_t xTask,
                                   signed char *pcTaskName)
{
    for (;;){
        ;
    }
}


void RTOS_ConfigureTimerForRuntimeStats(void) {
  RTOS_RunTimeCounter = 0;
  EnableIRQ(TPM2_IRQn);
}

uint32_t RTOS_GetRuntimeCounterValueFromISR(void) {
  return RTOS_RunTimeCounter;
}
