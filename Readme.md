DC Load New Brain
===========================

Este proyecto consiste en un cambio de microcontrolador para la carga electrónica hecha anteriormente. Se pasa de un Atmega 328p (8bits a 16 Mhz) a un Cortex-M0+ (32bits a 48 Mhz), y
de un firmware basado en Arduino a uno usando FreeRTOS.

![PCB](https://1.bp.blogspot.com/-lOOZ26DPPfM/XVJ2_dxDAZI/AAAAAAAAB5E/fMLY6NaKATUXImcI0OqzsSxOn4zLwn0bgCKgBGAs/s640/20190731_0001.jpg)


Características añadidas a la carga electrónica:

 - Se reescribe todo en C usando FreeRTOS.
 - El algoritmo de compensación de corriente es mucho más rápido y eficiente.
 - Se utilizan múltiples rangos a la hora de calibrar la carga con lo que se consigue más          precisión. (Se pasa de ± 5 mV de error en tensión a ± 2 mV)
 - Mejora de tiempo entre transiciones en el modo transient. (10-99.999ms)
 - Se añade un zumbador con posibilidad de desconexión.
 - En el menú de Set-Up se comprueba si se superan los limites de la carga.
 - Se desactiva la carga al cambiar de modo de operación.
 - Se añade capacidad de definir tiempos de subida y bajada de la carga. (En mA/ms).
 - Se añade soporte para tarjeta SD para poder guardar logs.
 - Se usa el RTC del micro para añadir fecha y hora a los logs.
 - Se añade batería para poder terminar la escritura en la SD en caso de fallo de alimentación y   evitar que el sistema de archivos FAT32 se corrompa.
 - Se añade posibilidad de cargar el firmware por USB.
 - No permite conectar la carga si se detecta conexión USB. (Para evitar referenciar a tierra y    posibles fugas).
 - Se añade capacidad de detener la descarga de un batería al llegar a unos mAh indicados.
 - La lectura de resistencia interna permanece en pantalla hasta la pulsación de un botón.
 - Se añade protección contra sobre corriente.
 - Se añade un watchdog para reiniciar el micro en caso de bloqueo.
 - Se añade protección de offset. (Si existe un voltaje o corriente superior a 1mV o 2mA al        arrancar, no deja continuar. Ya que la diferencia puede ocasionar que el ADC no llegue al       final de escala y las protecciones de máxima corriente y máximo voltaje no funcionarían.)

![Carga](https://1.bp.blogspot.com/-R3f8CqPosAI/XVfeyAtJHqI/AAAAAAAACCA/EcEe1k7oYz0_VEsIjcswsT8jq6Acf2LwwCKgBGAs/s640/20190731_0099.jpg)

Los detalles del proyecto de la carga electrónica se encuentran en el [blog](https://www.circuiteando.net/2018/05/carga-electronica-parte-1-introduccion.html) y en su
[reposirorio.](https://gitlab.com/JGReyes/DC_Load)

Para más información sobre este proyecto consultar el [blog.](https://www.circuiteando.net/2019/08/dc-load-new-brain.html)